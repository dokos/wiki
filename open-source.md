---
title: Logiciel Open source
description: 
published: true
date: 2022-10-10T13:43:19.169Z
tags: 
editor: markdown
dateCreated: 2021-12-14T10:16:21.492Z
---

# Un logiciel Open-source

La platefome de Dokos est un logiciel libre et open source publié sous licence GNU General Public License (GPLv3). Cette licence vous accorde :

- La liberté d'utiliser le logiciel à quelque fin que ce soit
- La liberté de changer le logiciel en fonction de vos besoins
- La liberté de partager le logiciel avec n'importe qui
- La liberté de partager les changements que vous apportez 
- Le code source de Dokos et des modules annexes est disponible sur GitLab à l'adresse <https://gitlab.com/dokos>.

### Quels sont les avantages d'un logiciel open-source ?

1. Il n'y a pas de verrouillage avec un fournisseur spécifique.
2. Vous pouvez héberger l'application n'importe où, y compris sur votre propre serveur, pour obtenir l'entière propriété et la confidentialité des données.
3. Vous pouvez accéder à une communauté pour obtenir de l'aide.
4. Vous pouvez bénéficier de l'utilisation d'un produit qui est critiqué et utilisé par un large éventail de personnes, qui ont signalé des centaines de problèmes et des suggestions pour améliorer le produit.

### Un accompagnement possible pour utiliser le logiciel de Dokos

Bien que Dokos soit gratuit et open source, faire fonctionner un serveur pour Dokos, l'implémenter et le prendre en charge n'est pas une tâche facile. 

Vous pouvez opter pour une aide professionnelle sur www.dokos.io. Cela vous permettra de vous concentrer sur la gestion de votre activité.