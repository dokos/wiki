---
title: Accueil
description: Outil de gestion pour les entreprises | ERP Français Open-Source
published: true
date: 2021-11-17T14:31:56.793Z
tags: 
editor: markdown
dateCreated: 2020-11-26T12:11:53.800Z
---

# Bienvenue sur DOKOS

Dokos est une adaptation d'un logiciel de gestion open-source bien connu: ERPNext.  
Bénéficiant de plus de 10 ans de développements et d'une utilisation dans des milliers d'entreprises, c'est un logiciel de gestion robuste et performant.

Dokos a été créé pour permettre d'adapter ce logiciel aux normes françaises et européennes et d'accélérer son développement en Europe.

Il est distribué sous licence GPLv3.

---

**Retrouvez toutes nos actualités sur les réseaux sociaux** :

- <a href="https://www.linkedin.com/company/dokos.io" target="_blank">**Linkedin**</a>
- <a href="https://www.facebook.com/dokos.io" target="_blank">**Facebook**</a>
- <a href="https://www.youtube.com/channel/UC2f3m8QANAVfKi2Pzw2fBlw
" target="_blank">**Youtube**</a> .

## Liens rapides

### Général

- [1. Introduction à l'ERP](/fr/introduction)
- [2. Démarrer avec Dokos](/fr/getting-started)
- [3. Contribuer](/fr/contributing)
- [4. Versions](/fr/versions)
- [5. Utilisation de l'ERP Dokos](/fr/tools)
{.links-list}

### Les modules

- [1. Achats](/fr/buying)
- [2. Comptabilité](/fr/accounting)
- [3. CRM](/fr/crm/)
- [4. Intégrations](/fr/integrations)
- [5. Lieu](/fr/venue)
- [6. Ressources Humaines](/fr/human-resources)
- [7. Site web](/fr/website)
- [8. Stocks](/fr/stocks)
- [9. Ventes](/fr/selling)
{.links-list}

---
Une partie du contenu est une adaptation de la [documentation d'ERPNext](https://docs.erpnext.com/)  
Licence: [CC 4.0 BY-SA-NC](https://creativecommons.org/licenses/by-nc-sa/4.0/)