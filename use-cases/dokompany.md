---
title: Présentation de la demo : Dokompany
description: 
published: true
date: 2021-10-27T13:22:15.103Z
tags: 
editor: markdown
dateCreated: 2021-10-27T13:13:04.901Z
---

# Dokompany

## Démo et Cas d'usage

Cette société est une organisation fictive. Elle sera utilisée dans nos cas d'usages et sur la démo comme exemple concret pour illustrer nos propos. 

L'objectif est de vous mettre une organisation qui reprend un ensemble d'activité pour vous montrer toutes les possibilités sur notre logiciel de gestion.

Lien d'accès à la démo : **https://demo.dokos.cloud/**

## Présentation générale

Dokompany est une société qui a été créé en 2016. L'activité principale de l'entreprise est de produire et vente des planches de skateboard. Pour cela, elle travaille avec des fournisseurs pour l'achat des pièces nécessaires pour l'assemblage et elle revend ses planches à tout type de client.
