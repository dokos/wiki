---
title: Présentation du Tiers Lieu : Mappemonde
description: 
published: true
date: 2021-11-04T15:12:33.527Z
tags: 
editor: markdown
dateCreated: 2021-09-10T12:13:37.148Z
---

# MappeMonde

## Démo et Cas d'usage

Ce Tiers-Lieu est une organisation fictive. Il sera utilisé dans nos cas d'usages et sur la démo comme exemple concret pour illustrer nos propos. 

L'objectif des cas d'usage de vous mettre une organisation qui reprend un ensemble d'activité pour vous montrer toutes les possibilités sur notre logiciel de gestion.

Lien d'accès à la démo : **https://tierslieux.dokos.io/**

## Présentation générale

Mappemonde est un Tiers Lieu qui a été créé en 2021. Il regroupe de nombreuses activités comme des espaces de Coworking, des salles de réunion, des machines pour jouer à des jeux d'arcades, la réservation de jeux de sociétés, de livres etc..

Situé au coeur de Paris, notre Tiers-lieu est accessible à toutes les personnes. Il suffit d'adéherer à nos offres que vous pouvez retrouver sur notre site web et sur place. Une carte d'adhérent vous sera remise avec un accès à notre intranet. 

Vous pourrez réserver directement en ligne une salle de réunion, un jeu etc.

## Les activités

Chez MappeMonde, vous allez pouvoir travailler, vous divertir et même vous ressourcer. Voici la liste de nos principales activités qui sont accessibles à nos adhérents.

- Salle de Coworking
- Salle de réunion
- Jeux d'arcades
- Jeux de sociétés
- Bibliothèque
- Accès ordinateur et internet
- Impression des documents