---
title: Intégration avec Paypal
description: 
published: true
date: 2021-01-27T14:28:34.071Z
tags: 
editor: markdown
dateCreated: 2021-01-27T14:27:01.388Z
---

# Configuration

Afin de configurer une intégration avec Paypal, vous devez ajouter les éléments suivants:

- Ouvrez les `Paramètres Paypal`
  Allez dans `Intégrations > Paramètres Paypal`
- Ajoutez le nom d'utilisateur, le mot de passe et la signature de votre application Paypal
  Ces informations peuvent être obtenues en créant un compte sur `https://developer.paypal.com/` puis en créant une nouvelle application
- Cochez `Utiliser la sandbox` s'il s'agit d'une application Paypal de test (Sandbox)

# Usage

- [Passerelles de paiement](/fr/accounting/payment-gateways)
{.links-list}