---
title: Allocation de politique de congés
description: 
published: true
date: 2020-12-04T08:49:40.088Z
tags: 
editor: markdown
dateCreated: 2020-12-04T07:49:12.626Z
---

```mermaid
graph LR;
    PC(Politique de congés)-->APC(Allocation de politique de congés)-->AC(Allocation de congés);
		class APC current-doc;
```


# Usage

L'objectif de cet outil est de permettre d'allouer les congés aux employés en fonction des politiques de congés de l'entreprise.

# Allocation automatique

Vous pouvez créer vos documents d'allocation de politique de congés pour les allocations futures et les congés seront automatiquement alloués à partir de la date d'entrée en vigueur (**Effectif à partir du**).