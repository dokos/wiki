---
title: Période de congés
description: 
published: true
date: 2020-11-27T07:16:17.845Z
tags: 
editor: undefined
dateCreated: 2020-11-27T07:16:15.978Z
---

# Période de congés

Une période de congés permet de définir les dates de début et de fin pour le calcul des congés d'un type de congés donné.  

> Les congés payés sont calculés du 01/06 au 31/05 et les RTT du 01/01 au 31/12.  
> Créez donc deux périodes de congés différentes pour ces deux types de congés.  
{.is-info}

## Allocation de congés

Une fois les dates définies et votre document enregistré, cliquez sur le bouton "Accorder des congés", choisissez des filtres si besoin et cliquez sur "Accorder". Si vous ne mettez aucun filtre, tous les employés seront sélectionnés.  
De nouvelles "Allocation de congés" seront créées automatiquement pour tous les employés correspondant aux filtres choisis.  

Pour les congés acquis, l'allocation totale sera de 0 et le calcul démarrera automatiquement la nuit suivante.  