---
title: Paramètres du panier
description: 
published: true
date: 2020-12-02T14:57:38.387Z
tags: 
editor: markdown
dateCreated: 2020-12-02T14:57:38.387Z
---

# Paramètres de caisse

En activant la caisse, vous permettez à vos clients de régler leurs commandes passées sur le site web directement en ligne.  

Le **compte passerelle de paiement** est le compte qui sera utilisé pour effectuer le paiement.
A ce jour un seul compte passerelle de paiement peut être sélectionné, ce qui veut dire que vos paiements ne peuvent être que dans une seule devise et avec une seule passerelle de paiement.

L'**URL de réussite de paiement** est la page vers laquelle l'utilisateur sera re-dirigé après avoir effectué son paiement.