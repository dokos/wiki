---
title: Démarrer avec Dokos
description: 
published: true
date: 2020-11-27T07:14:10.727Z
tags: 
editor: undefined
dateCreated: 2020-11-26T14:15:30.702Z
---

# Installation

- [Installation Rapide](/fr/getting-started/easy-installation)
- [Installation Docker *:construction:*](/fr/getting-started/docker-installation)
- [Migration ERPNext](/fr/getting-started/erpnext-migration)
- [Domain Setup](/fr/getting-started/domain-setup)
- [SSL Certificate](/fr/getting-started/ssl-certificate)
{.links-list}

# Maintenance

- [Mettre à jour Dokos](/fr/getting-started/updating-dokos)
{.links-list}

# Configuration