---
title: v1.0.0
description: 
published: true
date: 2020-11-27T07:16:51.040Z
tags: 
editor: undefined
dateCreated: 2020-11-26T17:25:56.793Z
---

# V1.0.0

:tada: Lire [l'article de blog](https://dokos.io/blog/dokos-nouvel-erp-open-source) :tada:

La v1.0.0 de dokos est basée sur Frappe/ERPNext v12.x.x
Vous trouverez ci-dessous les principales différences avec Frappe/ERPNext.

## dokos
- Les domaines suivants ne sont pas présent dans dokos:
    * Hospitalité
    * Santé
    * Education
    * À But Non Lucratif
    * Agriculture
- Le hub a été supprimé de dokos
- Les types de documents suivants ont été supprimé de dokos:
    * POS Settings (Paramètres du point de vente): Le point de vente hors ligne a été entièrement désactivé dans dokos
    * Cashier closing (Clôture du caissier)
    * Cashier closing payments (Paiements de clôture du caissier)
    * Bank Statement Settings (Paramètres de relevé bancaire)
    * Bank Statement Setting Item (Elément de paramètre de relevé bancaire)
    * Bank Statement Transaction Entry (Transaction bancaire)
    * Bank Statement Transaction Invoice Item (Poste de facture d'une transaction bancaire)
    * Bank Statement Transaction Payment Item (Poste de paiement d'une transaction bancaire)
    * Bank Statement Transaction Settings (Paramètre de transaction bancaire)
    * Bank Statement Transaction Settings Item (Paramètre de transaction bancaire)
    * Subscription Invoice (Facture d'abonnement)
    * Subscription Settings (Paramètres d'abonnement)
    * Subscriber (Abonné)
- Le point de vente hors ligne a été entièrement supprimé car il n'était pas conforme et reposait sur une solution de conservation des données non permanente (LocalStorage).
- La fonctionalité d'abonnement a été complètement refondue et il désormais possible de:
    * Générer une commande client avant la génération d'une facture de vente
    * Lier un abonnement avec un abonnement sur une passerelle de paiement et faire en sorte que son cycle de vie soit géré via les webhooks de cette plateforme
- Les intégration Stripe et GoCardless ont été refondues pour gérer les abonnements et générer des événements via les webhooks
- Un nouveau type de document __Prélèvement Sepa__ permet de générer un fichier XML aux formats pain.008.001.02, pain.008.002.02 ou pain.008.003.02
- Vous pouvez désormais étendre la date de fin de validité de vos devis
- Les factures de ventes/factures d'achat/écritures de paiement n'obtiennent leur nom définitif qu'après validation. Cela résout un problème règlementaire que des utilisateurs pouvaient rencontrer dans certains pays.
- Un nouveau type de document __Réservation d'articles__ permet d'ouvrir les réservations via votre portail pour vos clients.
- Refonte de la page __Profil (/me)__ du portail pour intégrer un module de mise à jour des méthodes de paiement (Seul Stripe est compatible pour l'instant)

## dodock [Modèle d'application]
- L'interface utilisateur a été améliorée avec quelques changements majeurs:
    * Le bureau est désormais un grand tableau de bord sur lequel vous pouvez attacher trois types d'éléments: calendriers, graphiques et cartes
        1. Les calendriers récupèrent les événements de tous les types de document avec une "vue calendrier"
        2. Les graphiques peuvent être créé dans le type de document "Graphique du tableau de bord" sur la base n'importe quel type de document ou via des scripts personnalisés dans une application personnalisée
        3. Les cartes peuvent être créées dans le type de document "Carte du tableau de bord" sur la base de n'importe quel type de document ou via des scripts personnalisés dans une application personnalisée
    * Les modules sont désormais accessibles via une barre latérale
    * Les détails des modules (types de documents/pages/rapports) peuvent être trouvés en cliquant sur le module correspondant dans la barre latérale
    * Chaque module peut disposer de son propre tableau de bord
    * L'interface utilisateur est désormais en plein écran par défaut
    * Le bureau est également moins accessible sur mobiles pour le moment. Le décision actuelle est de l'optimiser pour les tablettes et les écrans d'ordinateur.

- Les documents peuvent désormais être nommés définitivement après validation. (Utile pour les factures par exemple. Voir ci-dessus)
- Les types de document peuvent être scellés désormais:
    * Ils sont intégrés dans une chaîne cryptée, garantissant ainsi qu'ils ne puissent pas être supprimés du système sans trace.
    * Cela replace le type de document "Transaction Log"
- Les impressions de documents peuvent être suivies désormais: l'horodatage de la première impression est enregistré dans la base de données et toutes les impressions suivantes peuvent être marquées comme étant des duplicata.
- Le système de traduction a été refondu:
    * Les traductions sont désormais spécifiques d'un fichier, réduisant ainsi le risque de colision dans certaines langues.
    * Le format du fichier de traduction est désormais le JSON
- L'intégration Stripe est désormais disponible uniquement dans dokos
- Multiples améliorations de l'interface du portail