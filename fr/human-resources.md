---
title: Ressources Humaines
description: 
published: true
date: 2020-12-04T18:06:07.649Z
tags: 
editor: markdown
dateCreated: 2020-11-27T07:14:12.770Z
---

# Gestion des congés

```mermaid
journey
    title Gestion des congés
    section Configuration
      Liste de congés: 5: RH
      Types de congés: 5: RH
      Périodes de congés: 5: RH
      Politiques de congés: 5: RH
    section Allocation de congés
    	Allocation de congés: 5: RH
      Feuilles de présence: 5: Employé
      Calcul des congés: 5: Dokos
    section Demandes de congés
     Demandes de congés: 5: Employé
     Validation de congés: 5: RH
```

## Cas d'usage
- [Premiers pas avec les congés](/fr/human-resources/leave-setup)
{.links-list}

## Documents
- [Allocation de politique de congés](/fr/human-resources/leave-policy-assignment)
- [Politique de congés](/fr/human-resources/leave-policy)
- [Période de congés](/fr/human-resources/leave-period)
- [Type de congés](/fr/human-resources/leave-type)
{.links-list}

# Gestion des présences

- [Présences](/fr/human-resources/attendance)
{.links-list}