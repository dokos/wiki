---
title: Modèle de promesse d'embauche
description: 
published: true
date: 2021-06-14T12:39:35.360Z
tags: 
editor: markdown
dateCreated: 2021-06-14T12:39:35.360Z
---

# Modèle de promesse d'embauche

Le modèle de promesse d'embauche vous permet de créer un format par défaut pour votre lettre de promesse d'embauche. Sur DOKOS vous avez la possibilité par exemple de créer un modèle selon le poste, le département etc. 

Le modèle sera repris automatiquement quand vous allez créer une Promesse d'embauche et que vous sélectionnerez le modèle.

---

Pour accéder à **la liste de Modèle de promesse d'embauche**, allez sur :

> Accueil > Ressources Humaines > Recrutement > **Modèle de promesse d'embauche**

![liste_promesse_d'embauche.png](/humains-ressources/appointment-letter-template/liste_promesse_d'embauche.png)

## 1. Comment créer un modèle de promesse d'embauche ?

1. Accéder à la liste de modèle de promesse d'embauche, cliquez sur **:heavy_plus_sign: Ajouter Modèle de promesse d'embauche**
2. Remplissez l'introduction, les termes et la conclusion
3. Cliquez sur **Enregistrer**.

![détails_modèle.png](/humains-ressources/appointment-letter-template/détails_modèle.png)

