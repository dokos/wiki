---
title: Groupe d'employés
description: 
published: true
date: 2021-06-02T12:24:56.410Z
tags: 
editor: markdown
dateCreated: 2021-06-02T12:24:56.410Z
---

# Groupe d'employés

Le groupe d'employés regroupe des employés en fonction de certains attributs tels que la désignation, le grade, la branche, etc.

Pour accéder au **groupe d'employés**, accédez à :

Accueil > Ressources humaines > Employé > **Groupe d'employés**

![liste_groupe_employé.png](/humains-ressources/employee-group/liste_groupe_employé.png)

## 1. Prérequis avant utilisation

Avant de créer un groupe d'employés, il est conseillé de créer les documents suivants :

- **[Employé](/fr/human-resources/employee)**

## 2. Comment créer un groupe d'employés

1. Accédez à la **liste des groupes d'employés**, cliquez sur :heavy_plus_sign: Ajouter Groupe employé.
2. Entrez le **nom**.
3. Sélectionnez et ajoutez l'**ID d'employé au groupe**. Le nom de l'employé sera automatiquement récupéré.
4. **Enregistrer**.

![créer_groupe_employé.png](/humains-ressources/employee-group/créer_groupe_employé.png)

## 3. Caractéristiques

### 3.1 Contrat de niveau de service

Un groupe d'employés peut être ajouté au doctype **Accord de niveau de service**, où le niveau de service peut être spécifié pour un groupe d'employés particulier.

