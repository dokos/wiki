---
title: Plan d'abonnement
description: 
published: true
date: 2022-10-27T16:14:37.415Z
tags: 
editor: markdown
dateCreated: 2021-09-30T16:54:02.924Z
---

# Plan d'abonnement

La notion de plan de d'abonnement consiste à créer un plan avec la table des articles, des quantités, des prix et la période.
Vous pouvez créer autant de plans d'abonnement que nécessaire.

Dans une même facture vous ne pouvez ajouter que des plans d'abonnements ayant la même devise, la même règle de détermination du prix et le même intervale de facturation.

Si votre plan d'abonnement est lié à un plan de facturation Stripe, vous pouvez l'ajouter dans la section plans de passerelles de paiement.

A la différence, un modèle d'abonnement vous permettra de définir la devise, les intervales de facturation et d'autres paramètres spécifiques.

---

Pour accéder à la liste des plans d'abonnements, allez sur :

> Accueil > Lieu > Abonnement > **Plan d'abonnement**

## 1. Comment créer un plan d'abonnement ?

1. Allez sur la liste des plans d'abonnements, cliquez sur **Ajouter un Plan d'abonnement**
2. Renseignez les différents champs suivant :
	- Indiquez un **Nom du plan**
 	- Remplissez la table des articles (quantités, articles, prix, dates)

## 2. Détails du plan d'abonnement

### 2.1 Portail

- **Modifiable sur portail** : Si la case est cochée, alors le plan d'abonnement pourra être modifié depuis le portail.
- **Limiter au groupe de client** : Sélectionnez le groupe de client qui aura accès au portail avec ce plan d'abonnement.

### 2.2 Description du portail

Dans cette section, indiquez une description pour ce plan d'abonnement. Cette description sera affichée sur le portail client.