---
title: Modèle d'abonnement
description: 
published: true
date: 2022-10-27T16:15:01.829Z
tags: 
editor: markdown
dateCreated: 2021-10-04T09:37:33.109Z
---

# Modèle d'abonnement

Un modèle d'abonnement vous permettra de définir la devise, les intervales de facturation et d'autres paramètres spécifiques.

Le modèle d'abonnement pourra être utilisé dans la création de vos abonnements.

---

Pour accéder à la liste des modèles d'abonnement, allez sur : 

> Accueil > Lieu > Abonnement > **Modèle d'abonnement**

## 1. Comment créer un modèle d'abonnement ?

1. Allez dans la liste des modèles d'abonnement, cliquez sur **Ajouter Modèle d'abonnement**.
2. Remplissez les différents champs suivant :
	- Indiquez le nom du modèle d'abonnement
	- Choisissez une **devise**
	-	Indiquez le nombre de jours avant échéance
    
> Vous pouvez également créer un abonnement en partant d'un modèle d'abonnement. Il suffit alors d'aller sur un modèle d'abonnement. Dans la section **Documents liés** > Cliquez sur **ajouter un abonnement**.
{.is-warning}

## 2. Détails du modèle d'abonnement

### 2.1 Paramètres

- **Générer une commande client au début de la période** : Pour activer cette option, cochez la case.
- **Générer une facture au début de la période** : Pour activer cette option, cochez la case.
- **Valider la facture automatiquement** : Pour activer cette option, cochez la case.
- **Générer une demande de paiement automatiquement** : Pour activer cette option, cochez la case.
- **Intervalle de facturation** : Définissez un intervalle de facturation (Jour, Semaine, Mois, Année).
- **Fréquence de facturation** : Choisissez le nombre de facturation dans l'intervalle. Par défaut, la fréquence est de 1.
- **Date de début** : Choisissez la date de début de l'abonnement (Date de création, 1er jour du mois, 15 du mois, Dernier jour du mois).

### 2.2 Plan

Depuis cette section, gérez vos **paramètres de plan** :

Vous pouvez sélectionner un plan d'abonnement qui sera relié au modèle d'abonnement. 
Dès que vous aller créer un abonnement depuis ce modèle, alors le plan sera également généré sur l'abonnement.

### 2.3 Réductions

Depuis cette section, gérez vos **paramètres de Réductions** :

- **Appliquer une Remise Supplémentaire Sur** : Total TTC ou Total net
- **Pourcentage de réduction additionnel** : Indiquez un pourcentage de réduction sur l'abonnement.
- **Montant de la réduction additionnelle** : Indiquez un montant de réduction sur l'abonnement.

### 2.4 Termes et conditions

Depuis cette section, gérez vos **paramètres de termes et conditions** :

- Sélectionnez un modèle de **Termes et conditions**.

### 2.5 Portail

Depuis cette section, gérez vos **paramètres de portail** :

- **Disponible sur le portail** : Si cochée, alors ce modèle d'abonnement sera visible sur le portail
- **Description du portail** : Renseignez une description du modèle d'abonnement.

### 2.6 Passerelles de paiement

Depuis cette section, gérez les **passerelles de paiement** :

- Sélectionnez les passerelles de paiement autorisé pour ce modèle (Stripe, GoCardless..)
