---
title: Facture de vente
description: 
published: true
date: 2022-10-27T16:03:56.725Z
tags: 
editor: markdown
dateCreated: 2021-05-14T18:15:54.544Z
---

# Facture de vente
Une facture de vente est une facture que vous envoyez à vos clients contre laquelle le client effectue le paiement.

La facture de vente est une transaction comptable. Lors de la soumission de la facture de vente, le système met à jour la créance et comptabilise les revenus sur un compte client.

```mermaid
flowchart LR
	id4-->id9(Bon de livraion)
  id2(Commande client)-->id4(FACTURE DE VENTE)
  id4-->id5(Ecriture de paiement)
  id2-->id6("Ecriture de paiement (Acompte)")
  id1(Piste)-->id7(Opportunité)
 	id7-->id8[Devis Client]
  id8-->id2
```

---

Pour accéder à la liste des **factures de vente**, allez sur :

> Accueil > Comptabilité > Comptes clients > **Facture de vente**
**ou** 
Accueil > Vente > **Facture de vente**


## 1. Prérequis 
Avant de créer et d'utiliser une facture de vente, il est conseillé de créer d'abord les éléments suivants:

- **[Article](/fr/stocks/item)**
- **[Client](/fr/crm/customer)**

**Optionnel** :

- **[Commande client](/fr/selling/sales-order)**
- **[Bon de livraison](/fr/stocks/delivery-note)**

## 2. Comment créer une facture de vente

Une facture client est généralement créée à partir d'une **commande client** ou d'un **bon de livraison**. Les détails de l'article du client seront récupérés dans la facture de vente. Cependant, vous pouvez également créer directement une facture de vente, par exemple une facture PDV.

Pour récupérer automatiquement les détails dans une facture de vente, cliquez sur Obtenir les articles à partir de . Les détails peuvent être récupérés à partir d'une commande client, d'un bon de livraison ou d'un devis.

Pour la création manuelle, procédez comme suit:

1. Accédez à la l**iste Facture de vente** et cliquez sur **:heavy_plus_sign: AJouter Facture de vente**.
2. Sélectionnez le **client**.
3. Définissez la **date d'échéance du paiement**.
4. Dans le **tableau Articles**, sélectionnez les **articles** et définissez les **quantités**.
5. Les **prix** seront **récupérés** automatiquement si le prix de l'article est ajouté, sinon ajoutez un prix dans le tableau.
6. La date et l'heure de publication seront définies sur actuelles, vous pouvez modifier après avoir coché la case sous Heure de publication pour effectuer une entrée antidatée.
7. **Enregistrer** et **envoyer**.

### 2.1 Options supplémentaires lors de la création d'une facture de vente 
**Facture d'acompte** : Si la case est cochée, alors cette facture deviendra une facture d'acompte.
**Est un avoir ? (Note de crédit)** : cochez cette case si le client a renvoyé les articles. Pour en savoir plus, visitez la page Note de crédit.

### 2.2 Statuts 
Ce sont les statuts qui sont automatiquement affectés à la facture de vente.

- **Brouillon** : un brouillon est enregistré mais n'a pas encore été soumis.
- **Soumis** : La facture est soumise au système et le grand livre a été mis à jour.
- **Payé** : le client a effectué le paiement et une entrée de paiement a été soumise.
- **Impayé** : la facture est générée mais le paiement est en attente mais avant la date d'échéance du paiement.
- **En retard** : le paiement est en attente au-delà de la date d'échéance du paiement.
- **Annulé** : la facture de vente est annulée pour quelque raison que ce soit. Une fois qu'une facture est annulée, son impact sur le compte et le stock est annulé.
- **Note de crédit émise** : l'article est retourné par le client et une note de crédit est créée en regard de cette facture.
- **Retour** : il est affecté à la note de crédit créée par rapport à la facture de vente d'origine. Cependant, vous pouvez également créer une note de crédit autonome.
- **Non payé et avec escompte** : le paiement est en attente et tout abonnement en cours a été réduit à l'aide de l'escompte sur facture .
- **En retard et avec escompte** : le paiement est en attente au-delà de la date d'échéance du paiement et tout abonnement en cours a été réduit à l'aide de l'escompte de facture.

### 3. Caractéristiques 
### 3.1 Dates 
- **Date comptable**: La date à laquelle la facture de vente affectera vos livres de comptes, c'est-à-dire votre grand livre. Cela affectera tous vos soldes au cours de cette période comptable.

- **Date d'échéance** : la date à laquelle le paiement est dû (si vous avez vendu à crédit). La limite de crédit peut être définie à partir de la fiche client.

### 3.2 Dimensions comptables 
Les dimensions comptables vous permettent d'étiqueter les transactions en fonction d'un territoire, d'une succursale, d'un client, etc. spécifiques. Cela permet d'afficher les états comptables séparément en fonction de la ou des dimensions sélectionnées. Pour en savoir plus, consultez l'aide sur la fonctionnalité Dimensions comptables 

### 3.3 Détails de la commande client
- **N° de commande fournisseur du Client** : suivez le numéro de commande reçu du client, principalement pour éviter la création de bon de commande ou de facture en double pour le même bon de commande reçu du client. Vous pouvez faire plus de configuration liée à la validation du numéro de bon de commande du client dans les paramètres de vente
- **Date de la commande client** : la date à laquelle le client a passé le bon de commande.

### 3.4 Adresse et contact 
- **Adresse du client** : il s'agit de l'adresse de facturation du client.
- **Personne à contacter** : si le client est une entreprise, la personne à contacter est récupérée dans ce champ si elle est définie dans le formulaire client .
- **Territoire** : Un territoire est la région à laquelle appartient le client, extraite du formulaire Client. La valeur par défaut est Tous les territoires.
- **Adresse de livraison** : Adresse à laquelle les articles seront expédiés.

### 3.5 Devise
Vous pouvez définir la devise dans laquelle la commande de facture de vente doit être envoyée. Cela peut être extrait de la fiche client ou des transactions précédentes telles que la commande client.

Souhaite sélectionner la devise du Client uniquement pour la référence du Client, tandis que la comptabilisation des comptes se fera uniquement dans la devise de base de la Société. Apprenez-en plus ici .
Tenir un compte client séparé dans la devise du client. La créance pour cette facture doit être enregistrée dans cette devise elle-même. Lisez la comptabilité multidevise pour en savoir plus.

### 3.6 Liste de prix 
Si vous sélectionnez une liste de prix, les prix des articles seront extraits de cette liste. Cochez la case **Ignorer la règle de tarification** ignorera les règles de tarification définies dans Comptes > **Règle de tarification**.

### 3.7 Le tableau des articles

- **Mettre à jour le stock** 
Cochez cette case pour mettre à jour le registre des stocks lors de la soumission de la facture de vente. Si vous avez créé un bon de livraison, le registre des stocks sera modifié. Si vous ignorez la création du bon de livraison, cochez cette case.

- **Scanner le code - barres** : vous pouvez ajouter des éléments dans le tableau des éléments en scannant leurs codes-barres si vous disposez d'un lecteur de codes-barres. Lisez la documentation pour le suivi des articles à l'aide du code-barres pour en savoir plus.

Le code de l'article, le nom, la description, l'image et le fabricant seront récupérés à partir de la fiche article .

- **Remise et marge** : vous pouvez appliquer une remise sur des articles individuels en pourcentage ou sur le montant total de l'article. Lisez Appliquer une réduction pour plus de détails.
- **Tarif** : Le tarif est récupéré s'il est défini dans la liste de prix et le montant total est calculé.
- **Livraison directe** : la livraison directe est lorsque vous effectuez la transaction de vente, mais l'article est livré par le fournisseur. Pour en savoir plus, visitez la page Drop Shipping .
- **Détails comptables** : Les comptes de revenus et de dépenses peuvent être modifiés ici que vous le souhaitez. Si cet élément est un actif , il peut être lié ici. Ceci est utile lorsque vous vendez un actif .
- **Revenu différé** : si le revenu de cet article sera facturé au cours des prochains mois en partie, cochez la case "Activer le revenu différé". Pour en savoir plus, visitez la page Revenus différés .
- **Poids de l'article** : les détails du poids de l'article par unité et UdM de poids sont récupérés s'ils sont définis dans la base d'articles.
- **Détails du stock** : les détails suivants seront récupérés dans le maître des articles:
- **Entrepôt** : L'entrepôt d'où le stock sera envoyé.
- **Qté disponible à l'entrepôt** : la quantité disponible dans l'entrepôt sélectionné.
- **Numéro de lot et numéro de série** : Si votre article est sérialisé ou mis en lots, vous devrez entrer le numéro de série et le lot dans le tableau Articles. Vous êtes autorisé à saisir plusieurs numéros de série sur une même ligne (chacun sur une ligne distincte) et vous devez saisir le même nombre de numéros de série que la quantité.
- **Modèle de taxe d'article** : vous pouvez définir un modèle de taxe d'article pour appliquer un montant de taxe spécifique à cet article particulier. Pour en savoir plus, visitez cette page .
- **Références** : Si cette facture client a été créée à partir d'une commande client / bon de livraison, elle sera référencée ici. De plus, la quantité livrée sera affichée.
- **Saut de page** : Créer un saut de page juste avant cet élément lors de l'impression.

### 3.8 Feuille de temps 
Si vous souhaitez facturer les employés travaillant sur des projets sur une base horaire (basée sur un contrat), ils peuvent remplir des feuilles de temps qui correspondent à leur taux de facturation. Lorsque vous effectuez une nouvelle facture de vente, sélectionnez le projet pour lequel la facturation doit être effectuée et les entrées de feuille de temps correspondantes pour ce projet seront extraites.

Si les employés de votre entreprise travaillent à un emplacement et qu'il doit être facturé, vous pouvez créer une facture basée sur la feuille de temps.

### 3.9 Utilisation des points de fidélité

Vous pouvez créer un programme de fidélité pour vos clients, ainsi vous pouvez convertir des points acquis lors d'achats précédents pour les utiliser. Plus d'informations à venir.

### 3.10 Taxes et frais 
Les taxes et frais seront récupérés à partir du bon de commande ou du bon de livraison .

Consultez la page Modèle de taxes de vente et frais pour en savoir plus sur les taxes.

Le total des taxes et frais sera affiché sous le tableau.

Pour ajouter automatiquement des taxes via une catégorie de taxe, visitez cette page .

Assurez-vous de marquer correctement toutes vos taxes dans le tableau des taxes et frais pour une évaluation précise.

**Règle d'expédition**
Une règle d'expédition permet de définir le coût d'expédition d'un article. Le coût augmentera généralement avec la distance d'expédition. Pour en savoir plus, visitez la page des règles d'expédition.

### 3.11 Remise supplémentaire 
Toute remise supplémentaire sur l'ensemble de la facture peut être définie dans cette section. Cette remise pourrait être basée sur le total général, c'est-à-dire après impôts / charges ou total net, c'est-à-dire avant taxes / charges. La remise supplémentaire peut être appliquée sous forme de pourcentage ou de montant. Visitez la page Application de la remise pour plus de détails.

### 3.12 Avances et acomptes
Pour les Articles de grande valeur, le vendeur peut demander un acompte avant de traiter la commande. Le bouton Obtenir des avances reçues ouvre une fenêtre contextuelle à partir de laquelle vous pouvez récupérer les commandes où le paiement anticipé a été effectué. Pour en savoir plus, visitez la page Saisie de paiement anticipé.

### 3.13 Termes de paiement 
Le paiement d'une facture peut être effectué en plusieurs parties en fonction de votre entente avec le fournisseur. Ceci est récupéré s'il est défini dans la commande client. Pour en savoir plus, visitez la page Conditions de paiement.

### 3.14 Perte 
La perte se produit lorsque le client paie un montant inférieur au montant de la facture. Cela peut être une petite différence comme 0,50. Sur plusieurs commandes, cela peut représenter un grand nombre. Pour des raisons d'exactitude comptable, ce montant de différence est **perdu**.

### 3.15 Termes et conditions 
Il peut y avoir certaines conditions générales sur l'article que vous vendez, celles-ci peuvent être appliquées ici. Lisez la documentation des conditions générales pour savoir comment les ajouter.

### 3.16 Paramètres d'impression 
En-tête de lettre 
Vous pouvez imprimer votre facture de vente sur le papier à en-tête de votre entreprise. En savoir plus ici .

«Regrouper les mêmes éléments» regroupera les mêmes éléments ajoutés plusieurs fois dans le tableau des éléments. Cela peut être vu lorsque votre impression.

**Imprimer les titres** 
Les en-têtes des factures de vente peuvent également être modifiés lors de l'impression du document. Vous pouvez le faire en sélectionnant un en- tête d'impression . Pour créer de nouveaux en-têtes d'impression, accédez à : Accueil > Paramètres > Impression > En-têtes d'impression.

Il existe des cases à cocher supplémentaires pour imprimer la facture de vente sans le montant, cela peut être utile lorsque l'article est de grande valeur. Vous pouvez également regrouper les mêmes éléments sur une seule ligne lors de l'impression.


### 3.19 Plus d'informations 
Les détails de vente suivants peuvent être enregistrés:

- **Campagne** : si cette facture fait partie d'une campagne de vente en cours, elle peut être liée.
- **Source** : Une source principale peut être étiquetée ici pour connaître la source des ventes.

### 3.20 Détails comptables 
- **Débit à** : Le compte sur lequel la créance sera comptabilisée pour ce client.
- **Est une entrée d'ouverture** : S'il s'agit d'une entrée d'ouverture affectant vos comptes, sélectionnez «Oui». c'est-à-dire que si vous migrez d'un autre ERP vers DOKOS en milieu d'année, vous souhaiterez peut-être utiliser une entrée d'ouverture pour mettre à jour les soldes de compte dans DOKOS.

**Remarques** : Toutes les remarques supplémentaires concernant la facture de vente peuvent être ajoutées ici.

### 3.21 Commission 
Si la vente a eu lieu via l'un de vos partenaires commerciaux, vous pouvez ajouter les détails de leur commission ici. Ceci est généralement extrait de la commande client / bon de livraison.

### 3.22 Équipe de vente 
Vendeurs: ERPNext vous permet d'ajouter plusieurs vendeurs qui peuvent avoir travaillé sur cette transaction. Ceci est également extrait de la commande client / bon de livraison.

### 3.23 Récupération automatique des numéros de lot d'articles 
Si vous vendez un article à partir d'un lot , ERPNext récupérera automatiquement un numéro de lot pour vous si "Mettre à jour le stock" est coché. Le numéro de lot sera récupéré sur la base du premier expirant, premier sorti (FEFO). Il s'agit d'une variante du premier entré, premier sorti (FIFO) qui donne la priorité la plus élevée aux éléments expirant le plus tôt.

Notez que si le premier lot de la file d'attente ne peut pas satisfaire la commande sur la facture, le prochain lot de la file d'attente qui peut satisfaire la commande sera sélectionné. Si aucun lot ne peut satisfaire la commande, ERPNext annulera sa tentative de récupération automatique d'un numéro de lot approprié.

### 3.24 Après la soumission 
Lors de la soumission d'une facture de vente, les documents suivants peuvent être créés en regard de celle-ci:

- **Entrée de journal**
- **Saisie de paiement**
- **Demande de paiement**
- **Remise sur facture**
- **Bon de livraison**
