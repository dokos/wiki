---
title: v2.0.0
description: 
published: true
date: 2021-05-14T16:58:03.432Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:26:27.994Z
---

# V2.0.0

> Dokos v2.0.0 is based on ERPNext version 13
{.is-info}


## Accounting

- :rocket: Immutable General Ledger: General ledger entries can no longer be deleted
- :rocket: Balance sheet is now formatted according to french standards for french companies

- :airplane: Automatic deduction of VAT in GoCardless invoices
- :airplane: Repartition of payments according to the invoice payment terms
- :airplane: Possibility to add and configure accounting journals for each doctype
- :airplane: Default autoname by fiscal year for GL Entry changed to be based on fiscal year
- :airplane: Feature to create distributed cost centers
- :airplane: New option to create down payment invoices

- :helicopter: Feature to make accounting journal adjustments for accounting transcations
- :helicopter: New report to calculate monthly recurring income (MRR)
- :helicopter: Validation and update of child company accounts on renaming / addition of an account.
In multi-company mode, if we rename an account it renames the same account in the child companies
- :helicopter: Possibility of grouping by customer or supplier in accounts receivable / payable reports
- :helicopter: Ability to create journal entry templates
- :helicopter: A default account is now configurable for interbank transfer entries in the Company page
- :helicopter: New function to create Dunning
- :helicopter: New report to process statements of accounts
- :helicopter: New report _Transactions tax summary_ to summarize sales/purchases transactions taxes
- :helicopter: New hook to get accounting dimensions from custom applications
- :helicopter: Configurable accounting dimensions
{.grid-list}
<details>
<summary>And also...</summary>
  
- Option to manually set a payment entry or journal entry as reconciled
- OFX bank transactions import enhancements
- Hidden columns added with account number to add them in CSV/XLSX exports
- Sepa direct debit in accounting Workspace
- Added column cost_center to receivable reports
- Update onboarding for accounts
- Technical change to handle Opening Invoices creation
- Remove unused type:"General" option removed in mode of payment
- update accounts onboarding
- Add tour  functionality for Account Settings
- New filter field in Bank Reconciliation Statement
- added option to add custom remarks in payment entry
- Add a link to final invoice in down payment invoice
- create_payment_entry api in subscription
- Get the next invoice date in the subscription through the method of ‘get_next_invoice_date’
- Add Summary in accounting reports
- Cost center renaming needs to be done through a dedicated dialog
- Feature to add a description in tax templates that will be fetched in invoices and displayed on the print format
- Credit controller role users are notified with credit limit extension
- Allow Rename for Tax Category

</details>


## Buying

- :helicopter: Possibility to compare supplier quotations
- :helicopter: Feature to scan barcodes in purchase receipts
- :helicopter: Possibility to manage several unit of measures in requests for quotation
{.grid-list}
<details>
<summary>And also...</summary>
  
- (RFQ/SQ) Link to Material Requests in Tools section
- Supplier Email Preview in RFQ
- Supplier Quotation UX fixes
- Link between blanket order and quotations
- Checkbox in supplier document to allow creating purchase invoice creation without purchase order
- Provision to make a request for quotation against an opportunity
- The margin on price list is now displayed in buying transactions
- Basic validation for indian PAN No. in Supplier
- The rate of stock unit of measure is now captured in purchase transactions

</details>

## CRM

- :helicopter: New Video doctype
- :helicopter: Youtube integration
- :helicopter: Possibility to use the Jinja language in contract templates
- :helicopter: LinkedIn and Twitter integrations
{.grid-list}

<details>
<summary>And also...</summary>
  
- Opportunity amount auto calculation when items are added
- Lead status is changed to Open if next contact is today
- Link to customer in call log
- Added phone field in product Inquiry
- Calendar view in opportunity
- Sales stages can now be renamed
- Add Print Language to Lead and Opportunity
- Additional CSS for the event doctype
- Improved call log doctype
- New doctype : Voice Call Settings 
  
</details>

## Dodock Framework

- :rocket: New dashboards for each module
- :rocket: New workspaces
{.grid-list}
- :helicopter: New hook to override a doctype class (controller) through a custom application
- :helicopter: New control (UI component) to set a duration
- :helicopter: Event Streaming feature to synchronize two Dodock instances
- :helicopter: Dodock is now compatible with Python 3.8 and Python 3.9
	* · Dodock will not be compatible with Python 3.6 in a future minor version
- :helicopter: Theme switcher (CTRL + SHIFT + G)
- :helicopter: Contextual help available in some documents (_Tour_ help menu)
- :helicopter: New Hook to set a custom website theme
- :helicopter: All doc methods called from the client must now be whitelisted
- :helicopter: Custom scripts have been renammed Client Script
- :helicopter: Client scripts can now be extended to the list view
- :helicopter: Guests can now be granted an access to the desk with limited permissions
- :helicopter: Enhanced system to seal documents that cannot be deleted
<details>
<summary>And also...</summary>
  
- In reports, hidden columns are now exported when a user exports a report to CSV or XLSX
- New feature to add a cover page above a print format in PDF
- API that validate an SQL backup file before starting a site restoration
- Added filters and order_by fields in frappe.get_last_doc API
- Option on docfields to avoid having negative values in Int, Float and Currency fields
- Gantt view settings now overrides the calendar settings
- The `get_roles` API is now whitelisted (available through the client API)
- Enhanced `bench execute` command, It Allow  to execute anything console would allow
- New feature to write console requests directly in the user interface instead of having to use the command line interface
- Feature to show a bigger dialog for `frappe.msgprint` and `frappe.throw` API
- Allow sending emails via Server Script
- New form grid layout
- New API to validate and sanitize search inputs for queries
- New API to handle tags
- Google Python API updated to a newer version
- site_config.json file is now included in backups
- Option to add a `mute_notifications` key in site_config.json to mute notifications sent from a site
- New option to send the parameter `no_backup` in the `remove_app` API
- 'md', 'json', 'js', 'xml' extensions added in Jinja templating rendering discovery function
- New hook `override_whitelisted_methods` to override open_mapped_docs functions
- Upgrade Datatable to v1.15.3
- Feat to Add filepreview to attach control
- Integrated rate limiting functionality
- Installed apps data is now stored in a single DocType
- `frappe.bold` has been added to safe globals form server scripts
- New parameter in the Communication Medium DocType
- Option to configure CORS headers in the server configuration file
- Option to show absolute values in print formats
- Checkbox added in some doctypes to show absolute values in print formats
- Possibility to create a new File Grid View
- New `as_list` paramater option in `frappe.msgprint` API
- New `lang` parameter in `format_date`, `format_datetime` and `format_date` API
- Refactored SCSS files structure
- Compress private and public files during backup
- Add icons for salesforce and fairlogin
- Autoprefixer library to parse css
- Technical to fix issues connecting with the mobile application
- Major changes to the Search API
- Dodock now allow passwords longer than 140 characters
- Country informations for Congo
- New API `frappe.utils.time_diff_in_minutes`
- New API to fetch backups
- New option to refresh all charts and cards in dashboard
- New docker images build automatically for each release
- Add webhook hmac verification
- Logs are now registered for each site independantly on the server
- Possibility to run the build command for each app independantly
- Possibility to disable comments on the website
- Possibility to adjust the scheduler minimum interval (60 seconds by default until now)
- Embedded monitoring functionality
- New `before_validate` event in server scripts
- Icons in leaderboard is Already written in Dodock
- Add global defaults to settings
- Support added for date format DD-Mon-YY in data import tool
- Option to make a custom permission query via a server script
- Options to add conditional events in Event Streaming functionality
- New doctype : Bench list-apps API now returns all applications installed on all sitesBench list-apps API now returns all applications installed on all sites
- Use default social icons
- Add icons for social logins
- Set default provider icons
- New option to pass a `lang` (language) parameter in `global_date_format` to return a localized date
- Warning issued when a database restoration is done on an older version of Dodock
- Possibility to backup and restore a postgreSQL site
- LDAP3 requirements added
- LDAP passwords can now be reset from user settings
- 2 factor authentication can now be enabled for LDAP users
- New system settings to logout user on password change
- Emails can now be appended to any doctype without needing to configure a hook
- Server scripts can now be scheduled for later execution
- Source maps for css/scss/less files
- New functonality to create virtual doctypes
- Feature to lazy import python modules
- Refactored Sealing system
- Provision to rebuild Nested Set from treeview
- Set default site using FRAPPE_SITE env var
- Hide Child Records for a Nested DocType via User Permissions
- Util to get datetime in specific timezone
- New `before_commit` added in database transactions
- Option to display event time and event end in a Calendar View, through a custom calendar view 
- Option to display event time and event end in a Calendar View, through a custom calendar view 
- Option to make public files available on the event portal
- Enhancements in the calendar view
- Allow html in email templates
- New button in "Customize Form" to quickly customize a child table
- New option in System Settings to remove EXIF data in images file before uploading them on the server
- Translate kanboard board title
- Ability to add a custom message during uploads via the open_mapped_doc API
- Portal to allow users to register for an event
- New API Allow skip or backup only specific DocTypes during backups
- New position for the scroll to top button
- New breadcrumbs rule for print page:  Users can now go back to the document using these new breadcrumbs
- New option to add links and actions to a form directly in the customize form view
- Enhanced print formal builder style
- Form print previews are now displayed in a separate route
- New API to reset a module onboarding section
- Option to make an assignment rule based on the value of a field
- Option to put the currency symbol left or right of the currency value. The "In words" value format has also been enhanced.
- New doctype to change settings related to several log documents
- New `frappe.utils.make_chart` to create a chart with arguments
- Updated icon for social media integrations
- Enhanced style for the form tour functionality
- Enhanced styles for onboarding cards
- New icons in the Leaderboard page sidebar
- New routing logic for the Onboarding steps
- New `frappe.Plyr` API to display embedded videos
- Possibility to add embedded videos in the Onboarding cards
- New color pairs added to the standard colors variables for the light and dark themes
- Enhanced Role Permission Manager functionality
- Event title are now displayed in tooltips in calendar events
- Option to add a custom css class when creating a widget group
- New Assignment Rule: Option to set custom due_date for assignments
- Add Gantt Rebranded Styles
- Enhanced Background jobs page: add empty state for background jobs
- Refactor background jobs
- Enhanced structure for the "Download Backup" page
- New color picker UI component
- New button to apply filters in the Filters UI component
- New chevron icon to open/close a section chevron for forms
- Login page enhancements
- Add spacing after form section head
- The system will name the document based on the rules
- Icons for the "empty state" for grids and borders
- Sidebar is now hidden when seeing the Kanban view
- Show theme change alert for 3 seconds only
- Add filters to calendar view
- Flip avatar colors in dark color mode
- User: Add hourly limit for password reset requests
- Updated Image view
- Use small icon in avatar group action
- Possibility to pass an `icon` parameter in the `get_icon_label` function of the page API
- An icon has been added in the `Change User` button in the user profile page
- A class `hover-active`has been added on parent nodes in the Tree View to display the node in a different color when hovered
- Possibility to add an icon for each desk card
- New `Customize` button added on workspaces to allow end users to customize each workspace
- Update awesomebar focus state background
- Update kanban card title weight
- Possibility to display multiple roots in tree view
- New option to send a notification to all assignees on a document
- Remove unused status field from contacts
- Reduce border radius of dropdown item
- New layout for user stats in sidebar
- Update sidebar avatar and layout
- Delete old icons
- Add icons for user profile
- Group similar item like these together
- Update dropdown item spacing
- Update kanban title
- Update new assignment button for kanban
- Update avatar style
- Overlap avatar in form sidebar share
- Icons added in the view selection dropdown (in list view)
- Allow icon in dropdown
- Add new small file icon
- Add dashboard icon
- Enhanced notification panel in the navbar: remove slide animation on notifications
- Add null states to notification and events
- Feature to allow different fieldnames for setting a calendar recurrence
- Adjust font-size for checkboxes
- Hide primary button label in mobile if icon is present
- Show only create button for null state in mobile
- Text muted for action items in timeline message
- Style improvement for the close icon: override icon close fill globally
- New option to disable custom script
- Link to send an email to a document added in the timeline
- Timeline will be updated realtime whenever docinfo gets updated
- Feature to set a flexible date field in calendar recurrence
- New UI fixes and features
- New get_link_to_doctype API in contact
- Server script: Allow frappe.db.sql for read
- Allow get_print and attach print in server scripts
- Controls/text_editor: resize image for markdown editor
- Feature to add features and filters in reports via the Report DocType
- Prepared Report: System Setting to automatically delete old prepared reports
- Bulk restore documents in Deleted Document List view
- Feature to set linked doctypes to ignore when cancelling a document
- SMS Channel added but Whatsapp channel removed sinced
- Report: Ability to show child table row index number
- Feature to add document version info and share information to the new document timeline
- Improvements to auto repeat
- The navbar can now be configured via a DocType: Navbar Settings
- New API in the event doctype: add controller methods to add participants to event
- New timeline and print preview
- Possibility to hide modules based on seleted domain
- New timeline in doctypes
- Possibility to create custom number cards
- Dashboards can be synced from JSON files in a custom application
- Fullcalendar library updated to v5
- Frappe Charts updated to v2
- Date range picker in leaderboard
- New hook: `additional_timeline_content`
- Role permission: If desk access is removed from a role, attempt to remove it from users too
- Site doesn't send a notification if a communication is created from an email
- A new button is available in the menu of each doctype if a tour is available
- Show users currently composing an email on a form
- Custom field created by administrator can only be deleted by administrator
- New feature to test a notification when creating it
- Set default site using FRAPPE_SITE env var
- Show phone icon if phone call handler is available
- Set the average response time in the parent documentation of a communication
- Show warning if documents have workflow states that do not exist in the workflow
- Listview can now be configured from the menu > listview settings
- Report summary API: Displays the most important metrics for each report above the graph
- New refactored dashboards
- Possibility to minimize message popups
- API to indicate PDF orientation
- Email and phone numbers can now automatically be validated for data fields if Email or Phone are added as options
- New calendar view for ToDo
- Documents are not saved anymore if there has been no update
- New report column attribute disable_total
- Preview popups can now be configured from "Customize form"
- Code fields wen now be expanded/collapsed
- Email dialog can now be minimized
- Module-wise onboarding
- New calendar view in notifications
- Possibility to create custom workspaces
- Redesigned multiselect dialog
- Ability to filter to import emails in email groups
- The autoname rule define custom can be set in Customize form
- Add option to make assignments via assignment dialog
- Show images in Kanban
- Placeholder for Select fields
- Enhancements in newsletters
- Replace localStorage with localforage for storing drafts
- Rename "Custom Script" to "Client Script"
- Add copy_doc to safe_exec globals
- New hook for custom css in other applications
- Ability to attach photo from webcam
- Ability to set default desk page
- Feature to create and assign module profiles
- Add icon to timeline action button
- The email account signature is now fetched in email dialog
- Avatars are added in energy point log email for top performers
- Enhanced email style: Add with_container argument and update email style
- Update standard email template to match new style
- Feature to auto-update email groups
- New workspace "Build"
- API change to show or hide the permission alert dialog
- New permission "Select" allowing only to select a document without Reading it
- Possibility to click on the breadcrumb to copy the document's name
- config.py files are have been removed in favour of the new Workspace DocType
- Desk Page has been renamed to Workspace
- New theme switcher and add theme switcher to navbar
- The document name is now available in breadcrumbs
- Workflow transition condition allows datetime functions
- A desk access can be given to guests
- New feature to toggle sidebar in lists and documents
- New theme switcher
- Assign to dialog now shows user images
- Data pill can now have images
- The color picker swatch can be selected via keyboard
- A button allows to show/hide activity related elements in timeline
- New routing system with prettier URLs
- New routing system with prettier URLs
- A desk access can now be given to guests
</details>

## Human Resources

- :helicopter: New function to automatically allocate leaves based on leave policy
- :helicopter: French bank holidays can be automatically added to the holiday list for french companies

- :helicopter: Add Leave Type option: Is partially paid leave
- :helicopter: New DocType: Appointment Letter
- :helicopter: In and Out time for employees are now added to Attendance documents
- :helicopter: Expense approvers can now be configured by department
{.grid-list}

<details>
<summary>And also...</summary>
  
- New alert to let the validator know that an expense claim will be submitted with a sanctionned amount equal to 0
- JSON download for HSN wise outward summary
- Skip attendance for holidays from Upload Attendance
- New reports Employee leave balance
- Monthly attendance sheet enhancements
- Recruitments analytics
- Add checkbox for disabling leave notification in HR Settings
- Audit tracking has been activated in HR Settings

  
</details>
  
## Integrations

- :rocket: New Woocommerce connector:
{.grid-list}
	* · stock management
  * · VAT and miscellaneous fees
  * · automatic Webhooks configuration
  * · slaes invoices, delivery notes and payments created automatically
- :helicopter: Stripe payments can now be immediately processed from a payment request
- :helicopter: Stripe Webhookds can now be created/deleted from Stripe Settings
- :helicopter: Stripe subscriptions can now be handled by Dokos only without having to create subscription plans on Stripe
- :helicopter: Integration with TaxJar

<details>
<summary>And also...</summary>
  
- Razorpay API changed to use the Razorpay python API instead of direct HTTP requests
- Possibility to disable a payment gateway
- Dodock now uses a newer version of the Stripe API (2.49.0)
- Integration with Paytm payment gateway
- Possibility to link a generic Oauth2 client
- Add WooCommerce different status handling + stripe fees in Woocommerce Integration refactor
- Sync old shopify orders
- Add Workspace  in integrations page
- Add integrations workspace to be integrated in a more global "New Workspaces" category
- Add bank account field in GoCardless Settings
- Possibility to create Stripe invoice item in subscription plans
- Refactored WooCommerce integration: Shipping address and contact can be saved in WooCommerce customer
- Payment request number added to GoCardless metadata
- Report: GoCardless payments
- Feature to relink a bank account with Plaid when the link has expired
</details>


## Loans

- :rocket: New **Loan Management** module
- :helicopter: New Loans dashboard
{.grid-list}

<details>
<summary>And also...</summary>
  
- Utility function to get possible loan disbursal amount
- Enhancement in Loan Topups  

</details>

## Manufacturing

- :helicopter: New feature to flag items a supplier must source in bill of materials
- :helicopter: Production Planning Against Sales Order/Material Request/Work
- :helicopter: Production forecasting using exponential smoothing method
- :helicopter: Possibility to create BOM templates
{.grid-list}

<details>
<summary>And also...</summary>
  
- Sales order status filter added for production plan
- Option to open/close a production plan
- Added sequence id in routing for the completion of operations sequentially
- Allow BOM to use same item as raw material
- MRP at parent level in the production plan

</details>

## Payroll

- :rocket: New **Payroll** module
- :helicopter: Payroll based on employee cost center
- :helicopter: Multi-currency management
{.grid-list}

<details>
<summary>And also...</summary>
  
- Income tax slab
- Payroll based on attendance
- Lower deduction certificate doctype
- Additon of leave details in Salary Slip
- Option to compute year to date in salary slip components
  
</details>


## Printing

- :helicopter: Possibility to add new print options in transactional documents
- :helicopter: New option to print unit of measure after quantity
{.grid-list}

<details>
<summary>And also...</summary>
  
- New print style: Twenty Twenty
- Enhanced loading state for print preview
- Print previews are now rendered in a iFrame to avoid style conflict with the site  
- Enhanced print format column layout
- Contextual setting for print uom after quantity
- Hook to fetch additional print settings in the print_controller
</details>



## Project

- :helicopter: Creation of dependant tasks via project templates
{.grid-list}

<details>
<summary>And also...</summary>
  
- All hours are now billed by default in timesheets
- Dashboard for timesheet
  
</details>

## Quality

- :helicopter: Possibility to create quality inspection documents linked to job cards
- :helicopter: Possibility to add calculation formula in quality inspection documents
- :helicopter: Quality inspection rules can be value based or numeric
{.grid-list}

<details>
<summary>And also...</summary>
  
- Introduce parameter group in quality inspection  
  
</details>


## Regional

- :helicopter: New UAE VAT 201 report
- :helicopter: E-invoices for India
{.grid-list}

<details>
<summary>And also...</summary>
  
- Normal rounding for GST Taxes
- Option make account number length configurable for the DATEV report 
- Fiscal year validation for DATEV report
- Add address template for luxembourg
- New regional module for Turkey
- Regional address templates are now centralized
- New feature to add gratuity in payroll for India and UAE
- Remove german sales invoice validation
- Temporary against account in DATEV report
- Separate equity tree in CoA SKR04
 
</details>

## Point of Sale

- :helicopter: New page for an enhanced point of sale feature
- :helicopter: New intermediary document: POS invoice, consolidated once a day in sales invoices
- :helicopter: POS invoices are sealed to comply with french regulation
{.grid-list}

## Selling

- :helicopter: Option to define a contact as billing contact in the Contact form
- :helicopter: Feature to add conditions in pricing rules
- :helicopter: New feature to configure discounts on early payments
- :helicopter: Recursive pricing rules (buy 1 get 1, buy 2 get 2,etc...)
- :helicopter: Feature to fetch timesheets from sales invoices
{.grid-list}

<details>
<summary>And also...</summary>
 
- Added project in Sales Analytics report
- The payment request subject is now displayed on the payment portal
- Enable total row in Gross Profit Report
- Add Maintenance Visit link in Customer and Sales Order dashboards
- Territory Wise Treeview In 'Customer Acquisition and Loyalty' Report
- Gross profit is now calculated in quotations line items
  
</details>

## Stock

- :helicopter: New Shipment doctype to configure the parameters for a shipment
{.grid-list}
	* · Prepares the software for an integration with external shipment gateways 
- :helicopter: The balance of each serial number is now displayed in the stock ledger
- :helicopter: New Putaway function to create automatic assignment rules warehouses
- :helicopter: Enhanced return tracking for purchase receipts/delivery notes
- :helicopter: New report for stock quantity vs serial n° count
- :helicopter: Multi currency management in landed cost vouchers
- :helicopter: Item valuation reposting for backdated transactions

<details>
<summary>And also...</summary>
  
- Validate if removed item attributes exist in variants
- New rule for querying warehouses: sort warehouses based on item quantity in descending manner
- Material Request and Stock Entry Enhancement
- Inter warehouse stock transfer on valuation rate with taxation
- Auto set batch no on serial no selection
- Total quantity and dashboard added to Batch
- Show actual qty for warehouse in sales invoice
- Perpetual inventory is now disabled by default  
 
</details>


## Support

- :helicopter: New report: Issue analytics
- :helicopter: New report: Issue summary
{.grid-list}

<details>
<summary>And also...</summary>
 
- Disabled customers are not selected anymore in Issue doctype in customer field
- Added search to support page
- Issue Metrics and SLA Enhancements 
  
</details>

## Telephony

- :rocket: New **Telephony** module (technical module)
{.grid-list}

## Venue

- :rocket: New **Venue** module
- :helicopter: Item bookings
- :helicopter: Booking credits
- :helicopter: Feature to create booking credit rules (automation of booking credit addition/deletion)
- :helicopter: Option in Venue Settings to set an item booking as confirmed only if the payment is processed
- :helicopter: Possibility to make item bookings linked to events
- :helicopter: Possibility to create event slots to allow users to register for a slot
- :helicopter: Item booking dates are modified according to changes done in events
{.grid-list}

<details>
<summary>And also...</summary>
 
- New option to decide up to when an item booking can be cancelled on the portal
- New options Next first day of the week and Next first day of the month in the Posting Date Rule of the Item Booking Credit Rule DocType
- Switch available in Venue Settings to block overlaps in Item Bookings on the Desk (it was alreay handled on the portal)
- "New expiration rules in Item Booking Credit Rules to handle the following case:
  {.grid-list}
  * - Last day of the month
  * - Last day of the year"
- Don't deduct booking credits when pricing rules is applied
- Recurring booking credit rules
- New tool to bind quotations/sales orders and item bookings
- New Report: Item booking rate  
  
</details>

## Website and portal

- :helicopter: New support page on website
- :helicopter: New portal for subscription management
- :helicopter: New standard website theme called _Dokos_
- :helicopter: New _cart_ page
{.grid-list}

<details>
<summary>And also...</summary>
 
- Portal UI enhancement
- Item booking can be deleted directly from the portal calendar before validating the shopping cart
- Allow system user access to the portal
- Improvements in the shopping cart page  
- API to send the result of a document created by a webform back to the webform for further processing
- Possibility to set conditions on webform fields to handle how they are displayed
- Option to anonymize ip for Google Analytics
- Update layout and doctype for about us settings
- When error messages are displayed on the website, the stack trace is now hidden by default
- Refactored Login page: Add social options to login design
- Website enhancements
- New web template module to be able to create web templates to use in web pages
- Add dynamic routes to web page
- Option to set a custom signup page
- Portal and website UI enhancement
- New portal to see event details
- New option to add a custom header actions button in portal lists
- Add home page to website
- Minor: Allow web form to follow document permissions
- Option to give a feedback on help articles
- Add H1 tag to About and Contact pages for SEO
- Improved site SEO: Add canonical head link for SEO
- Unicons library is now available on the website
- New layout for the documentation page on the website
- Improve breadcrumbs and website search description
- Make logo optional in testimonial
- Custom JS and CSS can now be added to web forms
- Integration with Google Indexing API
- It is now possible to enable/disable comments on the blog
- Website tracking is now embedded
- Portal sidebar items are now added to the mobile navbar
- Google Search Preview option in blog
- Social media sharing in blog posts
- Update avatar on website navbar
  
</details>


