---
title: Introduction à DOKOS
description: 
published: true
date: 2022-12-13T09:13:47.355Z
tags: 
editor: markdown
dateCreated: 2021-05-08T09:13:57.852Z
---

# Introduction
## 1. Qu'est-ce qu'un ERP? 

Si vous êtes déjà convaincu que vous avez besoin d'un logiciel tout-en-un pour votre organisation, vous pouvez ignorer cette page.

ERP signifie en anglais **Enterprise Resource Planning**. Un ERP est un logiciel de gestion d'entreprise utilisé pour **collecter**, **stocker**, **gérer**, **interpréter les données des activités commerciales**, **appliquer les processus**, **activer les mécanismes de contrôle et automatiser une gamme d'activités**.

### 1.1 Historique
Les entreprises manufacturières doivent planifier et gérer leurs ressources telles que les matières premières, les machines et la main-d'œuvre pour répondre aux demandes des clients. Dans les années 1960 et 1970, les entreprises ont commencé à utiliser des logiciels pour mieux planifier et gérer les ressources. 

Ces logiciels étaient appelés Planification des besoins en matériel (MRP ou MRP I). Cette planification des ressources de fabrication (MRP ou MRP II) a évolué de nouvelles fonctionnalités sont intégrées telles que la prévision, la planification de la maintenance et la comptabilité financière. 

Plus tard, d'autres fonctionnalités telles que la gestion des ressources humaines, la planification de projet ont été ajoutées par les fournisseurs MRP dans le but d'offrir un logiciel tout-en-un, et ce logiciel a été nommé Enterprise Resource Planning (ERP). Alors que les ERP ont leur racine dans l'industrie manufacturière, aujourd'hui les entreprises de toutes sortes les utilisent.

**Que vous soyez une petite organisation, moyenne, grande, énorme ou gigantesque, vous pouvez avoir besoin d'un ERP.**


## 2. Pourquoi les entreprises / organisations utilisent-elle un ERP ?

### 2.1 Source unique de vérité

Si l'équipe commerciale de votre entreprise utilise un seul logiciel pour gérer les ventes et que l'équipe comptable utilise un logiciel comptable distinct, cela peut entraîner des problèmes. 

L'équipe comptable doit dupliquer les données client et les mettre à jour en permanence pour les maintenir synchronisées avec les données du logiciel de vente. Et en fonction de la personne que vous demandez, vous pouvez obtenir une réponse différente pour la même question, comme l'adresse de facturation d'un client spécifique. 

Les systèmes ERP éliminent cela car ils agissent comme une base de données centrale unique (source unique de vérité). 

Cela se traduit par une efficacité et une précision des données. Vous pouvez obtenir des informations sur votre entreprise via des rapports et des analyses.

### 2.2 Appliquer les mécanismes de processus et de contrôle

Pour que votre organisation fonctionne de manière transparente à grande échelle, vous devez appliquer certains processus et créer des mécanismes de contrôle. Par exemple, chaque offre doit être approuvée par un directeur des ventes, puis par un directeur de comptes et si la valeur totale des articles proposés est supérieure à un certain seuil, elle doit être approuvée par le PDG. Vous pouvez facilement appliquer ces processus via des ERP. Voici un exemple d'un autre processus: Un produit spécifique doit passer par un contrôle de qualité et respecter les paramètres de qualité avant d'être accepté et payé. Celles-ci sont appelées «règles métier» et les ERP facilitent l'application des règles métier.

### 2.3 Automatisation des actions

Vous pouvez automatiser une gamme de choses via des ERP. Par exemple, vous pouvez configurer votre ERP pour préparer une commande d'achat et l'envoyer au fournisseur préféré si le niveau de stock d'un article spécifique tombe en dessous d'un seuil. 

Voici un autre exemple, vous pouvez configurer votre ERP pour attribuer automatiquement les prospects au bon vendeur en fonction de certaines règles.

Les ERP évoluent continuellement dans leur quête pour devenir le logiciel tout-en-un. Par exemple, les entreprises antérieures devaient se procurer et gérer un système distinct si elles voulaient vendre leurs produits en ligne. Mais les systèmes ERP modernes comme notre ERP DOKOS offrent de telles capacités.

Qu'est-ce que DOKOS ? 
DOKOS est un ERP complet qui aide votre entreprise à enregistrer toutes les transactions commerciales dans un seul système. Avec DOKOS, vous pouvez prendre des décisions éclairées, factuelles et opportunes pour rester en tête de la concurrence. Il sert de colonne vertébrale à votre entreprise en pleine croissance en ajoutant force, transparence et contrôle.

## 3. DOKOS vous accompagnera à:

- Suivre toutes les factures et paiements.
- Savoir quelle quantité de produit est disponible en stock.
- Identifier et suivre les indicateurs clés de performance (KPI).
- Identifier les requêtes ouvertes des clients.
- Gérer la paie des employés.
- Attribuez des tâches et suivez-les.
- Maintenez une base de données de tous vos clients, fournisseurs et contacts.
- Préparer des devis.
- Gérer des lieux (réservation de salles de réunion...)
- Suiver vos budgets et vos dépenses.
- Déterminez le prix de vente effectif en fonction de la matière première réelle, des machines et du coût de l'effort.
- Recever des rappels sur les calendriers de maintenance.
- Publier votre site Web.

