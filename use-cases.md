---
title: Cas d'usage
description: 
published: true
date: 2022-03-18T14:52:17.462Z
tags: 
editor: markdown
dateCreated: 2021-09-02T18:36:27.977Z
---

# Cas d'usage

Dans cette rubrique vous retrouverez des cas d'usage qui seront publiés régulièrement et qui pourront vous servir d'exemple. L'équipe Dokos mettra en avant des aspects techniques du logiciel que vous pourrez reprendre pour votre activité.

## 1. Présentation des cas d'usages

- [Pour les entreprises : Dokompany](/fr/use-cases/dokompany)
- [Pour les Tiers-Lieu : MappeMonde](/fr/use-cases/mappemonde)

{.links-list}

## Les différents cas d'usages

- [Rapports SQL](/fr/customizations/use-cases/query-report)
- [Rapports de script](/fr/customizations/use-cases/script-report)
- [Création d'une revue de presse](/fr/use-cases/revue-de-presse)
- [Ajouter / Masquer des champs dans les formulaires](/fr/customizations/use-cases/customization-form)
- [Exonération de TVA](/fr/customizations/use-cases/exemption-vat)
- [Changer la numérotation des factures](/fr/customization/use-cases/invoice-number)
- [Ajouter un champ Statut d'envoi d'email](/fr/customizations/use-cases/email-sent)


{.links-list}