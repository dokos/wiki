---
title: Paramètres
description: Découvrez notre Outil de gestion pour les TPE, PME et Tiers Lieux. Documentation des paramètres de configuration. ERP français Open Source. Accompagnement sur mesure.
published: true
date: 2022-10-10T14:17:18.519Z
tags: 
editor: markdown
dateCreated: 2021-05-10T10:15:05.849Z
---

# Paramètres

Pour réussir la mise en œuvre d'un ERP, la personne chargée de la mise en place de l'outil de gestion doit prévoir du temps et effectuer la mise en œuvre avec dévouement. Le temps pour le projet de mise en place d'un ERP doit bien être pris en compte.

Vous trouverez les options de configuration globale ici, pour une configuration par module, visitez les modules respectifs.

## Paramètres de Dokos

- [Paramètre Société](/fr/settings/system-settings)
- [Paramètre Impression](/fr/settings/print/print-settings)
- [Paramètre des Ventes](/fr/selling/selling-settings)
- [Paramètre du portail](/fr/website/portal-settings)
- [Paramètre de comptabilité](/fr/accounting/accounts-settings)
- [Paramètre d'Achat](/fr/buying/buying-settings)
- [Paramètre de Production](/fr/manufacturing/manufacturing-settings)
- [Paramètre du Site web](/fr/website/website-settings)
- [Paramètre des Stocks](/fr/stocks/stock-settings)
- [Paramètre du Support](/fr/support/support-settings)
- [Paramètre RH](/fr/human-resources/hr-settings)
- [Paramètre du panier](/fr/website/shopping-cart-settings)
- [Valeurs par Défaut Globales](/fr/settings/global-defaults)
{.links-list}

## 1.1 Configuration de base

- [1. Configuration de la société](/fr/settings/company)
- [2. Configurer les taxes](/fr/settings/account)
- [3. Valeurs par défaut global](/fr/settings/global-defaults)
- [4. Paramètres du système](/fr/settings/system-settings)
- [5. En-tête de document](/fr/settings/print/letter-head)
- [6. Définition de l'objectif de vente de l'entreprise](/fr/settings/setting-company-sales-goal)
{.links-list}

## 1.2 Gestion de données 

- [1. Importations des données](/fr/settings/data-import)
- [2. Exportations des données](/fr/settings/data-export)
- [3. Téléchargement des sauvergardes](/fr/settings/backup)
- [4. Importations des plans comptables](/fr/settings/chart-of-accounts-importer)
{.links-list}

## 1.3 Email / Notification

- [1. Paramètrage des emails](/fr/getting-started/email-setup)
- [2. Notification](/fr/settings/notification)
{.links-list}


## 1.4 Utilisateur et autorisations
- [1. Ajouter un utilisateur](/fr/settings/users-and-permissions/user)
- [2. Rôle et profil de rôle](/fr/settings/users-and-permissions/role)
- [3. Autorisations basées sur les rôles](/fr/settings/users-and-permissions/permission-manager)
- [4. Autorisations des utilisateurs](/fr/settings/users-and-permissions/user-permissions)
- [5. Autorisation de rôle pour la page et le rapport](/fr/settings/users-and-permissions/role-permission-for-page-and-report)
- [6. Partager un document](/fr/settings)
- [7. Utilisateur limité](/fr/settings/users-and-permissions/limited-user)
- [8. Administrateur](/fr/settings/users-and-permissions/administrator)
{.links-list}

## 1.5 Impression des documents

- [1. Paramètres d'impresssion](/fr/settings/print/print-settings)
- [2. Format d'impression](/fr/settings/print/print-format)
- [3. Style d'impression](/fr/settings/print/print-style)
- [4. Titre d'impression des documents](/fr/settings/print/print-headings)
- [5. En-tête de document](/fr/settings/print/letter-head)
- [6. Modèle d'adresse](/fr/settings/print/address-template)
- [7. Traductions personnalisées](/fr/settings/print/custom-translations)
{.links-list}

## 1.6 Flux de travail

- [1. Créer un Flux de travail](/fr/settings/workflows/workflow)
- [2. Actions de Flux de travail](/fr/settings/workflows/workflow-action-master)
- [3. Règle d'attribution](/fr/settings/workflows/assignment-rule)
{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.






