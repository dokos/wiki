---
title: Gestion de la qualité
description: 
published: true
date: 2021-07-05T13:40:07.297Z
tags: 
editor: markdown
dateCreated: 2021-06-29T09:43:29.629Z
---

# Gestion de la qualité

La gestion de la qualité est l'acte de superviser toutes les activités et tâches nécessaires pour maintenir un niveau d'excellence souhaité dans les tâches. Le module Qualité de Dokos vous aide à gérer vos objectifs qualité, processus, revues, non-conformités, actions et réunions. Il est conçu pour vous aider à gérer un **système de gestion de la qualité** complet. Voici un aperçu de ce qui est inclus dans le module qualité

---

Pour accéder au module de **Qualité**, allez sur :

> Accueil > **Qualité**

![module_qualité.png](/quality/module_qualité.png)

## 1. Objectifs et procédures

- [1. Objectif de qualité](/fr/quality/quality-goal)
- [2. Procédure de qualité](/fr/quality/quality-procedure)
{.links-list}

## 2. Commentaire

- [1. Retour de qualité](/fr/quality/quality-feedback)
- [2. Modèle de retour de qualité](/fr/quality/quality-feedback-template)
{.links-list}

## 3. Réunion

- [1. Réunion de qualité](/fr/quality/quality-metting)
{.links-list}

## 4. Revue et actions

- [1. Non-conformité](/fr/quality/non-conformance)
- [2. Revue de la qualité](/fr/quality/quality-review)
- [3. Action de qualité](/fr/quality/quality-action)
{.links-list}






