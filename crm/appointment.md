---
title: Rendez-vous
description: 
published: true
date: 2022-10-28T14:53:06.050Z
tags: 
editor: markdown
dateCreated: 2021-05-19T08:00:39.102Z
---

# Rendez-vous et mise en ligne
Un rendez-vous est une réunion préétablie entre un responsable et un employé de votre entreprise.

Le type de document de rendez-vous peut être utilisé pour planifier et gérer l'interaction avec un prospect ou une opportunité.

---

Pour accéder à la **liste des rendez-vous**, allez sur :

> Accueil > CRM > Pipeline des ventes > **Rendez-vous**

## 1. Prérequis avant utilisation

Avant de créer un Rendez-vous, il est conseillé de créer ces différents documents : 

- 1. **Paramètres de réservation de rendez-vous**
- 2. **Liste de vacances**
- 3. **Employé**
- 4. **Prospect**
- 5. **E-mail**

## 2. Comment créer un rendez-vous ?

### 2.1 Créeation rapide

1. Allez dans la liste des Rendez-vous, cliquez sur **:heavy_plus_sign: Ajouter Rendez-vous**.
2. Définir l'heure du rendez-vous dans le champ **Heure programmée**
3. Indiquez le **statut**.
4. Saisir le **nom** de la personne pour le rendez-vous.
5. **Email** du contact.

### 2.2 Création complète

1. Allez dans la liste des Rendez-vous, cliquez sur **:heavy_plus_sign: Ajouter Rendez-vous**.
2. Faire **modifier en pleine page**
3. Remplir les différents champs pour complèter la **fiche Rendez-vous**
- Heure programmée
- Statut du Rendez-vous
- Nom, Numéro de télphone, ID Skype, Email
- Détails sur le rendez-vous et sur la personne

## 3. Caractéristiques

### 3.1 Documents liés

- **Rendez-vous avec**
- **Tiers**
- **Événement du calendrier**

## 4. Création de rendez-vous via le site Web

Vos clients/prospects peuvent créer un rendez-vous à l'aide de la page Web yoursitename.com/book_appointment.

Ils doivent d'abord fixer une date, une heure. Formulaire de rendez-vous

Ensuite, ajoutez plus de détails

Il fera correspondre l'e-mail du client avec les prospects dans le système et s'il en trouve un, il est lié au document. Si aucune piste n'est trouvée, le rendez-vous est marqué comme "Non vérifié" et un e-mail est envoyé au client pour confirmer son e-mail

### 4.1. Caractéristiques 

- **Affectation automatique**
Les rendez-vous sont automatiquement attribués aux employés selon la Agentsliste dans Paramètres de prise de rendez -vous . L'agent ayant le moins d'affectations pour la journée du rendez-vous et qui est libre à l'heure prévue est affecté au rendez-vous.


- **E-mail de confirmation**
S'il n'y a pas de piste correspondante dans votre système, un e-mail sera envoyé à l'adresse e-mail du rendez-vous pour confirmer si l'adresse e-mail est valide. Lors de la confirmation, un nouveau prospect sera également créé dans le système avec le rendez-vous.


