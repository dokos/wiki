---
title: Groupe de clients
description: 
published: true
date: 2022-10-28T14:54:15.026Z
tags: 
editor: markdown
dateCreated: 2021-05-28T12:02:16.756Z
---

# Groupe de client

Le groupe de clients est une agrégation de clients similaires d'une certaine manière.

Les groupes de clients vous permettent d'organiser vos clients. En règle générale, les clients sont regroupés par segment de marché en fonction du domaine dans lequel une entreprise opère. Les groupes de clients sont créés de manière hiérarchique dans DOKOS. Vous pouvez créer un groupe de clients principal et y ajouter des sous-groupes de clients.

Vous pouvez définir un prix une liste qui sera automatiquement appliquée à tous les clients appartenant à ce groupe. Vous pouvez également obtenir une analyse des tendances pour chaque groupe. Les groupes de clients particuliers, commerciaux et gouvernementaux sont créés par défaut. Vous pouvez ajouter vos propres groupes de clients en fonction de vos besoins, tels que la vente au détail, la vente en gros, etc.

---

Pour accéder à la liste des groupes de clients, allez sur :

> Accueil > CRM > Paramètres > **Groupe de clients**

## 1. Comment créer un groupe de clients 

1. Cliquez sur un groupe de clients parent comme **Tous les groupes de clients**.
2. Cliquez sur **Ajouter un enfant**.
3. Saisissez **Nom du groupe de clients**.
4. Cochez **Noeud de groupe** si vous souhaitez ajouter des sous-groupes de clients sous celui-ci.
5. Cliquez sur **Créer nouveau**.

> **Astuce** : Si vous pensez que tout cela représente trop d'efforts, vous pouvez le laisser dans **Groupe de clients par défaut**. Mais tous ces efforts porteront leurs fruits lorsque vous commencerez à recevoir des rapports. Un exemple de rapport type est donné ci-dessous:
{.is-warning}

## 2. Caractéristiques 

### 2.1 Attribuer une limite de crédit, une liste de prix par défaut et un modèle de conditions de paiement par défaut 

Vous pouvez attribuer la limite de crédit, la liste de prix et les conditions de paiement et elles seront automatiquement appliquées lorsqu'un client appartenant au groupe de clients est sélectionné dans des transactions de vente telles que Commande client et Facture client.

### 2.2 Compte à recevoir par défaut 
Il n'est pas nécessaire de créer un grand livre comptable distinct pour chaque client dans DOKOS. Lisez le compte recevable commun pour plus de détails.

Si vous avez besoin d'un compte client distinct pour un client, vous pouvez l'ajouter dans la section «Compte client par défaut».
