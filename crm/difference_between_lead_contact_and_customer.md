---
title: Différence entre prospect, contact et client
description: 
published: true
date: 2021-05-19T08:10:35.591Z
tags: 
editor: markdown
dateCreated: 2021-05-19T08:10:35.591Z
---

# Différence entre prospect, contact et client

Il est fréquent dans l'utilsation d'un ERP de faire un amalgame entre un prospect, un contact et voire un client.

Dans cette section, nous allons expliquer la distinction entre ces trois types de document.

---

## 1. Caractéristiques

- **Un prospect** : Un prospect désigne, de façon concise, un client potentiel pour une entreprise. Le terme définit de façon plus large un individu pour lequel des opérations commerciales et/ou de communication ont été mises en place afin de le séduire et de le compter parmi les nouveaux clients.

- **Un client** : Acheteur effectif de biens ou de services proposés par l'entreprise. Le client peut être une personne physique ou une personne morale. C'est une personne qui a déjà fait une ou des affaires avec l'entreprise.

- **Contact** : C'est une personne qui appartient au Client. Ceci est applicable dans le scénario BtoB (Business to Business), où vous pouvez contacter plusieurs personnes appartenant au même client.
	**Par exemple** : Vous pouvez avoir une entreprise A qui est votre cliente, et cette entreprise à plusieurs contacts avec vous (Chef de l'entreprise, le commercial, le comptable etc.)

Un **prospect** peut être converti en **client** en sélectionnant **Client** dans la liste déroulante Créer . Une fois le client créé, le prospect devient **converti** et toute autre opportunité provenant de la même source peut être créée pour ce client.


