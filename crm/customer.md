---
title: Client
description: 
published: true
date: 2022-10-28T14:51:41.437Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:55:26.815Z
---

# Client

Un client est une personne physique ou morale acheteur de vos produits ou services.

## 1. Information sur les clients

> CRM > Pipeline de vente > **Client**

Un client est une donnée de base, ne nécessitant aucun pré-requis. Cependant il est possible de créer un client à partir d'un prospect existant dans _Dokos_.

### 1.1 Paramétrages par défaut

Afin de faciliter la création de vos clients, il est possible de définir certaines informations par défaut dans les **paramètres de vente**.

1. **Système de nommage**: Chaque client possède un identifiant unique, qui est le nom du document dans lequel sont renseignés les informations du client. Cet identifiant du client peut être basé sur le nom complet du client ou sur un numéro de série.  
Si vous choisissez de le baser sur le nom complet du client, en cas d'homonymes, _Dokos_ rajoutera automatiquement un "-1" à la fin du nom pour les différencier.  
Si vous choisissez de le baser sur une série, vous pouvez définir le préfixe de cette série dans le type de document **Nom de série**. 

2. **Groupe de client**: Définissez le groupe de client par défaut. Il s'agit généralement du groupe de client le plus souvent sélectionné.

3. **Région par défaut**: Définissez la région qui sera sélectionnée par défaut dans le client.


### 1.2 Fonctionnalités

Les informations renseignées dans la fiche client servent ensuite de valeurs par défaut dans les transactions.
Par exemple, si vous mettez la langue d'impression "fr" dans la fiche du client, les documents générés pour ce client (devis, factures, ...) seront imprimés par défaut en français. Vous pouvez toujours modifier cette valeur dans le document correspondant au moment de sa création.


## 2. Comment accéder à la liste des Clients ?

Pour accéder à la liste des fournisseurs, accédez à:

> Accueil > Vente > Client


### 2.1 Comment ajouter un client?

- Aller sur la listes des clients
- Cliquer sur en haut à droite sur le bouton '+ Ajouter Client'
> Une fenêtre modale (ou Pop-up) s’affiche 
{.is-info}

### 2.2 Création de la fiche Client - *Ajout rapide*
- Saisir le **Nom complet**
- Indiquer le **Type**:Choix entre SOCIÉTÉ ou INDIVIDUEL
- Selectionner la **Groupe de clients**:Il est défini par défaut la groupe de client le plus souvent sélectionné
- Selectionner la **Région**: Lieu domiciliation du Client (France, Italie, Allemagne...)
- Optionnel: Vous pouvez  aussi remplisr les **Détails du contact principal**
- Optionnel: Vous pouvez  aussi remplisr les **Détails de l'adresse principale**
- Cliquez sur **Enregistrer**

### 2.3 Création de la fiche Client - *Complet*
- Cliquez sur "**Modifier en pleine page**"

Une fois arrivée sur la page "**Nouveau.elle. Customer**", vous pouvez créer complètement votre fiche Client.

#### 2.3.1 Détails de la fiche Nouveau·elle Customer

- **Nom du fournisseur** : Nom de la personne morale ou personne physique
- **Type**: Intiquer c'est une société ou indivituel
- **Numéro de compte permanent (PAN)**: Le numéro de compte permanent (Permanent Account Number – PAN) est [traduction] « un code alphanumérique à 10 caractères, délivré sous la forme d'une carte plastifiée par le ministère du Revenu (Income Tax Department) » de l'Inde
- **Catégorie de taxation à la source**: La retenue à la source est un dispositif fiscal permettant à un État de prélever l'impôt sur des sommes versées à un non-résidant fiscal du pays.Selectionner Catégorie de taxation à la source Si elle est déja défini ici. (Possibilité de  '+ créer Nouveau·elle Catégorie de taxation à la source')
- **Compte bancaire par défaut de la société**: Si un Compte bancaire est défini ici, il sera automatiquement sélectionné pour les futures transactions de vente.((Possibilité de  '+ créer Nouveau·elle Liste de prix')
- **Créé à partir du prospect**: Selectionner les prospects Si elle est déja défini ici. (Possibilité de  '+ créer Nouveau·elle prospect')
- **Responsable du compte**: Selectionner le Responsable du compte Si elle est déja défini ici. (Possibilité de  '+ créer Nouveau·elle Utilisateur')
- **Groupe de clients**: Sélectionnez selon la liste Groupe de fournisseurs. Exemples : Commercial, Gouvernement...
- **Région**: Lieu domiciliation du Client (France, Italie, Allemagne...)
- **Numéro de TVA**
- **Catégorie de taxe**: Selectionner la Catégorie de taxe Si elle est déja défini ici. (Possibilité de  '+ créer Nouveau·elle Catégorie de taxe')
- Option **Permettre la création de factures sans bon de commande**
- Option **Permettre la création de factures sans bon de livraison**
- Option **Desactivé**
- Option **Est un client interne**: Si 'oui', il faut indiquer sa Représente la société

#### 2.3.2 Devise et liste de prix
- **Devise de facturation**: La devise de votre client peut être différente de la devise de votre entreprise. Si vous choisissez EUR pour un client, la devise sera remplie en EUR et le taux de change sera indiqué pour les transactions d'achat futures.
- **Liste de prix par défaut**: si un modèle de la liste de prix est défini ici, il sera automatiquement sélectionné pour les futures transactions de vente.(Possibilité de  '+ créer Nouveau·elle Liste de prix')

#### 2.3.3 Adresse principale et coordonnées du contact
- **Contact principal du client**:Sélectionnez, pour rendre le client recherchable avec ces champs(Possibilité de  '+ créer Nouveau·elle Contact')
- **Adresse principale du client**: Selectionner l'adresse Si l'adresse est déja défini ici. (Possibilité de  '+ créer Nouveau·elle Adresse')

#### 2.3.4 Comptabilité
Renseigner si le compte débiteur n'est pas standard
- **société**:
- **compte**:

#### 2.3.5 Limite de crédit et conditions de paiement
- **Modèle de termes de paiement par défaut**: si un modèle de conditions de paiement est défini ici, il sera automatiquement sélectionné pour les futures transactions de vente.(Posibilité de '+ créer Nouveau·elle Modèle de termes de paiement')
- **Limite de crédit**: Indiquer la société et le montant Limite de crédit

#### 2.3.6 Informations additionnelles

Vous pouvez ajouter des informations facultatives sur la fiche du fournisseur :

- **Détails du client**
- **Part de Marché** de client
- **Industrie** de client
- **La langue d’impression** des documents. Par défaut sur la langue paramétrée dans Paramètre 
- Option **Est gelé**, si vous cochez la case Est gelé, les écritures comptables pour le client seront gelées. Dans ce cas, le seul utilisateur dont les entrées dépasseront le "gel" est le rôle attribué dans Rôle autorisé à définir des comptes gelés et à modifier les entrées gelées dans Comptabilité> Paramètres> Paramètres des comptes. Ceci est utile lorsque le nom du client ou les coordonnées bancaires sont modifiés.

#### 2.3.7 Points de fidélité
- **Programme de fidélité**: Selectionner si la Programme de fidélité est déja défini.(Possibilité de  '+ créer Nouveau·elle Programme de fidélité')

#### 2.3.8 Partenaire Commercial et Commission
- **Partenaire commercial**: Selectionner si la Partenaire Commercial est déja défini.(Possibilité de  '+ créer Nouveau·elle Partenaire Commercial')
- **Taux de Commission**: Indiquer la taux de commission entre la partenaire et vous

#### 2.3.9 Equipe de vente
- **Détails de l'Équipe des Ventes**: Indiquer le **vendeur** et son montant de **Contribution Totale Net**

## 3. Après l’enregistrement du Client

Une fois que tous les détails nécessaires sont remplis, enregistrez le document. Lors de l'enregistrement, les options permettant de créer les éléments suivants seront visibles dans le tableau de bord. Dnas le champs de Documents liés, Vous pouvez voir les listes de documents et créer les nouveaux documents aupres de ce client. Les Documents liés y compris:

Prévente:
- **Opportunité**
- **Devis**
- **Contrat**

Commandes:
- **Commande client**
- **Bon de livraison**
- **Facture de vente**

Paiements：
- **Écriture de paiement**
- **Compte bancaire**

Support：
- **Ticket**
- **Visite d'Entretien**
- **Note d'Installation**
- **Réclamation de Garantie**

Projets：
- **Projet**

Tarification：
- **Règle de prix**

Abonnements：
- **Abonnement**