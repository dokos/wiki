---
title: Publication sur les réseaux sociaux
description: 
published: true
date: 2022-10-28T14:56:19.055Z
tags: 
editor: markdown
dateCreated: 2021-06-22T12:23:59.599Z
---

# Publication sur les réseaux sociaux

La publication sur les réseaux sociaux est utilisée pour créer et planifier la publication pour LinkedIn et Twitter, elle peut également être liée à une campagne.

---

Pour accéder à **la liste publication sur les réseaux sociaux**, allez sur :

> Accueil > CRM > Campagne > **Publication sur les réseaux sociaux**

## 1. Prérequis avant utilisation

- Paramètres LinkedIn
- Paramètres Twitter

## 2. Comment créer une publication sur les réseaux sociaux

1. Accédez à la liste des publications sur les réseaux sociaux, cliquez sur **:heavy_plus_sign: Ajouter Publication sur les réseaux sociaux** .
2. Sélectionnez **Campagne**, le cas échéant, laissez-le vide.
3. Choisissez l'**heure prévue** de la publication à publier.
4. Sélectionnez les **plateformes de médias sociaux** sur lesquelles la publication doit être publiée.
5. Saisissez du **contenu** pour Twitter et LinkedIn.
6. Joindre une image (facultatif).
7. **Enregistrer** et envoyer.

## 3. Caractéristiques

### 3.1 Publier maintenant

Les utilisateurs peuvent également publier immédiatement juste après avoir soumis une publication sur les réseaux sociaux en cliquant sur le bouton **Publier maintenant**.

### 3.2 Reprogrammer

Les utilisateurs peuvent modifier l'heure prévue de la publication. En cas d'erreur, les utilisateurs peuvent reprogrammer la publication en modifiant l'Heure programmée du document de publication sur les réseaux sociaux.