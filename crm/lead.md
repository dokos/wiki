---
title: Piste
description: 
published: true
date: 2022-10-14T12:30:01.575Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:55:33.011Z
---

# Piste

> Depuis la version 3.0 de Dokos, l'ancien type de document **Prospect** a été renommé en **Piste**. 
{.is-warning}


Une piste est un client potentiel qui pourrait être intéressé par vos produits ou services.

Pour amener le client à franchir la porte, vous pouvez effectuer tout ou partie des actions suivantes :

- **Liste de votre produit sur des répertoires.**
- **Maintenir un site Web mis à jour et consultable.**
- **Rencontrer des gens lors d'événements commerciaux.**
- **Publicité de vos produits ou services.**

Lorsque vous faites savoir que vous êtes dans les parages et que vous avez quelque chose de précieux à offrir, les gens viennent vérifier vos produits. Ce sont vos leads.

Ils sont appelés **Piste** dans Dokos car ils peuvent vous conduire à une vente. Les directeurs des ventes travaillent généralement sur les pistes en les appelant, en établissant une relation et en envoyant des informations sur les produits ou services. Il est important de suivre toute cette conversation pour permettre à une autre personne qui pourrait avoir à faire le suivi de ce contact. La nouvelle personne est alors en mesure de connaître l'historique de cette piste particulière.

---

Pour accéder à la liste des Pistes, allez sur :
> CRM > **Piste**

# 1. Créer une Piste

1. Accédez à la liste des Pistes et cliquez sur :heavy_plus_sign: Ajouter Piste
	- **Le Piste est une organisation** : Cochez la case si vos Piste est une organisation. Notez que si la case est cochée, le champ **Nom de l'entreprise est obligatoire**.

2. **Remplir les différents champs pour complèter la fiche** :
	- Nom de la personne
	- Nom de l'organisation
	- Adresse email
	- Responsable de la Piste
	- Statut de la Piste : Les statuts suivants sont automatiquements mis à jours. Vous pouvez également ajoutez/supprimez des statuts en personnalisant le document.
		- **Ouvert** : Lors de la création de la Piste
		- **Opportunité** : Dès que vous aurez créé une opportunité
		- **Devis** : Dès que le devis est validé
		- **Converti** : Dès que le client accepte le devis et que vous avez créé une commande
		- **Devis perdu** : Si vous indiquez que le devis est perdu.
	- Salutation : Mme, M., etc.
	- Désignation
	- Genre
	- Source
	- Nom de la campagne
3. **Enregistrer**

> **Statut** : Si vous supprimez un statut qui est lié à une action cela pourrait créer une erreur. Notamment dans les prochaines mises à jours de la plateforme où certains statuts pourraient être lié à de nouvelles actions.  
{.is-warning}

# 2. Caractéristiques

## 2.1 Suivre

En renseignant les informations relatives au prochain contact avec la Piste, un événement est automatiquement ajouté à votre calendrier. Celui-ci vous enverra un email de rappel le matin du rendez-vous.
Vous pouvez également configurer des notifications en fonction de vos besoins spécifiques.

## 2.2 Notes

Dans cette section, vous avez la possibilité d'ajouter toutes les informations nécessaires sur la Piste, des remarques, des suggestions afin de faciliter la gestion de la Piste.


## 2.3 Adresse et contact

### 2.3.1 Adresse de la Piste

Vous pouvez indiquer l'**adresse** de votre Piste :

- Type d'adresse : Facturation, livraison, Magasin, Bureau, Entrepôt, etc.
- Titre de l'adresse : Domicile, Travail, etc.
- Adresse, Ville, État, Pays, Code postal

### 2.3.2 Contact de la Piste

Indiquez les **coordonnées** de la Piste : 

- Téléphone
- N°Mobile
- Fax
- Site web

## 2.3 Informations additionnelles

Vous pouvez ajouter des informations complémentaires sur votre Piste dans les notes, mais également dans des champs d'information structurée qui seront facilement requêtables pour analyser vos Pistes: part de marché, site web, industrie, région.
Ces informations seront reprises automatiquement à la création d'une fiche client à partir de cet Piste.

Le champ "Désabonné" est mis à jour automatiquement lorsque la Piste se désabonne des emails que vous lui envoyez. Il sert également à filtrer les Pistes que vous ajoutez à un nouveau groupe email.
