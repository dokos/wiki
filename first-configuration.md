---
title: Première configuration dans Dokos
description: 
published: true
date: 2022-02-08T17:24:12.676Z
tags: 
editor: markdown
dateCreated: 2022-02-08T17:00:39.083Z
---

# Première configuration dans Dokos

## Utilisateurs

### Les modules à activer

|   Modules   | Utilité |   Modules |   Utilité |
|:-   |:-  |:-    |:-
|  **Achat**  |  Configurez vos achats (appel d'offre, devis fournisseur, facture d'achat...)  |  **Actifs** |  Gestion des actifs
|  **Automatisation**  |  Les outils (ToDo, fichiers, Note..) |  **CRM** |  Votre relation client (Emailing, Clients...)
|  **Comptes**  |  Gestion de la comptabilité |  **Configuration** |  Affichage de l'Accueil
|  **Gestion de la quallité**  |  Gérez la qualité de vos produits avec des process |  **Gestion des prêts** |  Configurez vos prêts
|  **Intégrations**  |  Gérez les différents intégrations (Paiement, Google, Webhook...)  |  **Lieu** |  Gestion des actifs
|  **Module central**  |  Gestion des utilisateurs, des paramètres et du développement  |  **Paie** | Gestion des paies de vos employés
|  **Personnalisé**  |  Si vous avez créé des modules personnalisés |  **Production** | Gérez la production (plan de production, ordre de travail...)
|  **Projets**  |  Gérez vos projets, vos tâches  |  **RH** |  Gérez les ressources humaines (employés, contrat etc..)
|  **Site web**  |  Module du portail utilisateur  |  **Stock** |  Gestion des stocks
|  **Support**  |  Gestion des tickets, des garanties etc...  |  **Vente** |  Gestion des ventes (Devis, Commandes, Factures...)

