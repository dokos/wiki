---
title: Personnalisations
description: 
published: true
date: 2021-11-24T12:47:07.477Z
tags: 
editor: markdown
dateCreated: 2021-01-14T14:14:07.371Z
---

# Personnalisation

## 1. Développements

### 1.1 Modules

- [Modules Def](/fr/use-cases)
- [Espace de travail](/fr/customizations/workspace)
- [Onboarding par module](/fr/use-cases)
{.links-list}

### 1.2 Modèles

- [Type de document](/fr/customizations/doctype)
- [Flux de travail](/fr/settings/workflow)
{.links-list}

### 1.3 Vues

- [Rapport](/fr/use-cases)
- [Format d'impression](/fr/settings/print/print-format)
- [Tableau de bord](/fr/tools/dashboard)
{.links-list}

### 1.3 Scripts

- [Scripts Python](/fr/customizations/server-scripts)
- [Scripts client](/fr/use-cases)
- [Type de tâche de fond programmée](/fr/use-cases)
{.links-list}

### Autres personnalisations

- [Vue du calendrier](/fr/customizations/calendar-view)
- [Modèles Jinja](/fr/customizations/jinja-templates)
- [Cas d'usage](/fr/use-cases)
{.links-list}

