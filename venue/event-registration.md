---
title: Inscription à événement
description: 
published: true
date: 2022-10-28T15:00:21.209Z
tags: 
editor: markdown
dateCreated: 2022-02-04T13:40:18.769Z
---

# Inscription à un évenement

A la suite de la création de votre événement, vous souhaitez retrouver les inscriptions à l'événement.
Ce document va vous permettre de retrouver tous les participants à chaqu'un de vos évenement.
Ces inscriptions pourront être faites manuellement ou automatiquement si le participant s'inscrit en ligne.

---

Pour accéder à la liste Inscription à un événement, allez sur :
> Accueil > Lieu > Réservations et événements > **Inscription à événement**

## 1. Comment créer une inscription à un événement ?

### 1. Ajoutez un participant manuellement

1. Allez dans la liste **Inscription à un événement**, cliquez sur **Ajouter Inscription à événement**
2. Remplir le formulaire du document :
	- **Prénom** et **Nom**
 	 - **Adresse Email**
   - **Événement** : Sélectionnez l'évenement auquel le participant sera inscrit.
   - **Utilisateur** : Si l'utilisateur est répertorié dans votre système, alors vous pouvez le sélectionner ici.
   - **Contact** : Si le contact est répertorié dans votre système, alors vous pouvez le sélectionner ici.
   
3. Faites **Enregistrer**

### 2. Ajoutez un participant automatiquement

L'ajout automatique consiste à ce que vos participants s'inscrivent eux-même à votre événement depuis le portail client.

1. Allez sur l'événement qui sera disponible sur votre portail client
2. Modifiez votre événement et allez dans la **section Site Web**
3. Cochez les cases **Publié** et **Autorisez les inscriptions**
4. **Autorisez les annulations**: Vos participants pourront annuler leur inscription à l'événement via un Bouton.
5. **Visible pour** : Vous pouvez choisir pour qui l'événement sera visible sur votre portail client. 
5. **Route** : Indiquez l'URL de votre événement / Si vous n'indiquez rien, alors lors de l'enregistrement le système créera automatiquement une route sur la base suivant `/events/{nomdudocument-sujet}`
6. **Format d'impression** : Choisir un modèle qui affichera le format d'impression sur le portail
7. **Message d'inscription réussie** : Peronnalisez le message qui sera affiché dès que le participant aura validé son inscription.
8. Faites **Enregistrer**