---
title: Utilisation de crédit de réservation
description: 
published: true
date: 2022-01-31T15:20:26.523Z
tags: 
editor: markdown
dateCreated: 2022-01-31T15:13:10.099Z
---

# Utilisation de crédit de réservation

Les crédits consommés permettent la déduction d'une quantité d'une unité de mesure associée à un article.

---

Pour accèder à la **liste utilisation de crédit de réservation**, allez sur :
> Accueil > Lieu > Crédits de réservation > Utilisation de crédit de réservation

![utilisation_crédit_de_réservation.png](/venue/booking-credit/utilisation_crédit_de_réservation.png)

## 1. Ajouter des utilisation de crédit

1. Allez sur la **liste utilisation de crédit de réservation**, cliquez sur Ajouter **Nouveau**.
2. Renseignez les différents champs pour complèter l'utilisation de crédit.
- **Utilisateur** : Sélectionnez l'utilisateur concerné (facultatif)
- **Client** : Sélectionnez le client concerné (obligatoire)
- **Date / Heure** : Indiquez une date
- **Article** : Choisissez l'article
- **Quantité** : Indiquez une quantité
- **Unité de mesure** : Sélectionnez l'unité de mesure correspondante

3. Faites **Enregistrer** puis **Confirmer**.

![détails_configuration_crédit.png](/venue/booking-credit/détails_configuration_crédit.png)