---
title: Événement
description: 
published: true
date: 2022-10-28T14:58:20.880Z
tags: 
editor: markdown
dateCreated: 2021-09-30T13:11:25.018Z
---

# Événement

Dans Dokos, vous pouvez créer des événements pour vos clients, adhérents, employés. 

Ces événements pourront être également choisi par vos adhérents sur le portail client. 

---

Pour accéder à la liste des événements, allez sur :

> Accueil > Lieu > Réservations et événements > **Événement**

## 1. Comment créer un événement ?

1. Allez dans la **liste des événements**, cliquez sur **Ajouter Événement**.
2. Renseignez la première section en remplissant les champs suivants :
 	- Ajoutez le **Sujet**
	- Définissez une **Catégorie d'événement** : L'évenement peut être une réunion, un événement, un appel, l'envoi d'un email, autre.
	- Sélectionnez un **Type d'événement** : Il peut être public ou privé
  	- Indiquez **Commence le** : Indiquez la date de début
  	- Indiquez **Termine le** : Indiquez la date de fin
  	- Pour voir le **Statut** : Visualiser le statut de votre événement
  	- **Toute la journée** : Cochez la case si votre événement dure toute la journée.
  	- **Répéter cet événement** : Cochez la case pour répéter cet événement. Une fenêtre s'ouvrira et vous allez pouvoir indiquer la fréquence, répété jusqu'au, intervale de fréquence.
  	- **Synchroniser avec Google Agenda** : Cochez la case pour synchroniser l'événement sur Google Agenda.

## 2. Détails de l'événement
  
### 2.1 Participants

Dans cette section, vous avez la possibilité d'indiquer les particpants qui seront présents pour l'évenement. Il ne s'agit pas des participants inscrits comme vos adhérents qui pourront s'inscrire depuis le portail client.

Les participants peuvent être des personnes qui vont intervenir durant l'événement.

### 2.2 Description

Ajoutez ici une description pour expliquer en détail les informations à propos de votre événement. Vous pouvez ajouter les renseignements d'accès, de capacité, s'il y a des conditions pour participer à l'événement etc.  