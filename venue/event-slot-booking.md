---
title: Inscription à un poste d'événement
description: 
published: true
date: 2022-10-28T14:59:51.469Z
tags: 
editor: markdown
dateCreated: 2022-02-04T14:47:48.822Z
---

# Inscription à un poste d'évenement

A la suite de la création de votre poste d'événement, vous souhaitez retrouver les inscriptions à ce poste d'événement.
Ce document va vous permettre de retrouver tous les participants à chaqu'un de vos postes.

---

Pour accéder à la liste Inscription à un événement, allez sur :
> Accueil > Lieu > Réservations et événements > **Inscription à un poste d'événement**

## 1. Comment créer une inscription à un poste d'événement ?

### 1. Ajoutez un participant

1. Allez dans la liste **Inscription à un poste d'événement**, cliquez sur **Ajouter Inscription à un poste d'événement**
2. Remplir le formulaire du document :
	- **Prénom** et **Nom**
 	 - **Adresse Email**
   - **Événement** : Sélectionnez l'évenement auquel le participant sera inscrit.
   - **Utilisateur** : Si l'utilisateur est répertorié dans votre système, alors vous pouvez le sélectionner ici.
   - **Contact** : Si le contact est répertorié dans votre système, alors vous pouvez le sélectionner ici.
   
3. Faites **Enregistrer**