---
title: Règle de crédit de réservation
description: 
published: true
date: 2022-01-31T15:49:28.458Z
tags: 
editor: markdown
dateCreated: 2022-01-31T15:21:36.036Z
---

# Règle de crédit de réservation

Bien qu'il soit possible d'ajouter et de déduire des crédits manuellement, ces opérations sont fatidieuses et peuvent prendre beaucoup de temps.  
Il est donc possible de créer des règles qui ajouteront ou déduiront automatiquement ces crédits en fonction des actions des utilisateurs.  

---

Pour accéder à la liste **Règle de crédit de réservation**, allez sur :
> Accueil > Lieu > Crédits de réservation > **Règle de crédit de réservation**

### 1. Règles d'addition

Une règle d'addition va lire un document généré par l'action d'un utilisateur et ajouter les crédits correspondant si celui-ci répond à certains critères.  


> Nous souhaitons que des crédits soient automatiquement attribués à un client à chaque fois qu'il achète l'article "Coworking Ponctuel".
> 
> Nous allons donc créer une règle d'addition de crédits de réservation avec les critères suivants:
> 
> - Cette règle sera uniquement applicable pour un article, en l'occurence l'article Coworking Ponctuel.
> - L'élément déclencheur sera la validation d'une facture de vente.
> - Nous définirons également que nous souhaitons lire les informations concernant la quantité, l'unité de mesure et l'article dans la table enfant "Articles".
> - Nous pourrions également ajouter une condition, comme par exemple que cette règle ne concerne que les client appartenant au groupe de clients "Coworkers".
> - Nous définirons que ces crédits expirent au bout de 6 mois.
> - Puis nous établirons une correpondance entre certaines informations nécessaire pour le fonctionnement de la règle et les champs correspondants dans la facture de vente (et le tableau d'articles).
> 

Cette règle donnera le document suivant:

- **Type de règle**: Addition de crédits de réservation
- **Applicable uniquement pour**: Article
- **Document**: Coworking ponctuel

- **Document déclencheur**: Facture de vente
- **Action déclencheuse**: A la validation
- **Utiliser une table enfant pour l'article, l'unité de mesure et la quantité**: Oui
- **Table enfant**: Articles
- **Conditions**: doc.customer_group == "Coworkers"

- **Règle d'expiration**: Ajouter X Mois
- **Délai d'expiration**: 6

- **Champ Client**: Client
- **Champ contenant l'utilisateur**: _Vide_ (Les crédits seront attribués au client et non pas spécifiquement à un utilisateur lié à ce client)
- **Champ Unité de mesure**: Unité de mesure (Champ de la ligne d'articles)
- **Champ Article**: Article (Champ de la ligne d'articles)
- **Champ Date**: Date
- **Champ Quantité**: Quantité (Champ de la ligne d'articles)


> Un client achète 10 Heures de l'article Coworking ponctuel. La règle ci-dessus lui ajoutera automatiquement 10 Heures associées à l'article coworking ponctuel à la validation de la facture correspondante.
{.is-info}


#### 1.1 Règles personnalisées

Dans certains cas, les unités de mesure et l'aticle à ajouter au crédit du client ne correspondent pas à ceux du document de référence. On peut alors créer des correspondances grâce aux règles personnalisées.

Ces règles permettent de définir l'unité de mesure source - celle du document de référence -, la quantité cible pour une unité de quantité source et son unité de mesure cible - celle du crédit de réservation -.


> On souhaite facture un abonnement mensuel correspondant à 10 journées de coworking.
> Dans ce cas on créera une règle du type:
> |Unité de mesure source|Quantité cible|Unité de mesure cible|
> |----------------------|--------------|---------------------|
> |Mois|10|Jour|



### 2. Règles de déduction

Une règle de déduction va lire un document généré par l'action d'un utilisateur et déduire les crédits correspondant si celui-ci répond à certains critères.
Si les principes sont similaires aux règles d'addition de crédits, les règles de déduction ont quelques différences:

1. Elles ont une date de validité, qui peut servir notamment à déduire des crédits associés à des réservations faites dans le passé.
2. Il est possible d'arrondir l'usage à l'unité supérieure ou inférieure (Si l'utilisateur a consommé 2.5 heures, on peut déduire 2 heures ou 3 heures)
3. Les crédits peuvent être déduits:
	- Selon l'unité de mesure la plus proche de la consommation réelle
	Ex. Si l'utilisateur a acheté 5 heures, puis 2 jours de coworking et viens passer une journée dans le lieu, on lui déduira une journée

	- Selon la règle du premier entré/premier sorti
	Ex. Si l'utilisateur a acheté 10 journées, puis 10 heures de coworking et passe 2 heures dans le lieu, on lui déduira une journée

	- Selon des règles personnalisées

#### 2.1 Les règles personnalisées

Les règles de déduction personnalisées permettent de s'affranchir des règles de déduction par défaut, basées sur la consommation exacte de minutes de réservations.  

Il est ainsi possible de mettre en place des scénarios comme par exemple:

- Si l'utilisateur reste moins de 4 heures, on lui déduit le nombre d'heures consommées
- Si l'utilisateur reste entre 4 et 5 heures, on lui déduit une demi-journée
- Si l'utilisateur reste entre 6 et 7 heures, on lui déduit une demi-journée + les heures au delà de 5 heures
- Si l'utilisateur reste plus de 7 heures, on lui déduit 1 journée

Vous pouvez ainsi définir deux composantes:
1. Une durée (mins)
2. Un intervalle de temps (mins)

Le temps consommé est calculé en minutes.
Si cette durée est contenue dans un des intervalles de temps, le système déduira l'unité de mesure correspondant à cet intervalle, puis cherchera un autre intervalle ou une durée correspond au temps restant.

Pour configurer le cas de figure ci-dessus, il faut donc entrer les informations suivantes dans le tableau:

|Durée|Intervalle minimum|Intervalle maximum|Unité de mesure|Quantité de crédit|
|-----|------------------|------------------|---------------|------------------|
|60|||Heure|1|
||240|300|Demi-journée|1|
||480|960|Jour|1|


Afin de déterminer quel article doit être déduit, Dokos permet de configurer des conversion de réservation de crédits.

### 2.2 Conversion de réservation de crédits

Les conversion de réservation de crédits permettent d'indiquer à Dokos quels articles peuvent être convertis lors des déductions de crédit.  

Prenons par exemple un lieu qui loue 4 salles: 2 x 30m2 et 2 x 50m2.
Ce tiers lieu vendra 2 types de tickets: "Ticket petite salle" et "Ticket grande salle".

Dans ce cas les articles correspondant aux deux salles de 30m2 pourront être convertis en "Ticket petite salle" et les articles correspondant aux deux salles de 60m2 pourront être convertis en "Ticket grande salle".

Ces documents permettront également au système d'effectuer une conversion automatique lors des réservations sur le site web.