---
title: Poste d'événement
description: 
published: true
date: 2022-10-28T14:59:34.861Z
tags: 
editor: markdown
dateCreated: 2021-10-05T13:29:41.902Z
---

# Poste d'événement

Un poste d'événement est relié à un événement. Il peut être considéré comme un sous-événement. Par exemple, lors d'un événement d'une journée vous pouvez avoir plusieurs activités qui auront lieu durant cette journée. Ce sont donc des postes d'événements.

---

Pour accéder à la liste des Postes d'événement, allez sur :

> Accueil > Lieu > Réservation et événement > **Poste d'événement**

## 1. Comment créer un poste d'événement ?

1. Allez dans la liste des postes d'événements, cliquez sur **Ajouter un poste d'événement**
2. Remplissez les différents champs suivants :
	- Sélectionnez un **événement**
	- Indiquez le **titre du poste**
	- Renseignez l'**heure de début du créneau**
	- Renseignez l'**heure de fin du créneau**
3. Indiquez les **réservations disponibles** pour ce poste
4. Remplissez la **description du poste**
5. **Enregistrez**

## 2. Détails du poste d'événement

### 2.1 Réservation

Depuis cette section, gérez vos **paramètres de réservation** pour ce poste.

- Indiquez le **nombre de poste disponible**

### 2.2 Description

Depuis cette section, gérez la **description** pour ce poste.

- Renseignez une description pour détailler les informations à propos de ce poste d'événement.
