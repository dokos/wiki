---
title: Foire aux questions - FAQ
description: 
published: false
date: 2022-04-07T16:24:35.326Z
tags: 
editor: markdown
dateCreated: 2022-03-07T13:29:06.071Z
---

# Foire aux questions - FAQ

Sur cette page vous retrouverez les réponses aux questions les plus courantes.

## Articles

### Prix 

**Peut-on laisser aux clients / utilisateurs de choisir le montant de l'article depuis le portail client ?**

- Il n’y a malheureusement pas de solution “toute prête” dans Dokos pour permettre aux acheteurs de modifier/fixer les prix sur la boutique en ligne.

## Ventes

### Factures

**Comment personnaliser les numéros de factures ? Comment reprendre une série de numérotation pour les factures ?**

- Un cas d'usage a été écrit pour répondre à cette question.
<a href="https://doc.dokos.io/fr/customization/use-cases/invoice-number" target="_blank">**Changer la numérotation d'une facture**</a>

## Tableau de bord

### Personnalisation


