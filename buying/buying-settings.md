---
title: Paramètres d'Achat
description: 
published: true
date: 2022-10-27T15:52:33.334Z
tags: 
editor: markdown
dateCreated: 2021-05-05T10:25:13.092Z
---

# Paramètres d'Achat

Les **Paramètres d'achat** est l'endroit où vous pouvez définir les conditions qui seront appliquées dans les transactions du module d'achat. 

Pour accéder aux **paramètres d'achat**, allez sur:

> Accueil > Achats > Paramètres > **Paramètres d'achat**

## 1. Fournisseur

### 1.1 Dénomination des fournisseurs par défaut

Lorsqu'un fournisseur est enregistré, le système génère une identité ou un nom unique pour ce fournisseur qui peut être utilisé pour référer le fournisseur dans diverses transactions d'achat.

S'il n'est pas configuré autrement, DOKOS utilise le nom du fournisseur comme nom unique. Si vous souhaitez identifier les fournisseurs en utilisant des noms tels que SUPP-00001, SUPP-00002, ou toute autre série à motifs, sélectionnez la valeur de Nom du fournisseur par comme «Série de noms».

Vous pouvez définir ou sélectionner le modèle de série de noms dans: Paramètres > Données > Série de noms

Lisez Série de noms pour en savoir plus sur la définition d'une série de noms.

### 1.2 Groupe de fournisseurs par défaut 
Configurez la valeur par défaut du groupe de fournisseurs lors de la création d'un nouveau fournisseur. Par exemple, si la plupart de vos fournisseurs vous fournissent du matériel, vous pouvez définir la valeur par défaut sur «Matériel».

## 2. Achats 
### 2.1 Liste de prix d'achat par défaut 
Configurez ce qui devrait être la liste de prix par défaut lors de la création d'une nouvelle transaction d'achat, la valeur par défaut est définie sur **Achat standard**. Les prix des articles seront extraits de cette liste de prix. Vous pouvez modifier la **Liste de prix** en utilisant la flèche à l'extrémité droite du champ pour changer la devise et le pays.

#### 2.2 Commande fournisseur requis 
Si cette option est configurée sur **Oui**, DOKOS vous empêchera de créer une **facture d'achat** ou un **reçu d'achat** directement sans créer d'abord une **commande fournisseur**. Si des transactions au détail sont impliquées lorsque la commande se déroule hors ligne, les commandes fournisseurs peuvent être ignorés. Si vous acceptez des échantillons d'articles, vous pouvez créer directement un reçu d'achat pour recevoir les articles dans votre entrepôt.

Cette configuration peut être remplacée pour un fournisseur particulier en cochant la case "Autoriser la création de facture d'achat sans commande d'achat" dans la fiche fournisseur.

### 2.3 Reçu d'achat requis 
Si cette option est configurée sur **Oui**, DOKOS vous empêchera de créer une **facture d'achat** sans créer d'abord un **reçu d'achat**. Dans le cas où l'article en cours de transaction est un service, il ne nécessitera pas de reçu, vous pouvez directement créer une facture.

Cette configuration peut être remplacée pour un fournisseur particulier en cochant la case **Autoriser la création de facture d'achat sans reçu d'achat** dans la fiche fournisseur

### 2.4 Maintenir le même prix tout au long du cycle d'achat 
Si cela est activé, DOKOS validera si le prix d'un article change dans une facture d'achat ou un reçu d'achat créé à partir d'une commande fournisseur, c'est-à-dire qu'il vous aidera à maintenir le même prix tout au long du cycle d'achat.

Vous pouvez configurer l'action que le système doit entreprendre si le même prix n'est pas maintenu dans le champ **Action si le même prix n'est pas maintenu** :

- **Stop** : DOKOS vous empêchera de changer le prix en lançant une erreur de validation.
- **Avertir** : le système vous permettra de sauvegarder la transaction mais vous avertira par un message si le taux est modifié.

## 3. Autoriser les articles a être ajoutés plusieurs fois dans une transaction 
Lorsque cette case n'est pas cochée, un article ne peut pas être ajouté plusieurs fois dans la même commande fournisseur. 

Cependant, vous pouvez toujours modifier explicitement la quantité. Il s'agit d'une case à cocher de validation pour éviter tout achat accidentel du même article. Cela peut être vérifié pour des cas d'utilisation spécifiques où il existe plusieurs sources pour le même matériau, par exemple dans la fabrication.


