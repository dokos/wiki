---
title: Groupe de fournisseurs
description: 
published: true
date: 2022-10-27T15:51:14.088Z
tags: 
editor: markdown
dateCreated: 2021-05-05T10:31:18.298Z
---

# Groupe de fournisseurs

Le **groupe de fournisseurs** est une agrégation de fournisseurs qui sont similaires d'une certaine manière.

Un fournisseur peut être distingué d'un entrepreneur ou d'un sous-traitant, qui ajoute généralement des intrants spécialisés aux produits livrables. Un fournisseur est également appelé vendeur. Il existe différents types de fournisseurs en fonction des biens et des produits qu'ils fournissent.

DOKOS vous permet de **créer vos propres catégories de fournisseurs**. Ces catégories sont appelées groupes de fournisseurs. Par exemple, si vos fournisseurs sont principalement des sociétés pharmaceutiques et des distributeurs de produits de grande consommation, vous pouvez créer un nouveau groupe de fournisseurs pour eux et nommer les groupes en conséquence.

---

Pour accéder au **groupe de fournisseurs**, allez sur :

> Accueil > Achats > Fournisseur > **Groupe de fournisseurs**

## 1. Comment créer un groupe de fournisseurs 

- Accédez à la **liste des groupes de fournisseurs**, cliquez sur :heavy_plus_sign: Nouveau.
- **Est un noeaud** : Si la case est coché, cela créera un groupe parent et vous pourrez créer des sous-groupes de fournisseurs.
- Tapez un **nom** pour votre nouvelle catégorie de fournisseur.
- **Créer Nouveau**

### 1.1 Caractéristiques

- Cochez la case **Est un groupe** en fera un groupe de fournisseurs parents.
- Limite de crédit : Vous pouvez également affecter un modèle de conditions de paiement par défaut au groupe de fournisseurs. Utile dans le cas où tous vos fournisseurs de matériel prennent la moitié du paiement sur la commande client et la moitié après l'expédition.
- **Enregistrer**.

### 1.2 Arborescence des groupes fournisseurs

Vous pouvez également **créer un groupe de fournisseurs** sous la forme d'une **hiérarchie arborescente**, similaire au plan comptable.

Pour afficher l'arborescence, cliquez sur Arborescence dans la barre latérale. Pour revenir à l'affichage de la liste, sélectionnez simplement: Menu > **Afficher la liste** .