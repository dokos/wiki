---
title: Record_Video
description: 
published: false
date: 2021-08-18T10:44:16.799Z
tags: 
editor: markdown
dateCreated: 2021-08-16T09:50:02.120Z
---

# Vidéo

Une Vidéo est un outil de Dokos qui vous permet de configurer les étapes pour terminer l'enregistrement vidéo.

Pour accéder à la Vidéo, allez sur:
> Accueil > Dokops > Vidéo

![1.png](/video/1.png)

<br>

## Comment créer une Vidéo

**Titre**: C'est le titre de ce document que vous pouvez nommer
**Execution URL**: C'est le lien initial sur lequel on fait des opération.
**Video Name**： C‘est le nom de vidéo que vous pouvez nommer.(Attention! Il ne peut pas y avoir d'espace dans le nom.)
**Video Steps**: C'est l'endroit où on peut ajouter des étapes de record.
![2.png](/video/2.png)

<br>

### Comment configurer des étape

**Ajouter une ligne**: Le bouton nous pourmet d'ajouter une étape de record.

**Step Execution**: 2 choix dans ce champ:
- before: Le choix de cette option indique que cette étape est une étape de préparation et n'est pas enregistrée dans la vidéo. Cela a été fait avant le début de la vidéo
- it: Cette option indique que cette étape est une étape d'enregistrement vidéo. Le contenu défini apparaîtra dans la vidéo.
![5.png](/video/5.png)

**Passer**: Véréfier cette option va sauter cette étape.

**Step Type**: Dans cet article, nous pouvons choisir le type d'opération dans cette étape. Il existe plusieur type d'étape:
> Les paramettres de differents Step Type sont dans "Modifier" à l'arrière
{.is-info}
- arrow: Choisissez cette option, vous pouvez définir une flèche dans la vidéo dans cette étape. 
(Dans le parameter derrière vous pouvez définir la position, la taille et la couleur de la flèche.)
- click: La sélection de cette option signifie que cette étape est cliquer sur la souris.
(Il faut définir cliquer est dépendu de quel étape.)
- fill_field: Cette option signifie que cette étape consiste à remplir du contenu dans un certain champion.
(Dans le parameter derrière vous pouvez définir dans quel champ, le contenu rempli, le type de champ.)
- premier: Le premier exécutera le premier bouton de l'élément
- get: Cette option signifie que cette étape consiste à déplacer la souris à une certaine position dans la page Web via l'élément dans la DOM. 
(l'élément peut être défini dans le parameter plus tard)
- get_table_row: Cette option signifie que cette étape consiste à déplacer la souris à une certaine position dans le tableau. 
(la position peut être défini dans le parameter plus tard)
- login: Cette option signifie que cette étape consiste à se connecter le compte de la page.
- new_form: new_form va ouvrir un nouveau document.
- scrollTo: Cette option signifie que l'étape consiste à faire défiler la page.
(Vous pouvez définir la position de début, la position de fin et la durée dans les paramètres suivants)
- subtitle: Cette option signifie que cette étape consiste à définir les sous-titres ci-dessous.
(Vous pouvez définir la durée dans les paramètres suivants)
- visit: Cette option signifie que l'étape consiste à rediriger vers une certaine page Web.
(Vous pouvez définir l'URL dans les paramètres suivants)
- wait: Cette option signifie que l'étape consiste à attendre un moment.
(Vous pouvez définir la durée du temps d'attente dans les paramètres suivants)
![4.png](/video/4.png)

**First Parameter**： Dans ce champ, nous pouvons définir le premier parameter de différents types de contenu en fonction de différents Step Type.

**Motifier**: Cliquer sur "mMtifier",Ici vous permet de ajouter des parametres dans les champs suivants.
Et gliser le plus bas, il y a un champ "Depends on step". Vous pouvez entrer l'index d'étape pour definir cette opération est dépendu cette étape.
Pour des parameters de différents Step Type:
- arrow: Dans le First Parameter,vous pouvez remplir sous la forme:
```
{
	duration: ,
	blocking: ,
	pointAt: ,
	offsetX: ,
	offsetY: ,
	strokeWidth: ,
	color: ,
}
```
"duration" consiste à la durée de temps de cette flèche. "blocking" consiste à s'il faut interrompre une étape en cours. "pointAt" consiste à la position approximatif, vous pouvez utiliser "bottomLeft" "bottomRight" "topLeft""topRight". "offsetX" et "offsetY" peut préciser la place de flèche. "strokeWidth" peut définir la taille de flèche. "color" peut définir sa couleur. 

Par exemple:
```
{
	duration: 5000,
	blocking: true,
	pointAt: 'bottomLeft',
	offsetX: 0,
	offsetY: 220,
	strokeWidth: 5,
	color: 'green',
}
```
- click: Aucun paramètre mais toujours dépendant.
- fill_field: 
Dans le First Parameter: ```le nom du champ```
Dans le Seconde Parameter: ```La valeur```
Dans Third Parameter: ```Type de champ (Date, Data, Link, etc...)```

Par exemple je voudrais remplit le "Thierry Lussier" dans le champ "Nom complet":
Dans le First Parameter: 
```
'customer_name'
```

Dans le Seconde Parameter:
```
"Thierry Lussier"
```

Dans Third Parameter:
```
'Data'
```

- premier: Aucun paramètre mais toujours dépendant.
- get: Dans le First Parameter, vous pouvez remplir l'élément dans la DOM.
Par exemple dans le First Parameter:
```
'[data-original-title="Menu"]'
```
- get_table_row: Dans le First Parameter, vous pouvez remplir le numéro de la ligne.
(
cy.get_table_row(1).get(".btn-open-row").click()
cy.fill_table_field("items", 1, "rate", 17000, "Float")
)
```
1
```
- login: 
Dans le First Parameter: ```identifiant```
Dans le Seconde Parameter: ```mot de passe```

Par exemple je voudrais me connecter le compte de "Thierry Lussier":
Dans le First Parameter: 
```
"thierry@dokos.io"
```
Dans le Seconde Parameter:
```
"tierslieux"
```

- new_form: Dans le First Parameter, vous pouvez remplir le Type de document.
Par exemple:
```
cy.new_form(Material Request)
```

- scrollTo:
Dans le First Parameter: Point de départ du défilement des pages
Dans le Seconde Parameter: Fin du défilement des pages
Dans Third Parameter: La duration de défilement

Par exemple:
```
0
```

Dans le Seconde Parameter:
```
1500
```

Dans Third Parameter:
```
{ duration: 3000 }
```

- subtitle: 
Dans le First Parameter: le text de sous-titre
Dans le Seconde Parameter: des objets de sous-titre
Codes: 
```
cy.subtitle('Enregistrez votre demande.', {
			duration: 5000,
			blocking: true,
			textSize: '32pt',
		})
```
Par exemple dans le premier parameter:

```
'Enregistrez votre demande.'
```    
Dans le deuxième parameter:
```
{
    duration: 5000,
    blocking: true,
}
```
    
- visit: Dans le First Parameter, vous pouvez remplir le url relative.

Par exemple:
```
/app/home
```

- wait: Dans le First Parameter, vous pouvez remplir la durée (ms) d'attente.

Par exemple:
```
300
```

### Last Record Result
Cliquer sur ici pour éteindre le résultat du dernier enregistrement.

### Ajouter un commentaire
Possibilité d'ajouter des commentaire sur ce document.