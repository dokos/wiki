---
title: Projets
description: 
published: true
date: 2022-03-22T11:13:51.392Z
tags: 
editor: markdown
dateCreated: 2021-07-13T08:31:47.233Z
---

# Projets

Il y a beaucoup d'activités où les biens ne sont pas tangibles comme par exemple une société de conseil. Pour montrer leur valeur, ces entreprises doivent suivre et mettre à jour chacun de leurs processus pour rester à flot et se développer dans leur domaine. Le module Projet aide une organisation à suivre ces services et à garantir leur achèvement dans les délais.

Dokos aide toute activité à gérer efficacement les aspects commerciaux tels que la gestion de projet, le support client, les ventes et la gestion des achats, pour n'en citer que quelques-uns. Vous pouvez consulter les rubriques suivantes après avoir parcouru cette introduction.

---

Pour accéder à **la liste de Projets**, allez sur :

> Accueil > **Projets**

![projet.png](/projects/projet.png)

## 1. Projets

- [1. Projet](/fr/projects/project)
- [2. Tâche](/fr/projects/task)
- [3. Modèle de projet](/fr/projects/project-template)
- [4. Type de projet](/fr/projects/project-type)
- [5. Mise à jour du projet](/fr/projects/project-update)
{.links-list}

## 2. Suivi du temps

- [1. Feuille de temps](/fr/projects/timesheet)
- [2. Type d'activité](/fr/projects/activity-type)
- [3. Coût de l'activité](/fr/projects/activity-cost)
{.links-list}



