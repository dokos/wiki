---
title: Stocks
description: 
published: true
date: 2023-01-18T16:21:55.704Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:07:14.129Z
---

# Stock / Inventaire
Le module Stock ou Inventaire de DOKOS vous aide à :

- Maintenir les détails de vos produits et entrepôts.
- Suivre la réception, le transfert et la livraison des produits.
- Optimiser les niveaux de stock en fournissant des informations en temps réel via des rapports.
- Fournir une évaluation de chaque produit.

Le module Stock est étroitement intégré aux modules d'achat, de fabrication et de comptabilité.

## 1. Fonctionnalités de base

- [1. Informations sur les articles](/fr/stocks/item-info)
- [2. Articles](/fr/stocks/item)
- [3. Prix d'article](/fr/stocks/item-price)
- [4. Groupe d'article](/fr/stocks/item-group)
- [5. Ensemble de produits](/fr/stocks/product-bundle)
- [6. Entrepôt](/fr/stocks/warehouse)
- [7. Bon de livraison](/fr/stocks/delivery-note)
- [8. Bordereau de colis](/fr/stocks/packing-slip)
- [9. Stock d'ouverture](/fr/stocks/opening-stock)
{.links-list}

## 2. Installation

- [1. Paramètres des stocks](/fr/stocks/stock-settings)
- [2. Prix d'article](/fr/stocks/item-price)
- [3. Groupe d'article](/fr/stocks/item-group)
- [4. Liste de prix](/fr/stocks/price-list)
- [5. Bon de livraison](/fr/stocks/delivery-note)
- [6. Unité de Mesure](/fr/stocks/uom)
- [7. Fabricant](/fr/stocks/manufacturer)
{.links-list}


## 3. Opérations de stock

- [1. Écriture de stock](/fr/stocks/stock-entry)
- [2. Demande de matériel](/fr/stocks/material-request)
- [3. Reçu d'achat](/fr/buying/purchase-receipt)
- [4. Bon de livraison](/fr/stocks/delivery-note)
- [5. Numéro de série](/fr/stocks/serial-no)
- [6. Fonction de Lot](/fr/stocks/batch)
- [7. Retour des ventes](/fr/stocks/sales-return)
- [8. Retour des achats](/fr/stocks/purchase-return)
- [9. Conservation des échantillons](/fr/stocks/retain-sample-stock)
- [10. Inspection de la qualité](/fr/stocks/quality-inspection)
{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
