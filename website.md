---
title: Site web
description: 
published: true
date: 2022-03-01T15:41:31.412Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:42:46.494Z
---

# Site web

## 1. Configuration

- [1. Paramètres du site web](/fr/website/website-settings)
- [2. Paramètres du panier](/fr/website/shopping-cart-settings)
- [3. Thème du site web](/fr/website/website-theme)
- [4. Script du site web](/fr/website/website-script)
- [5. Paramétrage de la page A propos](/fr/website/about-us-settings)
- [6. Paramètres du formulaire de contact](/fr/website/contact-us-settings)
{.links-list}


## 2. Blog

- [1. Article de Blog](/fr/website/blog-post)
- [2. Blogueur](/fr/website/blogger)
{.links-list}

## 3. Site internet

- [1. Page Web](/fr/website/web-page)
- [2. Formulaire web](/fr/website/web-forms)
- [3. Barre latérale du site web](/fr/website/portal)
{.links-list}


## 4. Portail
- [1. Portail](/fr/website/portal-settings)
- [2. Trombinoscope](/fr/website/organization-chart)
{.links-list}


> Les liens **Page d'accueil** et **Section de page d'accueil** ne sont plus visibles depuis le module site web car ces modules ont été retirés et ont été remplacés par **Page Web**. Il faut donc dès à présent passer par Site Web > **Page Web** pour créer une page d'accueil.
La page d'accueil est à configuré depuis les paramètres du site en indiquant le lien de la Page d'accueil. (Documentation plus détaillée à venir)
{.is-warning}


Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
