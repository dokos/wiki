---
title: Intéressement des employés
description: 
published: true
date: 2021-06-21T08:03:02.323Z
tags: 
editor: markdown
dateCreated: 2021-06-21T07:59:53.784Z
---

# Intérenssement des employés

Les intéressements aux employés sont un moyen de récompenser et de motiver les performances des employés en dehors du salaire habituel.

Lorsqu'une organisation souhaite encourager la productivité de ses employés, l'une des options disponibles consiste à récompenser l'employé avec une prime d'intéressement. DOKOS vous permet de créer des intéressements aux employés au fur et à mesure des besoins pour une entrée de paie particulière.

---

Pour accéder à **la liste intéressement des employés**, allez sur :

> Accueil > Paie > **Intéressement des employés**

![liste_intéressement_des_employés.png](/payroll/employee-incentive/liste_intéressement_des_employés.png)

## 1. Prérequis avant utilisation

Avant de créer une incitation pour les employés, il est conseillé de créer les éléments suivants :

- **[Employé](/fr/human-resources/employee)**
- **[Composante salariale](/fr/payroll/salary-component)**

## 2. Comment créer un incitatif pour les employés 

1. Accédez à la liste des incitations aux employés, cliquez sur **:heavy_plus_sign: Ajouter Intéressement des employés**.
2. Sélectionnez l'**employé**.
3. Saisissez le **montant** de l'incitatif.
4. Sélectionnez la **date** de paie.
5. Sélectionnez la **composante salariale** sous laquelle vous souhaitez offrir l'incitatif.
6. **Enregistrer** et envoyer.
7. Lors de la soumission, le document **Salaire supplémentaire** de la **Composante salariale** spécifiée est créé. Celui-ci sera récupéré lors de l'exécution de la saisie de la paie.

![détails_intéressement.png](/payroll/employee-incentive/détails_intéressement.png)