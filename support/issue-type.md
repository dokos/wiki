---
title: Type de problème
description: 
published: true
date: 2022-11-02T08:54:01.074Z
tags: 
editor: markdown
dateCreated: 2021-07-22T09:28:51.220Z
---

# Type de problème

Le type de problème est utile pour marquer et classer les tickets.

La classification des problèmes aide à affecter les membres de l'équipe concernés à des problèmes spécifiques. Des exemples de types de problèmes peuvent être : « Fonctionnel », « Technique », « Matériel », etc. Ainsi, les ingénieurs peuvent être affectés à des problèmes techniques ou matériels et des consultants seraient affectés à des problèmes fonctionnels.

---

Pour accéder à **la liste des types de ticket**, allez sur :

> Accueil > Support > Tickets > **Type de ticket**

## 1. Comment créer un type de ticket ?

1. Pour créer un type de ticket, cliquez sur **:heavy_plus_sign: Ajouter Type de ticket**.
2. Saisissez un **nom** pour le type. 
3. Une **description** peut être ajoutée.
4. **Enregistrer**