---
title: Priorité de ticket
description: 
published: true
date: 2022-11-02T08:54:17.560Z
tags: 
editor: markdown
dateCreated: 2021-07-22T10:05:35.835Z
---

# Priorité de ticket

La priorité du ticket indique l'urgence de résoudre un problème.

L'utilisateur peut créer des priorités telles que "Haut", "Moyen", "Faible", "Critique", etc. La priorité de problème sera utilisée lors de la création de l' accord de niveau de service.

---

Pour accéder à **la liste Priorité des problèmes**, allez sur :

> Accueil > Support > Ticket > **Priorité du ticket**

## 1. Comment créer une Priorité de ticket ?

1. Pour créer une priorité de ticket, cliquez sur **:heavy_plus_sign: Ajouter Priorité de ticket**. 
2. Entrez un **nom** pour le type. 
3. Une **description** peut être ajoutée.
4. **Enregistrer**