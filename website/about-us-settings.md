---
title: Paramétrages de la page À Propos
description: 
published: true
date: 2021-07-20T12:42:22.985Z
tags: 
editor: markdown
dateCreated: 2021-07-20T12:03:47.917Z
---

# Paramétrages de la page À Propos

DOKOS vous donne la possibilité de configurer la page web **A propos**. Sur cette page vous pourrez expliquer l'histoire de votre société, détaillez les membres de votre équipe.

---

Pour accéder **aux paramétrages de la page A Propos**, allez sur :

> Accueil > Site web > **Paramétrages de la page A propos**

## 1. Titre de la page

- **Titre de la page** : Indiquez ici le titre de votre page A propos
- **Présentation de la société** : Faites une présentation en quelques lignes de votre activité.

![titre_de_la_page.png](/site-web/about-us-settings/titre_de_la_page.png)

## 2. Histoire de la société

- **La société** : Indiquez de quelle société il s'agit
- **Histoire de la société** : Ajoutez les informations historiques de votre société (Année de création, Ouvertures de points de vente, etc..)

![histoire_de_la_société.png](/site-web/about-us-settings/histoire_de_la_société.png)

## 3. Membres de l'équipe

- **Titre des Membres de l'Équipe** : Indiquez le nom pour les membres de l'équipe
- **Sous-titre pour les membres de l'équipe**
- **Membres de l'équipe** : Ajoutez dans la table tous les membres de votre équipe.
- **Pied de Page** : Indiquez des élements qui vont figurer en pied de page. 

![membres_de_l'équipe.png](/site-web/about-us-settings/membres_de_l'équipe.png)