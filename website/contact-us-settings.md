---
title: Paramètres du formulaire de contact
description: 
published: true
date: 2021-07-20T12:55:58.145Z
tags: 
editor: markdown
dateCreated: 2021-07-20T12:55:58.145Z
---

# Paramètres du formulaire de contact

DOKOS vous permet de renseigner les paramètres pour le formulaire de contact. 

## 1. Introduction

- **Transférer à l'adresse email** : Envoyez une demande à l'adresse email indiquée.
- **Titre** : Le titre du formulaire de contact. Par défaut, le nom sera **Nous contacter**
- **Introduction*** : Indiquez des informations pour introduire votre formulaire de contact.
- **Options de requête**: Les options de contact, comme "Demande de Ventes, Demande d'Aide" etc., doivent être chacune sur une nouvelle ligne ou séparées par des virgules.

![introduction_form.png](/site-web/contact-us-settings/introduction_form.png)

## 2. Adresse

Dans cette section, remplissez tous les champs pour renseigner l'adresse.

- **Titre de l'adresse**
- **Adresse, Complément d'adresse**
- **Ville, État, Code Postal, Pays**
- **Téléphone, Email, Skype**

![adresse_form.png](/site-web/contact-us-settings/adresse_form.png)