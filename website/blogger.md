---
title: Blogueur
description: 
published: true
date: 2021-07-07T13:24:36.641Z
tags: 
editor: markdown
dateCreated: 2021-07-07T13:23:30.738Z
---

# Blogueur

Un blogueur ou une blogueuse est une personne qui comme son nom l'indique, tient un blog. Il peut s'agir d'un blog de cuisine, de mode, de beauté, de culture (musique/bons plans/cinéma, etc.), de voyage, de décoration…. 
Vous pouvez fournir une courte biographie du blogueur et un avatar.

---

Pour accéder à la liste Blogueur, allez sur :

> Accueil > Site web > Blog > **Blogueur**

![liste_blogueur.png](/site-web/blogger/liste_blogueur.png)

## 1. Comment créer un blogueur ?

1. Accéder à la liste Blogueur, cliquez sur **:heavy_plus_sign: Ajouter Blogueur**.
2. Ajoutez un **nom**, un **nom complet**.
3. Sélectionnez l'**utilisateur**.
4. Renseignez une **biographie**.
5. Joignez une photo d'**avatar** pour le blogueur.
6. **Enregistrez**.

![détails_blogueur.png](/site-web/blogger/détails_blogueur.png)
