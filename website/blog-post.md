---
title: Article de blog
description: 
published: true
date: 2021-07-06T09:12:50.980Z
tags: 
editor: markdown
dateCreated: 2021-07-05T13:48:25.912Z
---

# Article de blog

Un article de blog est un article sur votre site Web.

Les blogs sont un excellent moyen de partager vos réflexions sur votre entreprise. Il permet de tenir vos clients et lecteurs informés des actualités liées à votre entreprise.

À l'ère d'Internet, l'écriture revêt une grande importance car lorsque les gens visitent votre site Web, ils veulent en savoir plus sur vous et votre produit.

---

Pour accéder à **la liste article de blog**, allez sur :

> Accueil > Site Web > Blog > **Article de blog**

![articles_de_blog.png](/site-web/blog-post/articles_de_blog.png)

## 1. Comment créer un article de blog ?

1. Accédez à la liste des articles de blog et cliquez sur **:heavy_plus_sign: Ajouter Article de Blog**.
2. Saisissez le **titre**, la **catégorie de blog**, le **blogueur** et le **contenu**.
3. Activez **Publié** et cliquez sur **Enregistrer**.

![détails_du_blog.png](/site-web/blog-post/détails_du_blog.png)

L'introduction du blog est une brève description de votre blog qui apparaît juste après le titre et avant le contenu.

![intro_du_blog.png](/site-web/blog-post/intro_du_blog.png)

Vous pouvez écrire votre blog en Rich Text, Markdown ou HTML. Si vous souhaitez écrire des pages de contenu simples, Rich Text et Markdown sont d'excellentes options. Apprenez à joindre des images à votre blog ici.

Apprenez le markdown en quelques minutes sur <a href="https://guides.github.com/features/mastering-markdown/" target="_blank">Mastering Markdown</a>.

Affichez votre article de blog en cliquant sur Voir sur le site Web dans la barre latérale.

Cochez l' option Désactiver les commentaires si vous souhaitez désactiver la possibilité pour les lecteurs d'ajouter des commentaires à l'article de blog actuel.

## 2. Caractéristiques

### 2.1 Blogueur

Blogger est un utilisateur qui peut publier des blogs. Pour créer un blogueur, accédez à :

> Accueil > Site Web > Blog > **Blogueur**

Vous pouvez mentionner une courte biographie sur le blogueur et également définir un avatar ici.

### 2.2 Catégorie de blog

Vous pouvez regrouper vos blogs en catégories. Pour créer une nouvelle catégorie de blog, accédez à :

> Accueil > Site Web > Blog > **Catégorie Blog**

Si vous cliquez sur Voir sur le site Web dans la barre latérale, vous serez redirigé vers une liste de blogs dans cette catégorie.

### 2.3 Balises Méta

Les balises méta sont des balises HTML qui décrivent une page et son contenu à un moteur de recherche ou à une plate-forme de médias sociaux. Une méta description fournit une brève description d'une page Web.

En HTML, la balise meta description s'écrira comme suit :

<head> <meta name="description" content="This is an example of a meta description. This will often show up in search results."> </head>

![meta.png](/site-web/blog-post/meta.png)

Dans la section SEO, vous pouvez ajouter la méta description et l'image de votre article de blog. DOKOS générera un aperçu du blog lorsqu'il sera répertorié comme résultat de recherche par Google.

Si vous laissez cette section vide, l'introduction du blog ou les 140 premiers caractères du blog seront affichés comme méta description. De la même manière, si vous laissez la méta image vide, la première image du blog sera sélectionnée.

Lors de la soumission du blog, vous pouvez également prévisualiser à quoi ressemblera le blog lorsqu'il sera partagé sur une plate-forme de médias sociaux comme Facebook ou Twitter. Pour le vérifier, utilisez les outils de débogage proposés par les plateformes :

- **Facebook** : Débogueur de partage

- **Twitter** : validateur de carte

- **LinkedIn** : inspecteur de poste

Pour vérifier l'aperçu de la publication, entrez simplement le lien page Web/blog dans l'outil :

En utilisant ces outils, vous pouvez optimiser votre article de blog pour le partage.


