---
title: Lieu
description: 
published: true
date: 2022-10-28T15:01:38.098Z
tags: 
editor: markdown
dateCreated: 2020-11-26T16:48:16.179Z
---

# Gestion d'un lieu

Ce module a pour objet de regrouper les éléments principaux permettant de gérer un lieu physique. Celui-ci peut être constitué de bureaux, d'espaces de co-working, de salles de réunion, mais également de machines mises à disposition.

---

Pour accéder au module des **Lieux**, allez sur :
> Accueil > **Lieu**

# Premiers pas

- [Configuration de Mon Tiers Lieu *Cas d'usage*](/fr/venue/use-case)
{.links-list}

## 1. Abonnement

- [1. Abonnement](/fr/selling/subscription)
- [2. Plan d'abonnement](/fr/selling/subscription-plan)
- [3. Modèle d'abonnement](/fr/selling/subscription-template)
- [Configurer un abonnement sur le portail utilisateur](/fr/selling/subscription-plan-portal)

{.links-list}

## 2. Réservations et événements

- [1. Événement](/fr/venue/event)
- [2. Réservation d'articles](/fr/venue/item-booking)
- [3. Poste d'événement](/fr/venue/event-slot)
- [4. Inscription à un poste d'événement](/fr/venue/event-slot-booking)
- [5. Inscription à un événement](/fr/venue/event-registration)
- [6. Calendrier de réservation d'articles](/fr/venue/item-booking-calendar)
- [7. Paramètres du lieu](/fr/venue/venue-settings)
{.links-list}

## 3. Réservations et événements

- [1. Crédit de réservation](/fr/venue/booking-credit)
- [2. Utilisation de crédit de réservation](/fr/venue/booking-credit-usage)
- [3. Règle de crédit de réservation](/fr/venue/booking-credit-rule)
{.links-list}

## 4. Site web et portail

- [1. Paramètres du site web](/fr/website/website-settings)
- [2. Paramètres du portail](/fr/website/portal-settings)
- [3. Paramètres du panier](/fr/website/shopping-cart-settings)
{.links-list}

## Abonnements

Les abonnements permettent de mettre en place une facturation récurrente pour un client.  
Vous pouvez par exemple créer un abonnement un forfait mensuel, ce qui veut dire que Dokos générera une facture par mois pour ce client jusqu'à annulation de l'abonnement.

- [Abonnements](/fr/selling/subscription)
{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
