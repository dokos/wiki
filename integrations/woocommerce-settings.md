---
title: Paramètres Woocommerce
description: 
published: true
date: 2021-11-29T10:27:24.703Z
tags: 
editor: markdown
dateCreated: 2021-11-29T09:55:02.910Z
---

# Paramètres Woocommerce

À l'aide de l'intégration WooCommerce, le système crée des commandes client dans DOKOS pour les commandes créées sur WooCommerce à l'aide du webhook WooCommerce.

Lors de la création d'une commande client à partir de WooCommerce, si le client ou l'article est manquant dans DOKOS, le système créera un nouveau client/article en utilisant les détails respectifs des données de commande WooCommerce. Il crée également une adresse liée au client en utilisant les détails d'expédition des données de commande.

## 1. Comment configurer WooCommerce ?

### 1.1 Générer la clé API et le secret API

1. Dans la barre latérale de votre site WooCommerce, cliquez sur **Paramètres**.
2. Cliquez sur l'onglet **Avancé** puis cliquez sur le lien API REST.

![wc-add-key.png](/settings/woocommerce/wc-add-key.png)


3. Cliquez sur le bouton **Ajouter une clé**. Fournissez les détails nécessaires et cliquez sur le bouton **Générer la clé API**.

![wc-generate-keys.png](/settings/woocommerce/wc-generate-keys.png)

### 1.2 Paramètres Woocommerce

1. Sur votre site DOKOS, allez dans : **Accueil > Intégrations > Paramètres > Paramètres Woocommerce**.
2. Collez la clé API et le secret générés à l'étape précédente dans les champs **Clé consommateur API** et **Secret consommateur API**.
2. Dans l'**URL du serveur Woocommerce**, collez l'url de votre site WooCommerce.
3. Assurez-vous que **Activer la synchronisation** est coché.
4. Sélectionnez le « **Compte de taxe** » et le « **Compte de fret et d'expédition** dans la section Détails du compte.
5. Sélectionnez **Création d'utilisateurs** dans la section Paramètres par défaut. Cet utilisateur sera utilisé pour créer des clients, des articles et des commandes clients. Assurez-vous que l'utilisateur dispose des autorisations appropriées.
6. Sélectionnez la **Société** qui sera utilisée pour créer les commandes client.
7. Cliquez sur **Enregistrer**.
8. Après avoir enregistré les paramètres Woocommerce, **Secret** et **Endpoint** sont générés automatiquement.

![paramètres_woocommerce.png](/settings/woocommerce/paramètres_woocommerce.png)

### 1.3 Paramètres du Webhook Woocommerce

1. Maintenant, depuis la barre latérale de votre site woocommerce, allez dans **Paramètres**.
2. Cliquez sur l'onglet **Avancé** puis sur le lien **Webhooks** puis sur le bouton **Ajouter un webhook**.
3. Donnez au **webhook un nom de votre choix**.
4. Cliquez sur la liste déroulante Statut et sélectionnez **Actif**.
5. Sélectionnez le sujet comme **Commande créée**.
6. Copiez le doctype **Endpoint** du **Woocommerce Settings** dans votre site DOKOS et collez-le dans le champ **Delivery URL**.
7. Copiez **Secret** du doctype **Paramètres Woocommerce** dans votre site DOKOS et collez-le dans le champ **Secret**.
8. Conservez la VERSION API telle quelle et cliquez sur Enregistrer le Webhook.

![wc-webhook.png](/settings/woocommerce/wc-webhook.png)

## 2. Caractéristiques

### 2.1 Paramètres par défaut

Dans le DocType des paramètres Woocommerce :

- **Entrepôt** : Cet entrepôt sera utilisé pour créer des commandes clients. L'entrepôt par défaut est "Magasins".
- **Livraison après (jours)** : Il s'agit du décalage par défaut (jours) pour la date de livraison dans les commandes clients. Le décalage par défaut est de 7 jours à compter de la date de passation de la commande.
- **Série de commandes clients** : vous pouvez définir une série distincte pour les commandes clients créées via woocommerce. La série par défaut est "SO-WOO-".
- **Unité de Mesure** : Il s'agit de l'UdM par défaut utilisée pour les articles et les commandes client. L'UOM par défaut est « Nos ».