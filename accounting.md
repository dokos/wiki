---
title: Comptabilité
description: 
published: true
date: 2022-04-28T14:50:59.324Z
tags: 
editor: markdown
dateCreated: 2020-11-26T12:24:00.536Z
---

# Comptabilité

Que vous ayez un comptable dans votre équipe interne OU que vous le fassiez vous-même OU que vous ayez choisi de l'externaliser, le processus de comptabilité financière est au centre de tout système de gestion d'entreprise (alias un système ERP).

Dans DOKOS, les opérations comptables se composent de 3 opérations principales :

- **Facture de vente**: Les factures que vous facturez à vos clients pour les produits ou services que vous fournissez.
- **Facture d'achat** : factures que vos fournisseurs vous remettent pour leurs produits ou services.
- **Écritures de journal** : pour les écritures comptables, comme les paiements, les crédits et d'autres types.

## 1. Données de base

- [1. Société](/fr/accounting/company)
- [2. Plan comptable](/fr/accounting/account)
- [3. Paramètres de comptabilité](/fr/accounting/accounts-settings)
- [4. Exercice fiscal](/fr/accounting/fiscal-year)
- [5. Dimension comptable](/fr/accounting/accounting-dimension)
- [6. Journal Comptable](/fr/accounting/accounting-journal)
- [7. Période comptable](/fr/accounting/accounting-period)
- [8. Terme de paiement](/fr/accounting/payment-term)
{.links-list}

## 2. Grand livre

- [1. Grand livre](/fr/accounting/GeneralLedger)
- [2. Écriture de journal](/fr/accounting/journal-entry)
- [3. Modèle d'écriture de journal](/fr/accounting/journal-entry-template)
{.links-list}

## 3. Comptes débiteurs

- [1. Facture de vente](/fr/selling/sales-invoice)
- [2. Ecriture de paiement](/fr/accounting/payment-entry)
- [3. Demande de paiement](/fr/accounting/payment-request)
{.links-list}

## 4. Comptes créditeurs

- [1. Ecriture de paiement](/fr/accounting/payment-entry)
{.links-list}

## 5. États financiers

- [1. Le Bilan Comptable](/fr/accounting/balance-sheet)
- [2. Le Compte de résultat](/fr/accounting/profit-and-loss-statement)
- [3. La Balance générale](/fr/accounting/profit-and-loss-statement)
{.links-list}

## Paramètres

- [1. Facture d'acompte](/fr/accounting/down-payment-invoice)
- [2. Prélèvement Sepa](/fr/accounting/sepa-direct-debit)
- [3. Passerelles de paiement](/fr/accounting/payment-gateways)
- [4. Rapprochement bancaire](/fr/accounting/bank-reconciliation)
- [5. Synchronisation bancaire](/fr/accounting/bank-synchronization)
{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
