---
title: Achats
description: 
published: true
date: 2022-10-14T10:44:45.523Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:02:29.182Z
---

# Achat

Si votre entreprise implique des achats de biens physiques, de prestations de services ou de tout autre type de produit, l'**achat** est une fonction indispensable pour votre activité. Vos fournisseurs sont aussi importants que vos clients et ils doivent recevoir des informations précises.

L'achat des bonnes quantités dans les bons montants peut avoir un impact positif sur votre flux de trésorerie et votre rentabilité. **DOKOS** contient un ensemble de fonctionnalité qui rendra votre processus d'achat aussi efficace et transparent que possible.

Voici à quoi ressemble un flux d'achat :

```mermaid
flowchart LR
  id2(Commande fournisseur)-->id3(Reçu d'achat)
  id2-->id4(Facture d'achat)
  id4-->id5(Ecriture de paiement)
  id2-->id6("Ecriture de paiement (Acompte)")
  id1(Demande de matériel)-->id7(Appel d'offre)
 	id7-->id8[Devis Fournisseur]
  id1-->id2
  id8-->id2
```
 
---

Pour accéder au module des **Achats**, allez sur :
> Accueil > **Achats**

## 1. Cycle d'achat

- [1. Demande de matériel](/fr/stocks/material-request)
- [2. Commande fournisseur](/fr/buying/purchase-order)
- [3. Facture d'achat](/fr/buying/purchase-invoice)
- [4. Appel d'offre / Demande de devis](/fr/buying/request-for-quotation)
- [5. Devis fournisseur](/fr/buying/supplier-quotation)
- [6. Reçu achat](/fr/buying/purchase-receipt)
{.links-list}

## 2. Articles et prix

- [1. Article](/fr/stocks/item)
- [2. Prix de l'article](/fr/stocks/item-price)
- [3. Liste de prix](/fr/stocks/price-list)
- [4. Ensemble de produits](/fr/stocks/product-bundle)
- [5. Groupe d'article](/fr/stocks/item-group)
- [6. Schéma promotionnel](/fr/stocks/promotional-scheme)
- [7. Règles de prix](/fr/stocks/price-rules)
{.links-list}


## 3. Fournisseur

- [1. Fournisseur ](/fr/buying/supplier)
- [2. Groupe de fournisseurs](/fr/buying/supplier-group)
- [3. Contact](/fr/crm/contact)
- [4. Adresse](/fr/crm/address)
{.links-list}

## 4. Fiche d'évaluation des fournisseurs

- [1. Fiche d'évaluation des fournisseurs](/fr/buying/supplier-scorecard)
{.links-list}

## 5. Paramètres

- [1. Paramètres d'achat](/fr/buying/buying-settings)
- [2. Modèle de taxes et frais d'achat](/fr/buying/purchase-taxes-and-charges-template)
- [3. Modèle termes et conditions](/fr/settings/terms-and-conditions)
{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
