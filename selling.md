---
title: Ventes
description: 
published: true
date: 2022-10-14T11:00:43.396Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:55:23.485Z
---

# Vente

La gestion des ventes est un processus cruciale pour toutes les activités. Il est important de suivre facilement le cycle de vente, que vous commercialisez des produits physiques, des prestations de services ou même des abonnements.

Avec DOKOS, le module de Vente vous permet de créer des devis pour vos propsects, des commandes clients et des factures de vente. De plus, avec la mise en place de nombreux rapports et indicateurs, vous allez pouvoir suivre facilement la progression de vos ventes.

**Voici à quoi ressemble un flux de vente :**

```mermaid
flowchart LR
	id4-->id9(Bon de livraion)
  id2(Commande client)-->id4(Facture de vente)
  id4-->id5(Ecriture de paiement)
  id2-->id6("Ecriture de paiement (Acompte)")
  id1(Piste)-->id7(Opportunité)
 	id7-->id8[Devis Client]
  id8-->id2
```
---

Pour accéder au module des Ventes, allez sur :

> Accueil > **Vente**

## 1. Cycle de vente
- [1. Client](/fr/crm/customer)
- [2. Devis client](/fr/selling/quotation)
- [3. Commande client](/fr/selling/sales-order)
- [4. Facture de vente](/fr/selling/sales-invoice)
- [5. Bon de livraison](/fr/stocks/delivery-note)
- [6. Partenaire commercial](/fr/selling/sales-partner)
- [7. Abonnement](/fr/selling/subscription)
{.links-list}

## 2. Articles et prix
- [1. Articles](/fr/stocks/item)
- [2. Prix de l'article](/fr/stocks/item-price)
- [3. Liste de prix](/fr/stocks/price-list)
- [4. Groupe d'article](/fr/stocks/item-group)
- [5. Ensemble de produits](/fr/stocks/product-bundle)
- [6. Schéma promotionnel](/fr/stocks/promotional-scheme)
- [7. Règles de prix](/fr/stocks/price-rules)
- [8. Règle de livraison](/fr/stocks/shipping-rule)
- [9. Code promotionnel](/fr/selling/coupon-code)
{.links-list}

## 3.  Paramètres
- [1. Paramètres de vente](/fr/selling/selling-settings)
- [2. Modèle de taxe et frais de vente](/fr/selling/sales-taxes-and-charges-template)
- [3. Modèles termes et conditions](/fr/settings/terms-and-conditions)
- [4. Source du prospect](/fr/crm/lead_source)
- [5. Groupe de clients](/fr/crm/customer-group)
- [6. Contact](/fr/crm/contact)
- [7. Adresse](/fr/crm/address)
- [8. Région](/fr/selling/territory)
- [9. Campagne](/fr/crm/campaing)
{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.

