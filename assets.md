---
title: Actifs
description: Immobilisations corporellles et incorporelles
published: true
date: 2021-08-27T11:34:28.631Z
tags: 
editor: markdown
dateCreated: 2020-12-21T07:47:33.124Z
---

# Actifs

Dans DOKOS, vous pouvez conserver des enregistrements d'immobilisations pour les actifs de l'entreprise tels que les ordinateurs, les meubles, les voitures, etc. et gérer leurs amortissements, leur vente ou leur mise en rebut. Vous pouvez suivre les emplacements des actifs ou conserver des enregistrements des employés qui utilisent l'actif. Vous pouvez également gérer les détails de maintenance des actifs.

--- 

Pour accéder au module d'**Actifs**, allez sur :

> Accueil > **Actifs**


## 1. Actifs

- [1. Actif](/fr/assets/asset)
- [2. Catégorie d'actif](/fr/assets/asset-category)
- [3. Lieu](/fr/assets/asset-location)
- [4. Mouvement d'actif](/fr/assets/asset-movement)
{.links-list}

## 2. Entretien

- [1. Équipe de maintenance](/fr/assets/asset-maintenance-team)
- [2. Maintenance des actifs](/fr/assets/asset-maintenance)
- [3. Journal maintenance des actifs](/fr/assets/asset-maintenance-log)
- [4. Réparations des actifs](/fr/assets/asset-repair)
{.links-list}

## 3. Transaction d'un actif
- [1. Ajustement de la valeur d'actif](/fr/assets/asset-value-adjustment)
- [2. Achat d'un actif](/fr/assets/purchasing-an-asset)
- [3. Vendre un actif](/fr/assets/selling-an-asset)
- [4. Enregistrer un nouvel actif immobilisé](/fr/assets/getting-started)
{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
