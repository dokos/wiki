---
title: Support
description: 
published: true
date: 2021-07-26T12:16:47.472Z
tags: 
editor: markdown
dateCreated: 2021-07-21T09:49:51.357Z
---

# Support

Un excellent support client et une excellente maintenance sont au cœur de toute entreprise prospère. Avec DOKOS, vous suivez toutes les demandes et problèmes entrants des clients et vous y répondez rapidement. La base de données des requêtes entrantes vous aidera également à identifier les plus grandes opportunités d'amélioration.

Vous pouvez suivre les requêtes entrantes à partir de votre courrier électronique à l'aide des tickets de support. Vous pouvez suivre les problèmes des clients liés à un numéro de série spécifique et y répondre en fonction de leur garantie et d'autres informations. Vous pouvez également établir des calendriers de maintenance pour les numéros de série et conserver un enregistrement de toutes les visites de maintenance effectuées auprès de vos clients.

--- 

Pour accéder au module **Support**, allez sur :

> Accueil > **Support**

## 1. Tickets

- [1. Ticket](/fr/support/issue)
- [2. Type de ticket](/fr/support/issue-type)
- [3. Priorité de ticket](/fr/support/issue-priority)
{.links-list}

## 2. Entretien

- [1. Échéancier d'entretien](/fr/support/maintenance-schedule)
- [2. Visite d'entretien](/fr/support/maintenance-visit)
{.links-list}

## 3. Accord de niveau de service

- [1. Accord de niveau de service](/fr/support/service-level-agreement)
{.links-list}

## 4. Garantie

- [1. Réclamation de Garantie](/fr/support/warrantie-claim)
- [2. N° de série](/fr/stocks/serial-no)
{.links-list}

## 5. Paramètres

- [1. Paramètres du support](/fr/support/support-settings)
{.links-list}
