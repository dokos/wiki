---
title: Démarrer avec Dokos
description: 
published: true
date: 2022-12-15T14:06:26.886Z
tags: 
editor: markdown
dateCreated: 2020-11-26T14:15:30.702Z
---

# Installation

- [Installation Rapide sur Ubuntu / Debian](/fr/getting-started/easy-installation)
- [Installation détaillée sur RHEL 8/9](/fr/getting-started/Installation_rhel_8_9)
- [Migration ERPNext](/fr/getting-started/erpnext-migration)
- [DNS Multi-tenant](/fr/getting-started/domain-setup)
- [Certificats SSL avec Let's Encrypt](/fr/getting-started/ssl-certificate)
{.links-list}

# Mise en place

- [Configuration de la société](/fr/accounting/company)
- [Configurer les taxes](/fr/settings/account)
- [Valeurs par défaut global](/fr/settings/global-defaults)
{.links-list}

# Maintenance

- [Mettre à jour Dokos](/fr/getting-started/updating-dokos)
{.links-list}

# Configuration

- [Formats d'impression](/fr/settings/print/print-format)
- [Paramètrage des emails](/fr/getting-started/email-setup)
- [Modèles d'adresses](/fr/getting-started/address-templates)
{.links-list}