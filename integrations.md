---
title: Intégrations
description: 
published: true
date: 2021-11-29T14:57:43.805Z
tags: 
editor: markdown
dateCreated: 2020-11-26T16:38:48.410Z
---

# Intégrations

## 1. Sauvegarde

- [1. Paramètres Dropbox](/fr/integrations/dropbox-settings)
- [2. Paramètres de sauvegarde S3](/fr/integrations/s3-backup-settings)
- [3. Google Drive](/fr/integrations/google-drive)
{.links-list}


## 2. Services Google

- [1. Paramètres Google](/fr/integrations/google-settings)
- [2. Contacts Google](/fr/integrations/google-contacts)
- [3. Google Agenda](/fr/integrations/google-calendar)
- [4. Google Drive](/fr/integrations/google-drive)
{.links-list}

## 3. Authentification

- [Clé de connexion sociale](/fr/integrations/social-login-key)
- [Paramètres LDAP](/fr/integrations/ldap-settings)
- [Client OAuth](/fr/integrations/oauth-client)
- [Paramètres du Fournisseur OAuth](/fr/integrations/oauth-provider-settings)
{.links-list}


## 4. Places de marché

- [Paramètres Woocommerce](/fr/integrations/woocommerce-settings)
- [Paramètres Amazon MWS](/fr/integrations/amazon-mws-settings)
- [Paramètres de Shopify](/fr/integrations/shopify-settings)
{.links-list}

## 5. Paiements

- [GoCardless](/fr/integrations/gocardless)
- [Stripe](/fr/integrations/stripe)
- [Paypal](/fr/integrations/paypal)
{.links-list}

## 6. Paramètres

- [Webhook](/fr/integrations/webhook)
- [URL de webhook entrant](/fr/integrations/incoming-webhook-url)
- [Paramètres des SMS](/fr/integrations/sms-settings)
- [Paramètres Plaid](/fr/integrations/plaid-settings)
- [Paramètres Exotel](/fr/integrations/exotel-settings)
{.links-list}

## 7. Connecteurs
- [Zapier](/fr/integrations/zapier)
- [Slack](/fr/integrations/slack)
- [Google Chat](/fr/integrations/google-chat)
- [Rocket Chat](/fr/integrations/rocket-chat)
- [Mattermost](/fr/integrations/mattermost)



{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
