---
title: CRM
description: 
published: true
date: 2022-10-28T14:48:13.891Z
tags: 
editor: markdown
dateCreated: 2021-05-18T09:21:20.059Z
---

# CRM
DOKOS vous aide à suivre les opportunités commerciales des prospects et des clients, à leur envoyer des devis et à réserver des commandes client.

--- 

Pour accéder au module de **CRM**, allez sur :

> Accueil > **CRM**


## 1. Pipeline de vente

- [1. Piste](/fr/crm/lead)
- [2. Opportunité](/fr/crm/opportunity)
- [3. Prospect](/fr/crm/prospect)
- [4. Client](/fr/crm/customer)
- [5. Contact](/fr/crm/contact)
- [6. Source du prospect](/fr/crm/lead_source)
- [7. Contrat](/fr/crm/contract)
- [8. Rendez-vous](/fr/crm/appointment)
- [9. Newsletter](/fr/crm/newsletter)
{.links-list}

## 2. Paramètres

- [1. Différence entre Prospect, Client et Contact](/fr/crm/difference_between_lead_contact_and_customer)
- [2. Groupe de clients](/fr/crm/customer-group)
- [3. Région](/fr/selling/territory)
- [4. Vendeur](/fr/crm/sales-person)
- [5. Paramètres LinkedIn](/fr/crm/linkedin-settings)
- [6. Paramètres Twitter](/fr/crm/twiter-settings)

{.links-list}

## 3. Campagne

- [1. Campagne](/fr/crm/campaing)
- [2. Publication sur les réseaux sociaux](/fr/crm/social-media-post)
- [3. Campagne Emailing](/fr/crm/email-campaign)
{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
