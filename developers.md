---
title: Développeurs
description: 
published: true
date: 2022-09-05T15:12:42.339Z
tags: 
editor: markdown
dateCreated: 2021-01-14T14:01:15.375Z
---

> Pour installer Dodock/Dokos, consultez le [guide d'installation](/fr/getting-started)
{.is-info}

# Développeurs

## 1. API

- [API de documents](/fr/developers/python-documents-api)
- [API de base de données](/fr/developers/python-database-api)
- [API REST](/fr/developers/rest-api)
- [API Jinja](/fr/developers/python-jinja-api)
{.links-list}

## 2. Exemples d'appels à l'API

- [Réserver un article](/fr/developers/item-booking-call)
- [Passer une commande](/fr/developers/sales-order-call)
- [Récupérer la liste des contacts et adresses associés à un client](/fr/developers/get-contacts-addresses)
{.links-list}