---
title: Lancement de Dokos
description: 
published: false
date: 2021-11-04T16:43:05.567Z
tags: 
editor: markdown
dateCreated: 2021-10-27T13:27:08.059Z
---

# Fonctionnement du logiciel

Dans ce premier cas d’usage, nous allons voir comment débuter sur le logiciel de Dokos.

Les premières utilisations du logiciel peuvent se montrer plus difficile, car vous allez devoir prendre en main l’outil et comprendre la structure du logiciel. Mais le fonctionnement reste le même sur Dokos et sur d'autres applications de gestion.

## 1.L'interface de Dokos

L'interface de Dokos est très simple : 

- 1 colonne sur la gauche pour avoir accès à tous les modules (Vente, Achat, CRM, Productione etc..)
- 1 page centrale : Montrant soit les tableaux de bord, soit la liste des documents, soit la page d'un document ou paramètres etc..

![l'interface_de_dokos.png](/use-cases/l'interface_de_dokos.png)

### 1.1 Personnaliser les tableaux de bord

En cliquant sur le Bouton **Personnaliser**, vous allez pouvoir modifier certains blocs du tableau de bord.

-	Ajoutez des graphiques
- Modifier les raccourcis pour avoir des accès directs à certains documents

### 1.2 Notification

Depuis l'icône "Cloche", vous pouvez voir toutes vos notifications.

### 1.3 Aide

Depuis la rubrique Aide, vous allez pouvoir avoir accès à de nombreux éléments :

- La documentation du logiciel
- Le forum communautaire
- Remonter un problème
- A propos : Information sur votre application Dokos

### 1.4 Accès au profil

Depuis votre profil, vous pouvez avoir accès à de nombreux élements :

- Mon profil
- Mes Paramètres
- Valeurs par défaut de la session
- Recharger
- Voir le site web
- Tâches de fond
- Déconnexion