---
title: Équipe de Maintenance des Actifs
description: 
published: true
date: 2021-06-24T09:50:50.868Z
tags: 
editor: markdown
dateCreated: 2021-06-24T09:50:28.002Z
---

# Équipe de Maintenance des Actifs

L'équipe de maintenance des actifs est responsable de l'exécution des activités de maintenance sur les actifs.

Les activités d'entretien peuvent être le nettoyage, le polissage, l'entretien ou toute autre activité requise pour maintenir l'actif en bon état.

---

Pour accéder à **la liste équipe de maintenance des actifs**, allez sur :

Accueil > Actifs > Maintenance > **Équipe de maintenance des actifs**

![liste_équipe.png](/asset/asset-maintenance-team/liste_équipe.png)

## 1. Prérequis avant utilisation

Avant de créer et d'utiliser Asset Maintenance Team, il est conseillé de créer d'abord les éléments suivants :

- **[Employé](/fr/human-resources/employee)**
- **[Actif](/fr/assets/asset)**

## 2. Comment créer une équipe de maintenance des actifs 

1. Accédez à **la liste Équipe de maintenance des actifs**, **:heavy_plus_sign: Ajouter Équipe de maintenance**.
2. Saisissez un **nom** pour l'équipe.
3. Sélectionnez **un responsable** pour l'équipe.
4. Dans le **tableau** Équipe de l'équipe de maintenance, ajoutez **les membres de l'équipe** et sélectionnez leurs **rôles** de maintenance .
5. **Enregistrer**.

![détails_équipe.png](/asset/asset-maintenance-team/détails_équipe.png)

## 3. Caractéristiques

### 3.1 Équipe

Dans cette section, indiquez les membres de l'équipe de maintenance. Ajoutez les différents champs qui sont :

- **Membre de l'équipe**
- **Nom complet**
- **Rôle de maintenance**

![liste_équipe.png](/asset/asset-maintenance-team/liste_équipe.png)