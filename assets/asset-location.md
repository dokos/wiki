---
title: Lieu
description: 
published: true
date: 2021-06-24T08:26:34.782Z
tags: 
editor: markdown
dateCreated: 2021-06-23T14:10:31.641Z
---

# Lieu

L'emplacement de l'actif indique où se trouve un actif.

Les actifs que votre organisation possède peuvent être situés dans diverses installations telles que des bureaux administratifs, des usines de production, des entrepôts, etc. Dans DOKOS, vous pouvez créer un **Emplacement** pour chacune de vos installations et suivre les actifs présents dans ces emplacements.

---

Pour accéder à **la liste Lieu**, allez sur :

> Accueil > Actifs > Actifs > **Lieu**

![liste_lieu.png](/asset/location/liste_lieu.png)

## 1. Comment créer un lieu ?

1. Allez sur la liste lieu, cliquez sur **:heavy_plus_sign: Ajouter Lieu**
2. Ajoutez un nom de lieu
3. Sélectionnez une l**ocalisation parente** si le nouveau lieu est un sous-lieu.
4. Cochez la case si le lieu **Est le contenant**. Il permet de vérifier s'il s'agit d'une unité hydroponique.
5. Cochez la case si le lieu **Est un groupe**.
6. Indiquez les **détails** du lieu comme la latitude et la longitude.
7. **Enregistrez**.

![détails_lieu.png](/asset/location/détails_lieu.png)

## 2. Caractéristiques

### 2.1 Détails de l'emplacement

Dans les détails de l'emplacement vous pouvez déterminer la latitude et la longitude du lieu;

![emplacement.png](/asset/location/emplacement.png)

> **Remarque** : Lorsqu'un actif est déplacé d'un emplacement à un autre, vous devez créer un enregistrement de mouvement d'actif.
{.is-warning}