---
title: Journal de Maintenance des Actifs
description: 
published: true
date: 2021-06-24T13:09:04.197Z
tags: 
editor: markdown
dateCreated: 2021-06-24T12:58:41.659Z
---

# Journal de Maintenance des Actifs

Le journal de maintenance des actifs enregistre les tâches effectuées dans une maintenance des actifs.

Pour chaque tâche dans Maintenance des actifs, le journal de maintenance des actifs est créé automatiquement pour suivre les maintenances à venir. Il aura un statut, une date d'achèvement et des actions effectuées. En fonction de la date d'achèvement ici, la prochaine date d'échéance est calculée automatiquement et un nouveau journal de maintenance des actifs est créé.

---

Pour accéder à **la liste journal de maintenance des actifs**, allez sur :

> Accueil > Actifs > Maintenance > **Journal de maintenance des actifs**

![journal_de_maintenance_des_actifs.png](/asset/asset-maintenance-log/journal_de_maintenance_des_actifs.png)

## 1. Prérequis avant utilisation

Avant de créer et d'utiliser le journal de maintenance des actifs, il est conseillé de créer d'abord les éléments suivants :

- **[Maintenance des actifs](/fr/assets/asset-maintenance)**

## 2. Comment créer un journal de maintenance des actifs

1. Allez sur la la liste Journal de maintenance des actifs, cliquez sur **:heavy_plus_sign: Ajouter Journal de maintenance des actifs**.
2. Sélectionnez **Maintenance des actifs**
3. Renseignez les **détails de la maintenance**, la tâche, le type d'entretien, le statut, la date d'achèvement.
4. Indiquez les **Actions réalisées**
5. **Enregistrer**

![détails_journal.png](/asset/asset-maintenance-log/détails_journal.png)

## 3. Caractéristiques

### 3.1 Détails de la maintenance

Dans cette section, indiquez toutes les informations de la maintenance.

- **Tâche** : Indiquez la nature de la tâche de maintenance.
- **Type d'entretien**
- A un certificat : Cochez la case si la maintenance à un certificat. Vous pouvez joindre le justificatif.
- **Statut d'entretien** : Prévu, Terminé, Annulé, En retard
- **Attribuer à**
- **Date d'achèvement**
- **Description**

![détails_de_la_maintenance.png](/asset/asset-maintenance-log/détails_de_la_maintenance.png)

## 2. Options dans le journal de maintenance des actifs

Un brouillon du journal de maintenance des actifs est créé comme prévu dans le formulaire Maintenance des actifs. Afin de soumettre un journal de maintenance des actifs, le statut de maintenance des actifs doit être **Terminé** ou **Annulé**.

Le statut du journal de maintenance des actifs peut être **Planifié**, **Terminé**, **Annulé** ou **En retard**.

Des notes supplémentaires peuvent être ajoutées dans la section Actions effectuées pour décrire l'activité en détail.