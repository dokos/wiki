---
title: Versions
description: 
published: true
date: 2022-09-13T13:39:29.260Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:11:24.808Z
---

# Notes de version

## Version 3
- [v3.0.0](/fr/versions/v3_0_0)
{.links-list}

## Version 2
- [V2.17.0](/fr/versions/v2_17_0)
- [V2.16.0](/fr/versions/v2_16_0)
- [V2.15.0](/fr/versions/v2_15_0)
- [V2.14.0](/fr/versions/v2_14_0)
- [V2.13.0](/fr/versions/v2_13_0)
- [V2.12.0](/fr/versions/v2_12_0)
- [V2.11.0](/fr/versions/v2_11_0)
- [V2.10.0](/fr/versions/v2_10_0)
- [V2.9.0](/fr/versions/v2_9_0)
- [V2.8.0](/fr/versions/v2_8_0)
- [V2.7.0](/fr/versions/v2_7_0)
- [V2.6.0](/fr/versions/v2_6_0)
- [V2.5.0](/fr/versions/v2_5_0)
- [V2.4.0](/fr/versions/v2_4_0)
- [V2.3.0](/fr/versions/v2_3_0)
- [V2.2.0](/fr/versions/v2_2_0)
- [V2.1.0](/fr/versions/v2_1_0)
- [v2.0.0](/fr/versions/v2_0_0)
{.links-list}

## Version 1

- [v1.4.0](/fr/versions/v1_4_0)
- [v1.3.0](/fr/versions/v1_3_0)
- [v1.2.0](/fr/versions/v1_2_0)
- [v1.1.0](/fr/versions/v1_1_0)
- [v1.0.0](/fr/versions/v1_0_0)
{.links-list}
