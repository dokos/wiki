---
title: Ressources Humaines
description: 
published: true
date: 2021-08-27T11:38:04.446Z
tags: 
editor: markdown
dateCreated: 2020-11-27T07:14:12.770Z
---

# Ressources Humaines
--- 

Pour accéder au module de **Ressources Humaines**, allez sur :

> Accueil > **Production**

## 1. Employé

- [1. Employé](/fr/human-resources/employee)
- [2. Type d'emploi](/fr/human-resources/employment-type)
- [3. Branche](/fr/human-resources/branch)
- [4. Département](/fr/human-resources/department)
- [5. Désignation](/fr/human-resources/designation)
- [6. Échelon de l'employé](/fr/human-resources/employee-grade)
- [7. Groupe d'employé](/fr/human-resources/employee-group)
- [8. Assurance maladie des employés](/fr/human-resources/employee-health-insurance)
{.links-list}

## 2. Cycle de vie des employés

- [1. Carte de compétence des employés](/fr/human-resources/employee-skill-map)
- [2. Transfert des employés](/fr/human-resources/employee-transfer)
- [3. Promotion des employés](/fr/human-resources/employee-promotion)
- [4. Départ des employés](/fr/human-resources/employee-separation)
{.links-list}

## 3. Gestion des quarts

- [1. Type de quart](/fr/human-resources/shift-type)
- [2. Sélection de quart](/fr/human-resources/shift-request)
- [3. Affectation de quart](/fr/human-resources/shift-assignment)
{.links-list}

## 4. Congés

- [Premiers pas avec les congés](/fr/human-resources/leave-setup)
- [1. Liste de congés](/fr/human-resources/holiday-list)
- [2. Type de congés](/fr/human-resources/leave-type)
- [3. Période de congés](/fr/human-resources/leave-period)
- [4. Politique de congés](/fr/human-resources/leave-policy)
- [5. Attribution de politique de congés](/fr/human-resources/leave-policy-assignment)
- [6. Demande de congés](/fr/human-resources/leave-application)
- [7. Allocation de congés](/fr/human-resources/leave-allocation)
- [8. Congés accumulés encaissés](/fr/human-resources/leave-encashment)
- [9. Liste de blocage des congés](/fr/human-resources/leave-block-list)
- [10. Demande de congé compensatoire](/fr/human-resources/compensatory-leave-request)
{.links-list}


## 5. Présences

- [1. Outil de gestion de présences](/fr/human-resources/employee-attendance-tool)
- [2. Présences](/fr/human-resources/attendance)
- [3. Demande de validation de présence](/fr/human-resources/attendance-request)
- [4. Outil de chargement de présences](/fr/human-resources/upload-attendance)
{.links-list}

## 6. Notes de frais

- [Notes de frais](/fr/human-resources/expense-claim)
- [Demande de déplacement](/fr/human-resources/travel-request)
- [Avance versées aux employés](/fr/human-resources/employee-advance)
{.links-list}

## 7. Recrutement

- [1. Offre d'emploi](/fr/human-resources/job-opening)
- [2. Proposition de poste](/fr/human-resources/job-offer)
- [3. Demandeur d'emploi](/fr/human-resources/job-applicant)
- [4. Promesse d'embauche](/fr/human-resources/appointment-letter)
- [5. Modèle de promesse d'embauche](/fr/human-resources/appointment-letter-template)
{.links-list}

## 8. Formation

- [1. Programme de formation](/fr/human-resources/training-program)
- [2. Événement de formation](/fr/human-resources/training-event)
- [3. Résultat de formation](/fr/human-resources/training-result)
{.links-list}

## 9. Gestion de la flotte

- [1. Chauffeur](/fr/human-resources/driver)
- [2. Véhicule](/fr/human-resources/vehicle)
- [3. Journal du véhicule](/fr/human-resources/vehicle-log)
{.links-list}

## 10. Paramètres RH

- [1. Paramètres des RH](/fr/human-resources/hr-settings)
{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
