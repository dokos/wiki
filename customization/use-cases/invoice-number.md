---
title: Changer la modification des factures
description: 
published: true
date: 2022-03-07T13:23:49.094Z
tags: 
editor: markdown
dateCreated: 2022-03-07T11:13:11.357Z
---

# Changer la modification des numéros de factures

Dans Dokos, vous pouvez personnaliser la numérotation de tous vos documents. Nous allons voir dans ce cas comment modifier la numérotation de vos factures de vente et reprendre votre série en cours. 

---

Pour pouvoir récupérer votre numérotation de facture, il vous suffit d’aller sur le type de document **“Nom de série”** (accessible depuis la barre de recherche).

1. Sélectionnez la transaction où vous souhaitez modifier le préfixe (pour votre cas Facture de vente)
2. Modifiez la liste pour la transaction (Un préfixe de série par ligne, vous pouvez indiquer plusieurs préfixe si besoins que vous pourrez sélectionner sur votre facture)

Par exemple pour avoir un nom de facture de type **FA-2022-03-03-001**, il faut mettre :

`FA-.YYYY.-.MM.-.DD.-.###`

3. Mettez à jour la liste en cliquant sur Mettre à jour.

Les **#** représentent le nombre de chiffre dans la série. Si vous mettez **###**, cela veut dire qu’il y aura 3 chiffres après le préfixe.

Et enfin pour repartir du dernier numéro de votre série de facture :

4. Il faut aller dans :

	- La section **Mettre à jour**, puis dans le champ **Préfixe**. (Ici récupérez le préfixe que vous venez de créer dans la liste déroulante. Pensez bien à mettre à jour la liste avant de rechercher le préfixe.)

	- Ensuite, allez dans Valeur actuelle et indiquez le dernier numéro de série.

Si la dernière facture était… -78. Il faut alors mettre 78 dans cette case au lieu de 0.

5. Mettez jour le le préfixe puis faites enregistrer.
Le prochain n° de facture dans notre cas sera donc FA-2022-03-03-079

Bonne journée,