---
title: Impression
description: 
published: true
date: 2021-05-12T08:40:12.296Z
tags: 
editor: markdown
dateCreated: 2021-05-12T08:40:12.296Z
---

# Impression

Les documents que vous envoyez à vos clients portent votre marque et votre image et doivent être adaptés à vos besoins. DOKOS vous offre de nombreuses options pour que vous puissiez définir la marque de votre oraganisation dans vos documents.

DOKOS vous offre également la possibilité d'imprimer des étiquettes de codes-barres, des reçus de point de vente et d'envoyer d'autres commandes brutes aux imprimantes à l'aide de la fonction d'impression brute.



