---
title: Rôle et profil de rôle
description: 
published: true
date: 2021-06-12T07:59:39.450Z
tags: 
editor: markdown
dateCreated: 2021-06-12T07:59:39.450Z
---

# Rôle et profil de rôle
Un **rôle** définit les **autorisations** d'accès à **divers documents** dans DOKOS.

Les **rôles définissent un ensemble d'autorisations** qui peuvent être définies à **partir du gestionnaire d'autorisations de rôles** . Les rôles les plus couramment utilisés sont déjà définis dans DOKOS, vous pouvez utiliser le système avec eux. Si nécessaire, vous pouvez ajouter d'autres rôles. 
Par exemple, si vous attribuez le rôle Utilisateur commercial à un utilisateur, celui-ci pourra accéder à des documents tels que Devis et Commandes client, car les autorisations sont déjà définies pour le rôle Utilisateur commercial.

Les profils de rôle stockent différents rôles afin que plusieurs rôles puissent être attribués à la fois.

Les **profils de rôle servent de modèle** pour stocker et sélectionner plusieurs rôles. Ce profil de rôle peut ensuite être attribué à un utilisateur. 

Par exemple, un superviseur des ventes aura les rôles Employé, Directeur des ventes, Utilisateur des ventes et Directeur principal des ventes. Les profils de rôle sont utiles pour attribuer plusieurs rôles à la fois lors de l'ajout de plusieurs employés.

---

Pour accéder au **rôle**, allez sur :

> Accueil > Utilisateurs et autorisations > **Rôle**

![ajouter_un_rôle.png](/setup/role-ande-role-profile/ajouter_un_rôle.png)

## 1. Comment ajouter un rôle ?

### 1.1 Ajout rapide
1. Allez dans la liste des rôles, cliquez sur **:heavy_plus_sign: Ajouter Rôle.**
2. Entrez un **nom** pour le rôle.

![ajout_rapide.png](/setup/role-ande-role-profile/ajout_rapide.png)

### 1.2 Ajout complet

1. Modifier en pleine page
2. **Nom** du rôle
3. Chemin de la **page d'Accueil**
4. **Désactivé** le rôle, si la case est cochée le rôle sera désactivé.
5. **Choisissez si le rôle dispose d'un accès au bureau**. Un rôle disposant d'un accès au bureau peut accéder aux modules DOKOS et aux documents de l'entreprise. Le niveau d'accès dépend des rôles attribués à l'utilisateur.
6. **Authentification à double facteur** : Vous pouvez ajouter une authentification à deux facteurs pour le rôle et la restreindre à un domaine spécifique.

4. **Enregistrer**.

![création_d'un_rôle.png](/setup/role-ande-role-profile/création_d'un_rôle.png)

## 2. Caractéristiques

### 2.1 Paramètres de navigation
Cochez les cases pour laisser les actions actives au nouveau rôle.

- Barre de recherche
- Notifications
- Chat

### 2.2 Paramètre de la liste
Cochez les cases pour laisser les actions actives au nouveau rôle.

- Barre latérale
- Actions en masse
- Voir la sélection de vues

### 2.3 Paramètre du formulaire
Cochez les cases pour laisser les actions actives au nouveau rôle.

- Barre latérale
- Chronologie
- Tableau de bord
- Personnalisé

![caractéristiques_rôles.png](/setup/role-ande-role-profile/caractéristiques_rôles.png)

### 2.4 Gestionnaire des rôles

À partir de là, vous pouvez accéder au gestionnaire d'autorisations de rôles et définir des autorisations pour le rôle sur différents types de document.

![gestionnaire_des_rôles.png](/setup/role-ande-role-profile/gestionnaire_des_rôles.png)


## 2. Comment ajouter un profil de rôle ?

Pour accéder au profil de rôle, allez sur :

> Accueil > Utilisateurs et autorisations > Autorisations > **Profil de rôle**

![profil_de_rôle.png](/setup/role-ande-role-profile/profil_de_rôle.png)

1. Allez dans la **liste Profil de rôle**, cliquez sur **:heavy_plus_sign: Ajouter Profil de rôle**.
2. Entrez un **nom**.
3. Sélectionnez les rôles que vous souhaitez attribuer à ce profil.
4. **Enregistrer**.

![type_de_profil.png](/setup/role-ande-role-profile/type_de_profil.png)