---
title: Signature électronique
description: 
published: true
date: 2022-10-25T07:39:29.099Z
tags: 
editor: markdown
dateCreated: 2022-10-25T07:39:29.099Z
---

# eSignature


L'application eSignature propose des intégrations pour faciliter la signature électronique depuis Dodock/Dokos.

Elle est compatible avec les versions suivantes:

|Framework|Version|
|---------|-------|
|Dodock|Version 3|
|Frappe|Version 14|

## Installation

Pour installer l'application eSignature sur votre site Dokos, placez vous dans votre `bench` et lancez les commandes suivantes:

```
bench get-app git@gitlab.com:dokos/esignature.git
bench --site {votre site} install-app esignature
```

Connectez-vous ou rechargez votre site pour découvrir un nouvel espace de travail *eSignature*


## Intégrations

Liste des intégrations disponibles

- [Adobe Sign](/fr/esignature/adobe-sign)
{.links-list}