---
title: Modèle de retours de qualité
description: 
published: true
date: 2021-07-01T09:21:37.896Z
tags: 
editor: markdown
dateCreated: 2021-07-01T08:46:30.502Z
---

# Modèle de retours de qualité

Pour créer le retour qualité, vous devez d'abord définir les paramètres en créant le modèle de feedback qualité. Les modèles contiennent généralement le nom du modèle, la portée et les différents paramètres à saisir. Les différents paramètres peuvent être décidés sur la base de différentes occasions et enquêtes.

---

Pour accéder à **la liste modèle de retours sur la qualité**, allez sur :

> Accueil > Qualité > **Modèle de retours sur la qualité**

![liste_modèle_retour_de_qualité.png](/quality/quality-feedback-template/liste_modèle_retour_de_qualité.png)

## 1. Comment créer un modèle de commentaires sur la qualité

1. Accédez à **la liste des modèles de commentaires sur la qualité**, cliquez sur **:heavy_plus_sign: Ajouter un modèle de commentaire sur la qualité**.
2. Saisissez un **nom** pour le modèle.
3. Dans le **tableau Paramètres**, répertoriez les différents processus/paramètres sur lesquels vous souhaitez que l'utilisateur évalue et donne son avis.
4. **Enregistrer**.

![détails_modèle_de_retour.png](/quality/quality-feedback-template/détails_modèle_de_retour.png)

