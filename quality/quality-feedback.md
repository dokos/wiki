---
title: Retours de qualité
description: 
published: true
date: 2021-07-01T08:41:31.893Z
tags: 
editor: markdown
dateCreated: 2021-07-01T08:30:14.742Z
---

# Retours de qualité

Les retours sur la qualité sont les informations qu'un client exprime sur la satisfaction ou l'insatisfaction concernant les produits ou services que vous proposez.

Les retours de qualité a différents champs pour évaluer différents paramètres. Le client/utilisateur peut facilement donner son avis en sélectionnant son modèle pour évaluer différents paramètres et également fournir un retour qualitatif sur ces paramètres.

Vous pouvez également récupérer les paramètres à l'aide d'un modèle de commentaires sur la qualité. Les modèles stockent des informations prédéfinies que vous pouvez sélectionner dans les commentaires sur la qualité.

Si le retour de qualité n'est pas conforme aux attentes, une Action Qualité pourra être initialisée sur la base de la gravité du retour de qualité.

---

Pour accéder à **la liste retour de qualité**, allez sur :

> Accueil > Qualité > Commentaires > **Retour de la qualité**

![liste_retour_de_qualité.png](/quality/quality-feedback/liste_retour_de_qualité.png)

## 1. Comment créer un retour de qualité

1. Accédez à **la liste des retours sur la qualité**, cliquez sur **:heavy_plus_sign: Ajouter un retour de qualité**.
2. Sélectionnez si le retour de qualité est par un client ou un utilisateur (utilisateur du compte DOKOS).
3. Sélectionnez le **client** ou l'**utilisateur** en particulier.
4. La sélection d'un modèle de retour d'information sur la qualité remplira automatiquement les paramètres du retour d'information sur la qualité.
5. La **date** sera automatiquement réglée sur **la date du jour**.
6. Le tableau **Paramètres** comporte les colonnes suivantes :
	- **Paramètre** : divers paramètres prédéfinis dans le modèle de commentaires sur la qualité que vous créez.
	- **Note** : Note pour un paramètre de 1 à 5.
	- **Rétroaction** : rétroaction basée sur l'évaluation donnée.
7. **Enregistrer**.

![détails_retour_de_qualité.png](/quality/quality-feedback/détails_retour_de_qualité.png)