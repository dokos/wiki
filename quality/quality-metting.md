---
title: Réunion qualité
description: 
published: true
date: 2021-07-01T13:02:02.714Z
tags: 
editor: markdown
dateCreated: 2021-07-01T12:15:39.153Z
---

# Réunion de qualité

C'est une assemblée de personnes pour un objectif ou un ordre du jour particulier, en particulier pour une discussion formelle sur les aspects de la qualité.

Des réunions qualité sont organisées pour passer en revue les revues de qualité qui ont été générées. Le procès-verbal (résumé) de ces réunions inclurait la discussion sur les différentes revues de la qualité.

---

Pour accéder à **la liste Réunion de qualité**, allez sur :

> Accueil > Qualité > Réunion > **Réunion de Qualité**

![liste_réunion_de_qualité.png](/quality/quality-meeting/liste_réunion_de_qualité.png)

## 1. Comment créer une réunion de qualité

1. Allez dans la liste Réunion Qualité, cliquez sur Nouveau.
2. Entrez un agenda. Il s'agit d'une liste des activités de la réunion dans l'ordre dans lequel elles doivent être abordées.
3. Dans le tableau du compte-rendu, saisissez :
	- **Type de document** : Sélectionnez parmi Examen qualité, Action qualité ou Commentaires sur la qualité.
	- **Nom du document** : sélectionnez l'examen de la qualité, l'action et le retour d'information à discuter.
 - **Minute** : documentation écrite ou enregistrée associée à l'examen.
5. **Enregistrer**.

![détails_réunion.png](/quality/quality-meeting/détails_réunion.png)

> **Remarque** : Le statut de la réunion peut être défini sur Fermé une fois l'ordre du jour terminé.
{.is-warning}


