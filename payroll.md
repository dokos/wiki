---
title: Paie
description: 
published: true
date: 2021-06-21T13:45:09.333Z
tags: 
editor: markdown
dateCreated: 2021-06-17T07:45:51.770Z
---

# Paie

Le traitement de la paie est une fonction importante de chaque RH d'entreprise. DOKOS simplifie considérablement ce processus en offrant un éventail de fonctionnalités que vous pouvez utiliser, de la gestion de la structure salariale au traitement en bloc de la paie des employés. Lisez la documentation suivante pour comprendre comment configurer et utiliser le module de Paie pour optimiser votre traitement de la paie.

---

## 1. Paie

- [1. Composante salariale](/fr/payroll/salary-component)
- [2. Grille des salaires](/fr/payroll/salary-structure)
- [3. Entrée de la paie](/fr/payroll/payroll-entry)
- [4. Attribution de la structure salariale](/fr/payroll/salary-structure-assignment)
- [5. Fiche de paie](/fr/payroll/salary-slip)
{.links-list}

## 2. Taxation

- [1. Période de paie](/fr/payroll/payroll-period)
- [2. Tranche d'impôt sur le revenu](/fr/payroll/income-tax-slab)
- [3. Autres revenus de l'employé](/fr/payroll/employee-other-income)
- [4. Déclaration d'exemption de taxe](/fr/payroll/employee-tax-exemption-declaration)
- [5. Soumission d'une preuve d'exemption de taxe](/fr/payroll/employee-tax-exemption-proof-submission)
- [6. Catégorie d'exemption de taxe](/fr/payroll/employee-tax-exemption-category)
- [7. Sous-catégorie d'exemption de taxe](/fr/payroll/employee-tax-exemption-sub-category)
{.links-list}


## 3. Indemnités

- [1. Salaire supplémentaire](/fr/payroll/additional-salary)
- [2. Prime de fidélisation](/fr/payroll/retention-bonus)
- [3. Intéressement des employés](/fr/payroll/employee-incentive)
- [4. Demande d'avantages sociaux](/fr/payroll/employee-benefit-application)
- [5. Requête d'avantages sociaux](/fr/payroll/employee-benefit-claim)
{.links-list}


## 4. Paramètres

- [1. Paramètres de Paie](/fr/payroll/payroll-settings)
{.links-list}


