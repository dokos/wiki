---
title: Coût de l'activité
description: 
published: true
date: 2021-07-16T11:32:31.106Z
tags: 
editor: markdown
dateCreated: 2021-07-16T10:51:32.181Z
---

# Coût de l'activité

Le coût d'activité enregistre le prix de facturation à l'heure et le coût de revient d'un employé par rapport à un type d'activité particulier.

Le système tire ce taux lors de la création de feuilles de temps. Il est utilisé pour déterminer le coût du projet.

---

Pour accéder à **la liste de coût de l'activité**, allez sur,

> Accueil > Projets > Suivi du temps > **Coût de l'activité**

![liste_coût_d'activité.png](/projects/activity-cost/liste_coût_d'activité.png)

## 1. Comment créer un coût d'activité 

1. Accédez à la liste des coûts d'activité et cliquez sur **:heavy_plus_sign: Ajouter coût d'activité**.
2. Ajoutez le **nom de l'employé** pour lequel vous configurez le coût de l'activité.
3. Ajoutez le **coût de revient** et le **prix de facturation** pour l'employé.
4. **Enregistrer**.

![coût_d'activité.png](/projects/activity-cost/coût_d'activité.png)

Alternativement, un coût d'activité peut également être créé via la liste d'activités.