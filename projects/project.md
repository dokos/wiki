---
title: Projet
description: 
published: true
date: 2021-07-13T12:53:12.320Z
tags: 
editor: markdown
dateCreated: 2021-07-13T08:50:58.068Z
---

# Projet

Un projet est un travail planifié qui est conçu pour trouver des informations sur quelque chose, pour produire quelque chose de nouveau ou pour améliorer quelque chose.

Dans DOKOS, la gestion de projet est axée sur les tâches. Vous pouvez créer un projet et le diviser en plusieurs tâches.

Un projet a une large portée et peut donc être divisé en tâches. Par exemple, de proposer un nouveau smartphone pour l'année prochaine en tant que projet. 
Ensuite, des choses comme la conception, le prototypage, les tests, la livraison, etc. deviennent des tâches dans le cadre du projet.

Bien que chaque tâche d'un projet puisse être attribuée à un individu ou à un groupe d'individus, l'attribution peut également être effectuée au niveau du projet.

Ces tâches peuvent être créées à partir d'un projet lui-même ou une tâche peut également être créée séparément.

---

Pour accéder à **la liste de Projet**, allez sur :

> Accueil > Projets > Projets > **Projet**

![liste_projet.png](/projects/project/liste_projet.png)

## 1. Comment créer un projet ?

1. Allez dans la liste Projet et cliquez sur **:heavy_plus_sign: Ajouter Projet**.
2. Ajoutez les **détails** suivants :
	-	**Nom du projet** : Titre du projet.
	- **Statut** : Le statut par défaut d'un projet sera « Ouvert » qui pourra ensuite être modifié en **Terminé** ou **Annulé**.
	- **Date de fin prévue** : Saisissez la date à laquelle vous envisagez de terminer le projet.
3. **Enregistrer**.

![détails_projet.png](/projects/project/détails_projet.png)

### 1.1 Options supplémentaires lors de la création d'un projet 

1. **À partir d'un modèle** : Si vous disposez d'un modèle de projet existant , vous pouvez choisir de créer votre projet à l'aide de ce modèle.
2. **Date de début prévue** : si vous avez un calendrier fixe pour le projet, vous pouvez définir à la fois la date de début prévue et la date de fin prévue dans le formulaire.
3. **Type de projet** : Vous pouvez classer vos projets en différents types , par exemple, internes ou externes.
4. **Priorité** : Vous pouvez sélectionner le niveau de priorité du projet en fonction de son importance. Vous pouvez également ajouter plus de niveaux de priorité.
5. **Département** : si le projet appartient ou appartient à un département de l'organisation, vous pouvez l'ajouter ici.
6. **Est actif** : Un onglet Oui/Non, qui vous permet de modifier le statut actif du projet à n'importe quelle étape ultérieure.
7. **Méthode d'achèvement** : Vous pouvez suivre le % d'achèvement de votre projet en fonction de l'une des trois méthodes, à savoir. Manuel, achèvement de la tâche, progression de la tâche et poids de la tâche .

> **Remarque** : Si le poids total des tâches n'est pas de 100, le résultat calculé sera divisé par le poids total. Par exemple, si le total des pondérations des tâches est de 70, le pourcentage d'achèvement = (70/0,8) % = 87,5 %.
{.is-warning}

## 2. Caractéristiques

### 2.1. Détails du client, utilisateurs et notes

- **Client** : Si un Projet est réalisé pour un Client particulier, les détails peuvent être renseignés ici.
- **Commande client** : Si un projet est basé sur une commande client d'un client, vous pouvez récupérer les détails ici. Cela vous permettrait d'informer le client de l'avancement du projet conformément au bon de commande émis.

![détails_client.png](/projects/project/détails_client.png)


- **Utilisateurs** : Vous pouvez ajouter n'importe quel utilisateur du site Web pour lui donner accès à ce projet. Par exemple, vous pouvez ajouter votre client en tant qu'utilisateur du site Web, pour lui permettre d'avoir accès à votre projet pour suivre l'avancement et/ou donner des entrées/remarques. De même, un fournisseur ou un employé contractuel/indépendant impliqué dans le projet peut être ajouté en tant qu'utilisateur.

![utilisateurs.png](/projects/project/utilisateurs.png)

En outre, vous pouvez également développer la fenêtre et sélectionner si vous souhaitez envoyer un e-mail de bienvenue à un utilisateur en particulier ou lui donner les droits de visualisation des pièces jointes.

Vous pouvez en savoir plus sur l'autorisation des utilisateurs à afficher les projets ici .

- **Notes** : Vous pouvez ajouter des notes supplémentaires au projet.

![notes.png](/projects/project/notes.png)

### 2.2. Dates de début et de fin

- Date de début réelle : sur la base du début réel du projet, suivi via des feuilles de temps, la date et l'heure de début réelles du projet seront enregistrées automatiquement.
- Date de fin réelle : sur la base de la fin réelle du projet, suivie via la dernière mise à jour de la feuille de temps, la date et l'heure de fin réelles du projet seront enregistrées automatiquement. Pour en savoir plus sur les feuilles de temps, cliquez ici .

### 2.3. Coût et facturation

- **Coût estimé** : Entrez le coût estimé du projet.
- **Montant total des ventes** : Si vous avez déjà lié le projet à une commande client, le montant total de la commande client sera automatiquement renseigné ici.
- **Montant total du coût** : Le système récupérera automatiquement le montant total du coût de toutes les feuilles de temps liées à ce projet.
- **Montant total facturable** : Le système récupérera automatiquement le montant total facturable de toutes les feuilles de temps liées à ce projet.
- **Demande de remboursement totale** : sur la base des dépenses réclamées par un employé pour l'achèvement du projet, la demande de remboursement totale sera calculée automatiquement.
- **Montant total facturé** : le montant total facturé est automatiquement renseigné dans le système à l'aide de la facture de vente créée par rapport à la commande client.
- **Coût d'achat total** : le coût d'achat total d'un projet est le coût extrait des factures d'achat créées par rapport à un bon de commande émis pour la fourniture de matériaux requis pour un projet.
- **Coût total des matériaux consommés** : à l'aide de l'entrée de stock effectuée conformément aux exigences des matériaux dans le projet, le coût total des matériaux consommés est capturé.

![coût_et_facturation.png](/projects/project/coût_et_facturation.png)

### 2.4. Marge

- **Marge brute** : La marge brute vous donnerait la marge que vous avez entre votre montant total des coûts et le montant total facturé.

- **Marge brute** = (Montant total des ventes + Montant total facturable) - Montant total du coût + Montant total facturable + Total des notes de frais + Coût total d'achat + Coût total des matériaux consommés)

- **% brut** : le pourcentage du montant total facturé dépensé dans le montant total des coûts constitue le pourcentage brut.

((Montant total des ventes + Montant total facturable) - Montant total du coût + Montant total facturable + Total des notes de frais + Coût total d'achat + Coût total des matériaux consommés) / Montant total des ventes)* 100

### 2.5. Surveiller les progrès

Lorsque vous activez l'option **Envoyer des emails de suivi d'avancement** en cochant la case, cela vous permet d'ajouter des détails de surveillance au projet. Un rapport sur l'avancement du projet sera envoyé à toutes les parties prenantes du projet.

- **Liste des jours fériés** : Vous pouvez sélectionner la liste des jours fériés pour votre entreprise. Cela vous permettra de collecter les rapports d'avancement uniquement les jours ouvrables.
- **Fréquence** : Vous pouvez définir la fréquence à laquelle vous souhaitez obtenir les rapports. Il peut être réglé sur une fréquence horaire, biquotidienne, quotidienne ou hebdomadaire.

![suivi.png](/projects/project/suivi.png)