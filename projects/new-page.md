---
title: Démarrer avec Dokos
description: 
published: true
date: 2022-10-27T07:40:23.623Z
tags: 
editor: markdown
dateCreated: 2022-10-27T07:40:23.623Z
---

# Installation

- [Installation Rapide](/fr/getting-started/easy-installation)
- [Migration ERPNext](/fr/getting-started/erpnext-migration)
- [DNS Multi-tenant](/fr/getting-started/domain-setup)
- [Certificats SSL avec Let's Encrypt](/fr/getting-started/ssl-certificate)
{.links-list}

# Mise en place

- [Configuration de la société](/fr/accounting/company)
- [Configurer les taxes](/fr/settings/account)
- [Valeurs par défaut global](/fr/settings/global-defaults)
{.links-list}

# Maintenance

- [Mettre à jour Dokos](/fr/getting-started/updating-dokos)
{.links-list}

# Configuration

- [Formats d'impression](/fr/settings/print/print-format)
- [Paramètrage des emails](/fr/getting-started/email-setup)
- [Modèles d'adresses](/fr/getting-started/address-templates)
{.links-list}