---
title: Mise à jour du projet
description: 
published: true
date: 2022-03-22T11:02:16.020Z
tags: 
editor: markdown
dateCreated: 2022-03-22T11:01:34.946Z
---

# Mise à jour du projet

Une mise à jour du projet est le statut du projet qui peut être envoyé à toutes les parties prenantes du projet.

Chaque fois que vous souhaitez informer les parties prenantes du projet, vous pouvez leur envoyer une mise à jour du projet.

Pour accéder à la mise à jour du projet, allez sur :

> Accueil > Projets > Projets > Mise à jour du projet

## 1. Comment créer une mise à jour de projet

1. Accédez à la liste de mise à jour du projet et cliquez sur Nouveau.
2. Ajoutez le nom du projet pour lequel vous souhaitez envoyer une mise à jour.
3. Ajoutez le nom des utilisateurs auxquels vous souhaitez envoyer la mise à jour du projet dans la table enfant. Ces Utilisateurs peuvent être vos Clients ou toute autre Partie Interne Interne ou Externe.
4. Enregistrer et envoyer.

Une fois que vous avez enregistré la mise à jour du projet, un e-mail est envoyé aux parties prenantes sélectionnées avec les mises à jour du projet.

### 1.1. Détails supplémentaires 

Lorsque vous développez le tableau qui contient les noms des Utilisateurs, vous pourrez ajouter les détails suivants :

- Vous pouvez choisir d'envoyer un e-mail de bienvenue à un utilisateur particulier lors de l'envoi de la mise à jour du projet.
- Vous pouvez choisir si vous souhaitez qu'un utilisateur particulier affiche les pièces jointes dans la mise à jour du projet.
- Vous pouvez ajouter la description de l'étape actuelle du projet dans le champ « Statut du projet ».