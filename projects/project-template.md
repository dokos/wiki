---
title: Modèle de projet
description: 
published: true
date: 2021-07-15T14:47:33.088Z
tags: 
editor: markdown
dateCreated: 2021-07-15T11:48:09.696Z
---

# Modèle de projet

Un modèle de projet est une séquence prédéfinie de tâches organisées selon un calendrier stipulé.

Ces modèles peuvent être extraits pour des types de projets similaires et les tâches qu'ils contiennent sont automatiquement remplies au moment de la création de chaque nouveau projet.

---

Pour accéder à **la liste modèle de projet**, allez sur :

> Accueil > Projets > Projets > **Modèle de projet**

![modèle_de_projet.png](/projects/project-template/modèle_de_projet.png)

## 1. Comment créer un nouveau modèle de projet ?

1. Accédez à la liste des modèles de projet et cliquez sur **Ajouter Modèle de projet**.
2. Ajoutez les **détails** suivants :
	- **Nom du modèle de projet** : Titre du modèle de projet
	- **Type de projet** : Les modèles de projet, tout comme les projets, peuvent être classés en différents types de projets, par exemple, interne ou externe.
	- **Tâches** : Chaque modèle de projet aura un ensemble d'une séquence prédéfinie de tâches. Dans ce tableau, vous pouvez sélectionner les tâches que vous souhaitez pour ce modèle.
  
![détails_modèle_de_projet.png](/projects/project-template/détails_modèle_de_projet.png)
