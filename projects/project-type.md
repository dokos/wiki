---
title: Type de projet
description: 
published: true
date: 2021-07-15T14:46:45.195Z
tags: 
editor: markdown
dateCreated: 2021-07-15T14:46:45.195Z
---

# Type de projet

Un type de projet est la classification des projets en différents types pour regrouper des types de projets similaires.

Les projets internes, les projets externes peuvent être des exemples de types de projets et sont déjà créés dans le système. Vous pouvez choisir d'ajouter d'autres types de projets. Cela s'avère pratique lorsque vous examinez les projets et que vous souhaitez filtrer les informations en fonction des types de projets.

---

Pour accéder à **la liste type de projet**, allez sur :

> Accueil > Projets > Projets > **Type de projet**

![liste_type_de_projet.png](/projects/project-type/liste_type_de_projet.png)

## 1. Comment créer un type de projet 

1. Accédez à la liste des types de projets et cliquez sur **Ajouter Type de projet**.
2. Ajoutez le **nom** du type de projet et la **description**
3. **Enregistrer**.

![détails_type_de_projet.png](/projects/project-type/détails_type_de_projet.png)
