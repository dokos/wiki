---
title: Type d'activité
description: 
published: true
date: 2021-07-16T10:38:15.512Z
tags: 
editor: markdown
dateCreated: 2021-07-16T10:38:15.512Z
---

# Type d'activité

Le type d'activité est une liste de différents types d'activités par rapport auxquelles une feuille de temps peut être établie.

---

Pour accéder à **la liste type d'activité**, allez sur,

> Accueil > Projets > Suivi du temps > **Type d'activité**

![liste_type_d'activité.png](/projects/activity-type/liste_type_d'activité.png)

## 1. Comment créer un type d'activité ?

1. Accédez à la liste des types d'activité et cliquez sur **:heavy_plus_sign: Ajouter Type d'activité**.
2. Ajoutez le **sujet** du type d'activité.
3. **Enregistrer**.

![type_d'activité.png](/projects/activity-type/type_d'activité.png)

De plus, vous pouvez également ajouter le **coût de revient** par défaut et le **prix de facturation** par défaut pour chaque activité.

En outre, le taux de coût par défaut et le taux de facturation par défaut pour les employés individuels peuvent également être configurés. Cela peut également être configuré à l'aide du coût d'activité .

Par défaut, les types d'activité suivants sont créés dans Dokos.

- **Planification**
- **Recherche**
- **Rédaction de proposition**
- **Exécution**
- **Communication**