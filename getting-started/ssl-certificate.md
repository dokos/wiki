---
title: Certificat SSL
description: 
published: true
date: 2022-10-25T14:38:36.014Z
tags: 
editor: markdown
dateCreated: 2020-11-26T14:46:18.697Z
---

# Let's Encrypt

Vous pouvez amener votre propre certificat pour sécuriser votre installation _Dokos_ ou générer un nouveau certificate en utilisant Let's Encrypt.

## Pré-requis

1. Votre bench doit être en mode DNS Multitenant
2. Votre domaine web doit rediriger vers le serveur de votre site
3. Vous devez pouvoir exécuter les commandes avec des droits administrateur (root)

## Générer un nouveau certificat

`sudo -H bench setup lets-encrypt {votre site}`

## Générer un nouveau certificat pour un domain personnalisé

`sudo -H bench setup lets-encrypt {votre site} --custom-domain {votre domaine personnalisé}`

## Renouveler un certificat

Chaque certificat Let's Encrypt est valide pendant 3 mois. Vous pouvez les renouveler 30 jours avant qu'ils expirent.
La génération d'un nouveau domaine crée automatiquement une tâche cron qui tente de renouveler le certificat automatiquement.
Si ce renouvelement automatique ne fonctionne pas, vous pouvez quand même renouveler votre certificat manuellement en lançant:

`sudo bench renew-lets-encrypt`