---
title: Installation Docker
description: 
published: false
date: 2022-10-04T08:40:17.947Z
tags: 
editor: markdown
dateCreated: 2020-11-26T14:27:51.833Z
---

# Installation Docker


> Ce type d'installation est encore expérimental.
{.is-warning}

Cette méthode d'installation est actuellement uniquement disponible sur la branche de développement. La branche master (production) sera bientôt disponible également.  

Cette page explique comment installer Dokos à l'aide de Docker. Seules les distributions Linux sont prises en charge entièrement par ce guide d'installation, même s'il est possible d'installer Dokos via Docker sur Windows ou macOS. Pour toute demande de support, rendez-vous sur [le forum de la communauté Dokos](https://community.dokos.io).

## Pré-requis

Afin d'installer Dokos sur Docker, vous devez installer les composants suivants sur votre serveur :
- Git
- Python 3
- Docker
- Docker-compose

### Installer Git et Python 3
Concernant Git et Python 3, ces deux composants sont pré-installés sur la plupart des distributions Linux.

### Installer Docker

**Un script d'installation simplifié de Docker est disponible dans le répertoire GitHub [docker/docker-install](https://github.com/docker/docker-install).** Vous trouverez plus d'informations sur l'installation de Docker sur [le site web de Docker](https://docs.docker.com/engine/install/).

<!-- ```sh
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
``` -->

### Installer docker-compose
Pour docker-compose, **l'installation est détaillée dans la section correspondante sur [le site web de Docker](https://docs.docker.com/compose/install/#install-compose-on-linux-systems).**

N'oubliez pas d'ajouter votre utilisateur au groupe `docker` ([voir dans la documentation Docker](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)).
<!-- D'autres [étapes post-installation de Docker sous Linux](https://docs.docker.com/engine/install/linux-postinstall/) facultatives sont disponibles. -->

```
sudo usermod -aG docker $USER
```

N'oubliez pas de vous déconnecter puis de vous reconnecter pour que l'adhésion au groupe utilisateur `docker` soit bien prise en compte.

### Vérifier l'installation des pré-requis

Vous pouvez vérifier que ces différents composants sont correctement installés avec les commandes suivantes :
```sh
git version
python3 -V
docker -v
docker-compose -v
docker run hello-world
```

## Télécharger Dokidocker

Afin de faciliter l'installation et la maintenance des images Docker, nous avons créé un outil appelé `dokidocker`. Cet outil se fonde sur le très bon travail fourni par l'équipe de [frappe_docker](https://github.com/frappe/frappe_docker).  

Afin de télécharger `dokidocker`, lancez les commandes suivantes sur votre serveur:

```sh
git clone https://gitlab.com/dokos/dokidocker.git
cd dokidocker
```

Dokidocker est composé de trois éléments principaux :
- Un fichier `docker-compose.yml` qui contient les instructions nécessaires au démarrage des conteneurs Docker.
- Une interface en ligne de commande `cli.py` qui simplifie les différentes opérations sur votre instance Dokos, comme la mise à jour ou la sauvegarde du site.
- Un fichier d'environnement `.env` (à créer) pour définir les différents paramètres du site. Ce fichier d'environnement est l'objet de la section suivante.

## Configurez votre environnement

Avant de lancer vos images Docker, vous devez définir un certain nombre de paramètres. Ces paramètres sont les variables d'environnement définies dans le fichier `.env`, un fichier que vous devez créer à partir du modèle `env-template` (configuration avancée) ou de l'outil en ligne de commande (configuration rapide).

### Configuration rapide
Vous pouvez créer une configuration rapidement à l'aide de l'outil en ligne de commande intégré à dokidocker. Lancez la commande suivante et remplissez les différentes informations de manière interactive.

```sh
./cli.py init
```

Cette commande s'occupe de créer le fichier de configuration `.env` et de démarrer les services Docker pour créer votre site.

### Configuration avancée
Si vous souhaitez configurer les détails de votre installation, comme les réglages de la base de données ou des numéros de port pour le serveur web, vous pouvez créer un fichier d'environnement en copiant le modèle fourni et en consultant la [référence des variables d'environnement](#référence-des-variables-denvironnement) :

```sh
cp env-template .env
```

Modifiez ensuite le fichier nouvellement créé pour personnaliser chaque variable d'environnement. Vous pouvez utiliser n'importe quel éditeur de texte ou un éditeur intégré à la ligne de commande comme par exemple `nano .env`. Chaque variable est définie selon le modèle `nom=valeur`, par exemple `SITE_NAME=erp.votre-site.com`. Les lignes vides ou commençant par un `#` sont ignorées.

Une fois le fichier `.env` configuré à votre guise, lancez l'installation du site avec la commande suivante :

```sh
./cli.py init
```

## Vérifier que votre site fonctionne
Le téléchargement des images Docker et la configuration du site peuvent prendre quelques minutes ou plus en fonction du débit disponible sur votre serveur. Quelques instants après avoir lancé la commande `./cli.py init`, votre site devrait être fonctionnel. Pour accéder à votre site, rendez-vous sur le site que vous avez renseigné dans la variable `SITE_NAME`.

Si jamais votre site ne semble pas fonctionner, rendez-vous dans la section [dépannage](#dépannage) de ce document.

Une fois le serveur lancé, vous pouvez accéder au site en utilisant le nom d'utilisateur `Administrator` et le mot de passe défini dans la variable `ADMIN_PASSWORD`.

## Arrêter et redémarrer les conteneurs

Pour stopper Dokos, sans perdre vos données, vous pouvez utiliser les commandes `docker-compose stop` ou `docker-compose down`.

Afin de démarrer Dokos, lancez la commande `docker-compose up -d`.

## Mettre à jour le site

1. **Choisir la nouvelle version à utiliser.**
Cette étape est nécessaire si vous utilisez des images qui référencent une version précise. Sélectionnez les nouvelles versions de Dodock et Dokos en modifiant les valeurs de `DODOCK_VERSION` et `DOKOS_VERSION` dans le fichier de configuration `.env`, par exemple en utilisant la commande `nano .env`.

2. **Télécharger les nouvelles images** avec la commande `docker-compose pull`

3. **Redémarrez les conteneurs** avec la commande `docker-compose up -d`

## Dépannage
/

## Référence des variables d'environnement

- **DOKOS_VERSION**: Définit la version de Dokos à utiliser. `latest` indique la dernière version de développement. Pour les autres tags disponibles, jetez un oeil au [registre](https://gitlab.com/dokos/dokidocker/container_registry/1233699).
- **DODOCK_VERSION**: Défini la version de Dodock à utiliser. `latest` indique la dernière version de développement. Pour les autres tags disponibles, jetez un oeil au [registre](https://gitlab.com/dokos/dokidocker/container_registry/1232957).
- **SITE_NAME**: Site créé après le lancement des conteneurs. Le nom du site doit être un nom de domaine complet, par exemple `erp.example.com`. Si Let's Encrypt est configuré, assurez-vous que vos paramètres DNS pointent correctement vers le serveur actuel.
- **SITES**: Liste des sites inclus dans ce déploiement. Dans le cas single-bench single-site comme celui de l'installation Docker, cette variable d'environnement doit contenir la même valeur que `SITE_NAME`.
- **ADMIN_PASSWORD**: Mot de passe administrateur pour Dodock/Dokos.
- **INSTALL_APPS**: Applications contenues dans l'image utilisée et qui doivent être installées sur le site défini dans `SITE_NAME`. Dodock est installée par défaut (et ne peut pas être retirée), Dokos s'appelle `erpnext` et est présente dans la configuration par défaut.
- **HTTP_PORT**: Le numéro du port d'écoute pour les connexions HTTP (couramment le port 80).
- **HTTPS_PORT**: Le numéro du port d'écoute pour les connexions chiffrées HTTPS (couramment le port 443).
- **MARIADB_HOST**: Nom de domaine pour MariaDB. Gardez `mariadb` si vous utilisez le conteneur de base de donnée par défaut.
- **DB_ROOT_USER**: Nom d'utilisateur Root pour MariaDB.
- **MYSQL_ROOT_PASSWORD**: Mot de passe pour accéder à la base de données MariaDB dans un conteneur. Si vous utilisez une base de données MariaDB gérée ou externe, vous n'avez pas besoin de définir de mot de passe ici.


## Notes

- L'installation du site avec `./cli.py init` peut prendre plusieurs minutes.
- Vous pouvez utiliser la commande `docker-compose logs` pour consulter les informations de lancement et les logs d'accès au serveur (`docker-compose logs webserver`).
- Une fois le site créé, la modification de certaines variables d'environnement de `.env` peut entraîner des problèmes.