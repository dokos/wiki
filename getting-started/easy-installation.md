---
title: Installation rapide
description: Installez Dokos sur un nouveau serveur
published: true
date: 2023-01-09T12:52:12.951Z
tags: 
editor: markdown
dateCreated: 2020-11-26T14:21:39.118Z
---

# Installation

- Il s'agit d'une installation automatique destinée à être installée sur un serveur vierge.
- Fonctionne avec Ubuntu 22.04 ou Debian 11

> Python 3.10 est la version par défaut sur Ubuntu 22.04. 
> Sur Debian 11 il faut l'installer manuellement.
> Exemple de tutoriel : https://tecadmin.net/how-to-install-python-3-10-on-debian-11/
{.is-warning}

- Il est nécessaire d'installer Python 3.10 et son utilitaire PIP en lançant `apt install python3-pip`
- Vous devez également installer les bibliothèques `build-essential` et `python-setuptool` en lançant `apt install build-essential python3-setuptools python3-venv`
- `export LC_ALL=C.UTF-8`  


- Ce script va installer les pré-requis, dokos-cli (bench) et créer un nouveau site dokos
- Vous devrez choisir un mot de passe pour l'administrateur système et pour MariaDB (utilisateur root)
- Vous pourrez ensuite vous connecter avec l'utilisateur **Administrator** et le mot de passe choisi.


> Dokos étant fondé sur la même infrastructure technique que le framework Frappe, vous pouvez vous réferer à cette documentation pour une installation manuelle : https://frappeframework.com/docs/user/en/installation


> Sur Ubuntu 22.04, le paquet par défaut pyOpenSSL nécessite parfois d'être mis à jour avant de lancer le script. Si vous avez des problèmes durant l'installation, vous pouvez utiliser la commande `sudo pip install pyOpenSSL --upgrade` pour mettre à jour ce paquet.
{.is-warning}

Ouvrez un terminal sur votre serveur et lancez les commandes suivantes.

### Création d'un utilisateur

Si vous êtes sur un serveur vierge et connecté en temps que **root**, créez d'abord un nouvel utilisateur pour dokos et donnez lui les droits **sudo** :

```
  adduser [dokos-user]
  usermod -aG sudo [dokos-user]
```

Puis connectez-vous avec cet utilisateur

    su - [dokos-user]


### Téléchargement du script d'installation

    wget https://gitlab.com/dokos/docli/raw/master/bench/install.py


### Lancement du script d'installation

Lancez le script pour installer dokos en mode production:

    sudo python3.10 install.py --production --user [dokos-user]


Pour une installation dans un container ou LXC:

	sudo python3.10 install.py --production --user [dokos-user] --container


Vous pouvez également ajouter `--verbose` pour obtenir des traces plus détaillées en cas d'erreur.

> Les applications s'appellent Frappe et ERPNext car Dokos est une adaptation de ces logiciels.
> L'architecture sous-jacente est similaire à celle de ces deux logiciels.
> Vous trouverez plus d'information sur leurs sites respectifs: [Frappe](https://frappe.io/docs), [ERPNext](https://erpnext.com/docs)



### Que fait ce script ?

- Installation des pré-requis
- Installation de l'outil de ligne de commande `bench`
- Création d'un nouveau bench (un dossier contenant votre ou vos sites dokos)
- Création d'un nouveau site dokos

### Comment démarrer dokos

Votre site sera automatiquement configuré et géré par `nginx` et `supervisor`.
Si ce n'est pas le cas, vous pouvez lancer depuis le dossier bench:

    sudo bench setup production [dokos-user]


Vous pouvez alors vous connecter à l'adresse de votre serveur pour commencer à utiliser dokos.

> Si votre serveur est à l'adresse 57.69.123.1, connectez-vous à cette adresse pour accéder à votre site.

---

Aide
====

Pour l'aide de bench, vous pouvez lancer

    bench --help

Mise à jour
========

## Dokos-cli

Pour mettre à jour dokos-cli (Bench), vous pouvez lancer la commande:

`pip install dokos-cli --upgrade`

## Dokos

Pour mettre à jour dokos, lancez `bench update --restart-supervisor` depuis votre dossier bench.
Cela mettra à jour les applications, les patches, cela compilera les fichiers JS et CSS et redémarrera supervisor.

Vous pouvez aussi lancer une partie du script de mise à jour avec les commandes suivantes:

`bench update --pull` récupèrera les mises à jour du code des applications

`bench update --patch` lancera la migration de la base de données vers une nouvelle version

`bench update --build` compilera les fichiers JS et CSS pour ce dossier bench

`bench update --bench` mettra à jour l'outil de ligne de commande bench

`bench update --requirements` mettra à jour les librairies dont dépendent les applications installées


Si vous avez fait des modifications locales du code de Dodock/Dokos (non recommandé), vous pouvez utiliser la commande suivante pour réinitialiser les répertoires:

`bench update --reset`