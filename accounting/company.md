---
title: Fiche Société
description: 
published: true
date: 2022-10-28T13:46:30.660Z
tags: 
editor: markdown
dateCreated: 2021-07-26T15:06:00.836Z
---

# Société

Une société est une personne morale constituée d'une association de personnes pour l'exploitation d'une entreprise commerciale ou industrielle.

Dans DOKOS, la première société est créée lors de la création d'un compte DOKOS. Pour chaque entreprise, vous pouvez définir un domaine comme la production, vente au détail ou services en fonction de la nature de votre activité commerciale.

---

Si vous avez plusieurs entreprises, vous pouvez les ajouter à partir de :

> Accueil > Comptabilité > **société**

## 1. Comment créer une nouvelle entreprise 

1. Allez dans la liste Entreprise, cliquez sur :heavy_plus_sign: **Ajouter Société.**
2. Saisissez le **nom**, l'**abréviation** et la **devise** par défaut de l'entreprise.
3. **Enregistrer**.

L'abréviation de votre entreprise est créée par défaut. Par exemple, DKY pour Dokompany. L'abréviation aide à différencier les actifs d'une entreprise d'une autre.

L'abréviation apparaît également dans divers comptes, centres de coûts, modèles de taxes, entrepôt, etc. de votre entreprise.

Vous pouvez également joindre un logo d'entreprise et ajouter une description de l'entreprise.

### 1.1 Structure multi-sociétés

Supposons que vous dirigez un groupe d'entreprises, certaines peuvent être de plus grandes entreprises et d'autres peuvent être plus petites et faire partie de la ou des plus grandes entreprises.

Dans DOKOS, vous pouvez créer plusieurs sociétés. La structure de l'entreprise peut être parallèle, c'est-à-dire des sociétés sœurs, des sociétés mère-enfant ou une combinaison des deux.

Une société mère est une organisation plus grande qui se compose d'une ou plusieurs sociétés filles. Une société fille est une filiale d'une société mère.

L'arborescence de l'entreprise affiche la structure globale de vos entreprises.

Une fois que vous avez construit un arbre d'entreprise, DOKOS validera si les comptes des sociétés filles correspondent aux comptes de la société mère. Tous les comptes peuvent être regroupés dans un plan comptable consolidé.

### 1.2 Autres options lors de la création d'une entreprise

- **Domaine** : Le domaine d'activité de l'entreprise. Ex : production, services, etc. Choisissez-en un lors de la création de votre compte.
- **Est un groupe** : Si coché, cela devient une société mère.
- **Société mère** : S'il s'agit d'une société enfant, définissez le parent à partir de ce champ, c'est-à-dire sélectionnez une société de groupe à laquelle appartient cette société. Si une société mère est définie, le plan comptable de la nouvelle société que vous créez sera créé en fonction de la société mère sélectionnée.

### 1.3 Plan comptable

Pour chaque entreprise, le maître pour le plan comptable est maintenu séparément. Cela vous permet de tenir une comptabilité distincte pour chaque entreprise conformément aux exigences légales. Vous pouvez également importer un plan comptable à l'aide de l' outil d'importation de plans comptables.

DOKOS a localisé un plan comptable facilement disponible pour certains pays. Lors de la création d'une nouvelle entreprise, vous pouvez choisir de configurer le plan comptable pour elle à partir de l'une des options suivantes.

- Plan comptable standard
- Basé sur le plan comptable de l'entreprise existante


Notez que, si la société mère est sélectionnée lors de la création d'une nouvelle société, le plan comptable sera créé sur la base de la société mère existante.

### 1.4 Valeurs par défaut

Dans le maître de l'entreprise, vous pouvez définir de nombreuses valeurs par défaut pour les maîtres et les comptes. Ces comptes par défaut vous aideront dans la comptabilisation rapide des transactions comptables, où la valeur du compte sera extraite du maître de l'entreprise si elle est fournie. Dès que la société est créée, un plan comptable et un centre de coûts par défaut sont automatiquement créés.

Les valeurs par défaut suivantes peuvent être définies pour une entreprise :

- Livre des finances par défaut
- En-tête par défaut
- Liste de jours fériés par défaut
- Heures de travail normales
- Termes et conditions par défaut
- De campagne
- Numéro d'identification fiscale
- Date de création

## 2. Caractéristiques

### 2.1 Objectif de vente mensuel

Définissez le nombre cible de ventes mensuelles dans la devise de l'entreprise, par exemple 10 000 €. Les ventes mensuelles totales seront visibles une fois les transactions effectuées.

### 2.2 Paramètres du compte

Certains des comptes suivants seront définis par défaut lorsque vous créez une nouvelle entreprise, d'autres peuvent être créés. Les comptes sont visibles dans le plan comptable. Ces valeurs peuvent être modifiées ultérieurement si nécessaire.

- **Compte bancaire par défau**t : Compte 512
- Compte de caisse par défaut : 5314 - Caisse en devises
- **Compte de transfert inter-bancaire** : 580  - Virements internes
- **Compte client par défaut** : 411 - Clients
- **Compte d'acompte client par défaut** : 4191 - Clients créditeurs
- **Compte d'arrondis** : 658  - Charges diverses de gestion courante
- **Centre de coûts pour les arrondis** : Principal
- **Compte de passage en perte** : 658  - Charges diverses de gestion courante
- **Compte de remises autorisées** : 709  - Rabais, remises et ristournes accordés par l'entreprise
- **Compte de remises reçues**
- **Compte de gains / pertes de change** : 666  - Pertes de Change
- **Compte de gains / pertes de change non réalisés** : 686  - Dotations aux amortissements, dépréciations et provisions
- **Compte de bénéfice/perte non réalisée**
- **Compte fournisseur par défaut** : 400  - Fournisseurs et comptes rattachés - DK
- **Compte d'acompte fournisseur par défaut** : 401 - Fournisseurs
- **Compte d'acompte fournisseur par défaut** : 4091 - Fournisseurs - Avances et acomptes versés sur commande
- **Compte d'avances versées aux employés par défaut** : 425 - Personnel - Avances et acomptes
- **Compte de charges par défaut** : 600 - Achats
- **Compte de produits par défaut** : 706  - Prestations de services ou 701 - Ventes de produits finis
- **Compte de produits comptabilisés d'avance par défaut** : 487 - Produits constatés d'avance
- **Compte de charges comptabilisés d'avance par défaut** :486 - Charges constatées d'avance - DKY
- **Compte de paie par défaut** : 421 - Personnel - Rémunérations dues
- **Compte de notes de frais par défaut** : 421 - Personnel - Rémunérations dues
- **Compte de remise sur le paiement par défaut**

### 2.3 Paramètres de stock 

Par défaut, des entrées comptables sont écrites de manière mensuelle ou trimestrielle pour les transactions de stock.
L'activation de la fonction d'inventaire permanent revient à écrire une entrée comptable pour chaque transaction de stock.

- Compte d'inventaire par défaut
- Compte d'ajustement des stocks
- Stock reçu mais non facturé
- Dépenses incluses dans l'évaluation

### 2.4 Paramètres d'amortissement des immobilisations 

Pour gérer les immobilisations d'une entreprise, les comptes suivants sont nécessaires. La plupart d'entre eux seront créés par défaut. Ils sont visibles dans le plan comptable .

- Compte d'amortissement cumulé
- Compte de dotation aux amortissements
- Série pour l'écriture d'amortissement d'immobilisation (écriture au journal)
- Dépenses incluses dans l'évaluation des actifs
- Compte de gain/perte lors de la cession d'actifs
- Centre de coûts d'amortissement des immobilisations
- Compte d'immobilisations en cours
- Actif reçu mais non facturé

### 2.5 Paramètres des stock

- Autoriser l'inventaire permanent
- Autoriser l'inventaire permanent pour les articles non stockés
- Compte de stock par défaut
- Compte de variation du stock
- Stock reçu non facturé
- Service reçu mais non facturé
- Dépenses incluses dans la valorisation

### 2.6 Budget

**Rôle d'approbateur de budget exceptionnel** : le rôle sélectionné ici peut contourner le budget défini pour approuver les dépenses.

### 2.7 Informations sur l'entreprise

Pour référence, les détails suivants de votre entreprise peuvent être enregistrés dans DOKOS :

- Date de constitution
- N°de téléphone
- Fax
- E-mail
- Site Internet
- Adresse
- Détails d'inscription

> **Remarque** : lorsque vous définissez l'adresse ici, il est important de cocher la case « Is Your Company Address ».
{.is-warning}

**Détails d'enregistrement** : Ici, vous pouvez enregistrer divers numéros de taxe/chèque/banque pour référence.

### 2.8 Supprimer toutes les transactions de l'entreprise

Vous pouvez supprimer toutes les transactions (Commandes, Factures) d'une Entreprise. À utiliser avec prudence , les transactions une fois supprimées ne peuvent pas être récupérées.

**Exigences** 

- L'utilisateur doit être un gestionnaire de système
- L'Utilisateur doit être le créateur de la Société

- Cliquez sur le bouton Supprimer les transactions de l'entreprise
- Vérifiez votre mot de passe
- Entrez le nom de l'entreprise pour confirmation

Les données de base telles que l'article, le compte, l'employé, la nomenclature, etc. resteront telles quelles.

**Qu'est-ce qui est affecté ?**

- Les ventes/bons de commande/factures reçus/notes seront supprimés
- Les ventes mensuelles et l'historique des ventes seront effacés
- Toutes les notifications seront effacées
- Les adresses de prospects auxquelles la société est liée seront supprimées
- Toutes les communications liées à la Société seront supprimées
- Toutes les séries de noms seront réinitialisées
- Les entrées de stock liées à un entrepôt de cette société seront supprimées
