---
title: Transaction bancaire
description: 
published: true
date: 2021-12-30T20:51:59.072Z
tags: 
editor: markdown
dateCreated: 2021-12-30T15:04:31.379Z
---

# Transaction bancaire

Ce formulaire affiche les transactions bancaires dans Lyon.

## 1. Prérequis avant utilisation

Avant d'utiliser la saisie d'opérations bancaires, il est conseillé de créer d'abord les éléments suivants :

- **Banque**
- **Compte bancaire**

## 2. Comment utiliser la transaction bancaire 

Habituellement,  une transaction bancaire n'est pas destinée à être créée manuellement. Il est automatiquement créé à l'aide de du téléchargement du relevé bancaire depuis le **Rapprochement Bancaire**.

### 2.1 Champs supplémentaires dans Transaction bancaire 

- **Date** : Date de l'opération bancaire.
- **Numéro de référence** : Référence de l'opération bancaire.
- **Compte bancaire** : Le compte bancaire à partir duquel les transactions ont été effectuées.

## 3. Caractéristiques / Champs

Ces champs sont mis à jour via le rapprochement bancaire et ne sont pas destinés à être modifiés à partir d'ici.

### 3.1 Devise et débit/crédit

- **Débit** : Le montant débité.
- **Crédit** : Le montant crédité.
- **Devise** : La devise dans laquelle la transaction a été effectuée.
- **Description** : une description de l'instruction.

### 3.2 Référence

Numéro de référence : Un chèque ou autre numéro de référence.

### 3.3 Saisies de paiement

- **Document de paiement** : Le document par rapport auquel la transaction a été effectuée, qu'il s'agisse d'une facture de vente, d'une note de frais, d'une facture d'achat, d'une écriture de paiement ou d'une écriture au journal.
- **Saisie de paiement** : La transaction spécifique.
- **Montant alloué** : Le montant alloué pour cette transaction particulière.
- **Montant alloué **: Le montant total alloué. Montant non alloué : Le montant total non alloué.