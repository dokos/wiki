---
title: Banque
description: 
published: true
date: 2022-04-26T10:35:56.163Z
tags: 
editor: markdown
dateCreated: 2022-04-26T10:35:32.245Z
---

# Banque

Dans Dokos, la sauvegarde de différentes banques vous permet de télécharger une feuille Excel et de mapper les transactions sur le grand livre. Les transactions sont créées en tant que transactions bancaires. Ceux-ci peuvent ensuite être utilisés à des fins de référence et de rapports. Cela se fait à l'aide du rapprochement bancaire .

Pour accéder à la banque, allez sur :

> Accueil > Comptabilité > Relevé bancaire > **Banque**

![banque.png](/accounting/banque.png)

## 1. Comment créer une banque ?

La création d'une banque est simple, allez dans la liste des banques, cliquez sur ajouter et entrez un nom de banque.

## 1.1 Configurer l'importation de données pour une banque

1. Sous « Champ dans la transaction bancaire », sélectionnez le champ à mettre à jour dans le formulaire « Saisie de transaction de relevé bancaire ».
2. Sous « Colonne dans le fichier bancaire », saisissez la colonne dans le fichier Excel exporté depuis la banque.
