---
title: Dimensions comptables
description: 
published: true
date: 2021-05-07T06:52:12.188Z
tags: 
editor: markdown
dateCreated: 2021-05-07T06:51:55.513Z
---

# Dimensions comptables
> Contenu bientôt disponible
{.is-warning}

La **dimension comptable** signifie étiqueter chaque transaction avec des dimensions appropriées comme la **succursale**, l'**unité commerciale**, etc. Cela vous permet de maintenir chaque segment séparément, limitant ainsi la maintenance globale sur les comptess et votre plan comptable reste pur.

Le **centre de coûts** et le **projet** sont traités comme des dimensions par défaut dans DOKOS. Lors de la définition d'un champ dans la dimension comptable, ce champ sera ajouté dans les rapports de transactions, le cas échéant.

Dans DOKOS, vous pouvez créer des **dimensions comptables configurables** et les **utiliser dans les transactions et les rapports**.

---

Pour accéder à la **liste des dimensions comptables**, accédez à:

> Accueil > Comptabilité > Paramètres > **Dimensions comptables**


