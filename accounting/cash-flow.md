---
title: Flux de trésorerie
description: 
published: true
date: 2022-04-06T15:54:08.954Z
tags: 
editor: markdown
dateCreated: 2022-04-06T15:54:08.954Z
---

# Flux de trésorerie

Les flux de trésorerie sont les sommes d'argent entrantes et sortantes du compte d'une entreprise. Ces flux sont analysés en comptabilité et finance afin de déterminer des besoins ou non en liquidités pour une entreprise. Les flux de trésorerie disponible est une notion permettant de déterminer la politique des investissements dans une entreprise.

## Que sont les flux de trésorerie ?

Le flux de trésorerie sont les sommes d'argent qui traversent les comptes de votre entreprise. Pour faire simple, prenons une entreprise qui dispose d'un seul compte bancaire par lequel transitent directement tous les mouvements d'argent. Vous aurez donc deux types de flux :

- Des flux de trésorerie entrants (l'argent qui rentre, le paiement des clients)
- Des flux de trésorerie sortants (des dépenses liées à votre activité)

Deux contraintes principales s'appliquent à toutes les entreprises :

- les comptes doivent toujours être positifs (si l'argent sort plus vite qu'il ne rentre : créances clients, encaissements tardifs, etc, alors vous avez un souci)
- les investissements doivent être financés par les fonds disponibles de l'entreprise

Entre le début et la fin d'un mois par exemple, voici comment se décomposent les disponiblités de votre entreprise : 

Trésorerie finale = Flux de trésorerie entrants(1) - Flux de trésorerie sorants(2) + Trésorerie initiale(3)

A chaque période soit vous augmentez votre trésorerie initale, soit vous consommez votre trésorerie ! 
