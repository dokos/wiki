---
title: Modèle d'écriture de journal
description: 
published: true
date: 2022-10-28T13:55:14.197Z
tags: 
editor: markdown
dateCreated: 2021-08-27T12:32:00.294Z
---

# Modèle d'écriture de journal

Un modèle d'entrée de journal vous permet de définir et de sélectionner une liste prédéterminée de comptes et d'options lors de la création d'une entrée de journal.

---

Pour accéder au modèle d'entrée de journal, accédez à :

> Accueil > Comptabilité > Grand livre > **Modèle d'écriture au journal**

## 1. Comment créer et utiliser un modèle d'entrée de journal : 

1. Accédez à la liste des modèles d'entrées de journal et cliquez sur Nouveau.
2. Ajoutez les détails suivants :
- Titre du modèle : il sera utilisé pour sélectionner le modèle dans l'entrée de journal.
- Société : Par défaut, la société définie dans Global Defaults est sélectionnée. Vous pouvez également sélectionner n'importe quelle autre entreprise.
- Type d'entrée : Vous pouvez sélectionner ici les types d'entrée disponibles dans Écriture au journal . La valeur par défaut est Écriture au journal .

Il y a 3 « types d'entrée » spéciaux dans ceci :

1. Saisie d'ouverture : Ceci obtiendra tous les comptes et les chargera dans le tableau "Ecritures comptables". Pour en savoir plus, visitez la page Solde d'ouverture .
2. Saisie bancaire : Ceci obtiendra et chargera le compte bancaire par défaut s'il est défini.
3. Cash Entry : Ceci obtiendra et chargera le compte de trésorerie par défaut s'il est défini.

- Est en cours d'ouverture : ce paramètre sera automatiquement défini sur « Oui » si « Entrée d'ouverture » ​​est sélectionné comme Type d'entrée.
- Séries : Vous pouvez choisir parmi une liste de séries de noms disponibles pour l'entrée de journal.
- Écritures comptables : Ici, vous pouvez sélectionner une liste de comptes à ajouter à l'écriture.

Enregistrez et accédez à l' entrée de journal et cliquez sur nouveau.

Dans le champ « À partir du modèle », lorsque vous sélectionnez le modèle, il chargera les comptes et les autres options qui y sont définies. Veuillez noter que cela effacera d'abord la table des écritures comptables, mais vous pouvez ajouter d'autres comptes à la table en dehors de ceux extraits du modèle.
